//Find the minimum sum which is gives by the subarray


import java.util.*;
class ArrayDemo{
	public static void main(String [] pdp){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter The Array Length");
		int arr[] = new int[sc.nextInt()];

		System.out.println("Enter The Elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}

		int sum = 0;
		int preSum = Integer.MAX_VALUE;
		int flag =0;

		for(int i=0;i<arr.length;i++){
			sum = 0;
			for(int j=i;j<arr.length;j++){
				sum = sum+arr[i];
				if(preSum>sum){
					flag = 1;
					preSum = sum;
				}
			}
		}

		if(flag == 1){
			System.out.println("Min Sum :- "+preSum);
		}
	}
}




