//i/p = target =7;
//arr=[2,3,1,2,4,3]


import java.util.*;
class ArrayDemo{
	public static void main(String [] pdp){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter TheArray Length");
		int arr[] = new int[sc.nextInt()];

		System.out.println("Enter The Array Elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}

		System.out.println("Enter The Target");
		int target = sc.nextInt();

		int prevCnt = Integer.MAX_VALUE;

		for(int i=0;i<arr.length;i++){
			int cnt = 0,sum=0;

			for(int j=i;j<arr.length;j++){
				sum = sum+arr[j];
				cnt++;

				if(sum>=target && prevCnt>cnt){
					prevCnt = cnt;
					break;
				}
			}
		}
		System.out.println("o/p :- "+prevCnt);
	}
}
