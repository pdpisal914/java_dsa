//Rotate Array

import java.util.*;
class ArrayDemo{
	public static void main(String [] pdp){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter The Array Length");
		int arr[] = new int[sc.nextInt()];

		System.out.println("Enter The Elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}

		System.out.println("Enter The no of rotations");
		int D = sc.nextInt();

		for(int i=1;i<=D;i++){
			int temp = arr[0];
				for(int j=1;j<arr.length;j++){
					arr[j-1]=arr[j];
				}
				arr[arr.length-1] = temp;
		}

		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+"   ");
		}
		System.out.println();
	}

}
