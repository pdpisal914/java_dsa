//Calculate the count of SubArrays

import java.util.*;
class ArrayDemo{
	public static void main(String [] pdp){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter The Array Length");
		int arr[] = new int[sc.nextInt()];


		System.out.println("Enter The Elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}

		int cnt = (arr.length*(arr.length+1))/2;

		System.out.println("Total SubArrays : "+cnt);
	}
}

