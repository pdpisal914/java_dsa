//Find last and first occring index of the target element

import java.util.*;
class ArrayDemo{
	public static void main(String [] pdp){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Array Length");
		int arr[]=new int[sc.nextInt()];

		System.out.println("Enter The Elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}

		System.out.println("Enter The Target Element");
		int target = sc.nextInt();

		int first =-1;
		int last = -1;
		int flag =0;

		for(int i=0;i<arr.length;i++){

			if(flag == 0 && arr[i]==target){
				first = i;
				flag =1;
			}else if(flag == 1&& arr[i]==target){
				last = i;
			}
		}
		System.out.println(first+"   "+last);

	}
}
