//Statement=WAP to print the sum of all even numbers & multiplication of odd numbers between 1 to 10;


class Code{
	public static void main(String [] pdp){
		int start=1;
		int end=10;
		int sum=0;
		int mult=1;

		while(start<=end){
			if(start%2==0){
				sum=sum+start;
			}else{
				mult=mult*start;
			}
			start++;
		}
		System.out.println(sum+"is the sum of even numbers");
		System.out.println(mult+"is the multiplication of odd digits");

	}
}
