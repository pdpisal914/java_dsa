//Statement=WAP to print the given number in reverse
//WAP to reverse the given number

class Reverse{
	public static void main(String [] pdp){
		int num=12345;
		int rev=0;

		while(num!=0){
			rev=rev*10 + (num%10);
			num=num/10;
		}
		System.out.println(rev);
	}
}

