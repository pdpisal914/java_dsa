class Parent{ 
	private Parent(){
	
	//	this(10);
		System.out.println(this);
		System.out.println("In Parent Constructor");
	}
	/*Parent(int x){
		System.out.println("In Para Parent");
	}*/
}
class Child extends Parent{
	Child(){
	//	this(11);
		System.out.println(this);
		System.out.println("In Child Consructor");
	}
	/*Child(int y){
		System.out.println("In Para Child");
	}*/
}
class Demo{
	public static void main(String [] pdp){
		Child obj = new Child();
	}
}
