class Parent{
	int x=5;
	Parent(){
		System.out.println("In Parent Constructor");
	}
	void fun(){
		System.out.println("In Parent fun");
	}
}
class Child extends Parent{
	int x =1000;
	Child(){
		super.x=11;
		super.fun();
		System.out.println(super.x);
		System.out.println("In Child No args Const");
	}
	Child(int x){
		super();
		super.fun();
		System.out.println(super.x);
		System.out.println("In Child para Const");
	}

	void gun(){
	
		System.out.println(x);
	}

	public static void main(String [] pdp){
		Child obj = new Child();
		Child obj1 = new Child(10);
		obj.fun();
		obj.gun();
	}
}

