class Parent{
	int x=10;
	Parent(){
		System.out.println("Parent No Args");
	}
	void fun(){
		System.out.println("Parent fun");
	}
}


class Demo extends Parent{
	Demo(){
		System.out.println("In No args");
	}
	void gun(){
		System.out.println("In Gun");
	}
	public static void main(String [] pdp){
		Demo obj = new Demo();
	}
	//super.x;
	static{
	System.out.println("In Instance Block");
		System.out.println(this);
		super.fun();
		//super.gun(); 
	}
	
}
