class Parent{
	Parent(){
		System.out.println("In Parent Constructor");
	}
	Parent(int x){
		System.out.println("In Parent para");
	}

	void fun(){
		System.out.println("In Parent fun");
	}
}
class Child extends Parent{
	Child(){
		super(10);
		System.out.println("In Child Constr");
	}
	{
		//super(); //super() should be first call in constructor
		//this();	//this() should be first call in constructor
		System.out.println("In Instance Block");
	}
	public static void main(String [] pdp){
		Child obj = new Child();
	}
}
