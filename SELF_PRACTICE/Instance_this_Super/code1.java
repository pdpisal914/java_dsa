class Parent{
	int x =90;
	Parent(){
		System.out.println("In P Constructor");
	}
}
class Child extends Parent{
	int x=1000;
	Child(){
		System.out.println(Parent.x);
		System.out.println("In C Constructor");
	}
	{
		System.out.println(super.x);

		System.out.println(x);
	}
	public static void main(String [] pdp){
		Child obj = new Child();
	}
}
