class Parent{
	Parent(){
		System.out.println("In Parent Constructor");
	}
	Parent(int x){
		System.out.println("In Parent para");
	}

	static void fun(){
		System.out.println("In Parent fun");
	}
}
class Child extends Parent{
	Child(){
		super(10);
		System.out.println("In Child Constr");
	}
	{
		super.fun();
		this.gun();
		System.out.println("In Instance Block");
	}

	void gun(){
		System.out.println("In gun");
	}
	public static void main(String [] pdp){
		Child obj = new Child();
	}
}
