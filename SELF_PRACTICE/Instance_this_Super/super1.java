class Parent{
	int x=5;
	Parent(){
		System.out.println("In Parent Constructor");
	//	Child obj = new Child(10);  recurssion 
	}
	void fun(){
		System.out.println("In Parent fun");
	}
}
class Child extends Parent{
	Child(){
	//	super.fun();  ..calling the parent method by using super
	//	System.out.println(super);//nhi chalat
		super.x=11;
		System.out.println(super.x);
//		System.out.println(x); No error
		System.out.println("In Child No args Const");
	}
	Child(int x){
		super();
		super.fun();
		System.out.println(super.x);
		System.out.println("In Child para Const");
	}

	void gun(){
		System.out.println(super.x);
		//super(); //method mdhe super vapru shkt nhiu
	}

	public static void main(String [] pdp){
		Child obj = new Child();
		Child obj1 = new Child(10);
		obj.fun();
		obj.gun();
	}
}

