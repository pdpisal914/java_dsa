import java.io.*;
class JaggedArray{
	public static void main(String [] pdp)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		//int arr[][]=new int[][]{new int[]{1,2,3,4,5},new int[]{1,2,3,4,5,6,7,8,9}};
		//int arr1[][]=new int[][]{{1,2,3,4,5},{1,2,3,4,5,6,7,8,9}};
		System.out.println("Enter the number of Rows in array");

		int arr[][]=new int[Integer.parseInt(br.readLine())][];

		for(int i=0;i<arr.length;i++){
			System.out.println("Enter the number of column in" + i + "th Row");
			arr[i]=new int[Integer.parseInt(br.readLine())];

			System.out.println("Enter the elements");
			for(int j=0;j<arr[i].length;j++){
				arr[i][j]=Integer.parseInt(br.readLine());
			}
		}

		for(int arr1[] : arr){
			for(int x:arr1){
				System.out.print(x);
			}
			System.out.println();
		}

	}

}
