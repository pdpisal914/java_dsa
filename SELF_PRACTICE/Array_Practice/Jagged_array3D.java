import java.io.*;
class Jagged3d{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the Total Number of Planes");
		int arr[][][]=new int[Integer.parseInt(br.readLine())][][];

		for(int i=0;i<arr.length;i++){
			System.out.println("Enter the Total number of rows in plane "+i);
			arr[i]=new int[Integer.parseInt(br.readLine())][];

			for(int j=0;j<arr[i].length;j++){
				System.out.println("Enter the Total number of columns");
				arr[i][j]=new int[Integer.parseInt(br.readLine())];
				
				System.out.println("Enter the elements");
				for(int k=0;k<arr[i][j].length;k++){
					arr[i][j][k]=Integer.parseInt(br.readLine());
				}
			}
		}

		for(int arr1[][] : arr){
			for(int arr2[]:arr1){
				for(int x:arr2){
					System.out.print(x);
				}
				System.out.println();
			}
			System.out.println("\n");
		}
	}
}
					
