import java.io.*;
class StringDemo{
	public static void main(String [] pdp) throws IOException{
	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		String str1 = "Pranav";//SCP
		String str2 = "Pranav";//SCP

		String str3 = new String("Pranav");//new Object
		String str4 = new String("Pranav");//new Object

		System.out.println("Enter the String Pranav");
		String str5 = br.readLine();//new Object
		System.out.println("Enter the string 6 Pranav");
		String str6 = new String(br.readLine());//New Object

		System.out.println(str1 + " = " + System.identityHashCode(str1));
		System.out.println(str2 + " = " + System.identityHashCode(str2));
		System.out.println(str3 + " = " + System.identityHashCode(str3));
		System.out.println(str4 + " = " + System.identityHashCode(str4));
		System.out.println(str5 + " = " + System.identityHashCode(str5));
		System.out.println(str6 + " = " + System.identityHashCode(str6));
	}
}


