abstract class Demo{
	int x = 10;
 	Demo(){
		System.out.println("In Abstract Class Constructor");
	}
	abstract void fun();
	void temp1_(){
		System.out.println("In temp");
	}
	abstract void gun();
	abstract protected void ghs();
};
class Child extends Demo{
	Child(){
		super();
	}
	void fun(){
		System.out.println("In fun");
	}
	void gun(){
		System.out.println("In gun");
	}
	protected void ghs(){
		System.out.println("n ghs");
	}
	void temp_(){
		System.out.println("In temp");
	}
	public static void main(String [] pdp){
		Demo obj=new Child();
		obj.fun();
		obj.temp_();
		obj.gun();
		obj.ghs();
	}
}


	
