interface Parent{
	/*static void fun(){
		System.out.println("In Fun()");
	}*/
	default void fun(){
		System.out.println("In fun default");
	}
}
class Child implements Parent{

	public static void main(String [] pdp){
		Child obj = new Child();
		Parent.fun();
		obj.fun();
	}
}

