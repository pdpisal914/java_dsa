
interface Ajoba{
	static void gun(){
		System.out.println("Ajoba Interface gun");
	}
}

interface Parent extends Ajoba{
	default void gun(){
		System.out.println(" Parent Interface gun");
			
	}

	public static void main(String [] pdp){
		Child obj = new Child();
		obj.gun();
		obj.fun();
	}

}
class Child implements Parent{
	
	void fun(){
		System.out.println("In Child fun");
	}

}
