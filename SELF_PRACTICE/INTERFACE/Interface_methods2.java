
interface Ajoba{
	default void gun(){
		System.out.println("Ajoba Interface gun");
	}
}

interface Parent extends Ajoba{
	default void gun(){
		System.out.println(" Parent Interface gun");
		System.out.println("This gun() = "+this);
	}

	public static void main(String [] pdp){
		Child obj = new Child();
		obj.gun();
		obj.fun();
		System.out.println("obj = "+obj);
	}
}
class Child implements Parent{
	void fun(){
		System.out.println("In Child fun");
		System.out.println("This fun() = "+this);
	}
}
