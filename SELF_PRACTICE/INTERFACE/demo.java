class Parent{
	void gun(){
		System.out.println("In gun parent");
		System.out.println(this);
		this.fun();

	}
}
class Child extends Parent{
	void fun(){
		System.out.println("In fun");
		System.out.println(this);
	}
	public static void main(String [] pdp){
	       Child obj = new Child();
 		obj.gun();	
 		obj.fun();	
	}
}	

