import java.io.*;

class Stack{
	int maxSize;
	int stackArr[];
	int top=-1;

	Stack(int size){
		this.stackArr= new int[size];
		this.maxSize = size;
	}

	void push(int data){

		if(top == maxSize){
			System.out.println("Stack Overflow");
			return;
		}else{
			top++;
			stackArr[top]=data;
		}
	}
	int pop(){

		if(isEmpty()){
			System.out.println("Stack Underflow");
			return -1;
		}else{
			int temp = stackArr[top];
			top--;
			return temp;
		}
	}

	boolean isEmpty(){

		if(top == -1){
			return true;
		}else{
			return false;
		}
	}

	int peek(){

		if(isEmpty()){
			System.out.println("Stack is Empty");
			return -1;
		}else{
			return stackArr[top];
		}
	}

	int size(){

		return top+1;
	}

	void printStack(){

		if(isEmpty()){
			System.out.println("Stack is Empty");
			return;
		}else{
			System.out.print("[");
			for(int i=0;i<=top;i++){
				if(i<top){
					System.out.print(stackArr[i]+" , ");
				}else{
					System.out.print(stackArr[i]);
				}
			}
			System.out.println("] ");
		}
	}

	static Stack MergeSort(Stack s1,Stack s2){
		Stack s3 = new Stack(s1.maxSize + s2.maxSize);

		while(s1.isEmpty() != true && s2.isEmpty() != true){
			
			if(s1.peek() >= s2.peek()){
				s3.push(s1.pop());
			}else{
				s3.push(s2.pop());
			}
		}

		while(s1.isEmpty() == false){
			s3.push(s1.pop());
		}

		while(s2.isEmpty() == false){
			s3.push(s2.pop());
		}

		Stack s4 = new Stack(s1.maxSize + s2.maxSize);
		while(s3.isEmpty() != true){
		//	System.out.println("Pranav");
			s4.push(s3.pop());
		}

		return s4;
	}


}
class Client{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter The Size OF 1st Stack");
		Stack s1 = new Stack(Integer.parseInt(br.readLine()));

		System.out.println("Enter Data of 1st Stack");
		for(int i=0;i<s1.maxSize;i++){
			s1.push(Integer.parseInt(br.readLine()));
		}
		System.out.println("Enter the Size of 2nd stack");
		Stack s2 = new Stack(Integer.parseInt(br.readLine()));

		System.out.println("Enter The Data of 2nd Stack");
		for(int i=0;i<s2.maxSize;i++){
			s2.push(Integer.parseInt(br.readLine()));
		}

		s1.printStack();
		s2.printStack();

		Stack s3 = Stack.MergeSort(s1,s2);
		s3.printStack();



	/*	char ch;
		do{
			System.out.println("Stack.....!");
			System.out.println("1.push");
			System.out.println("2.pop");
			System.out.println("3.peek");
			System.out.println("4.empty");
			System.out.println("5.size");
			System.out.println("6.print");

			System.out.println("Enter Your Choice");
			int choice = Integer.parseInt(br.readLine());

			switch(choice){
				case 1:{
					       System.out.println("Enter The Data");
					       int data = Integer.parseInt(br.readLine());
					       s.push(data);
					}
					break;
				case 2:{
					       int data = s.pop();
					       if(data != -1)
					       		System.out.println(data+" is Poped");
					}
				       break;
				case 3:
				       	{
						int ret = s.peek();
						if(ret != -1){
							System.out.println("peek : "+s.peek());
						}
					}
					break;
				case 4:
					{
						if(s.isEmpty()){
							System.out.println("Stack is Empty");
						}else{
							System.out.println("Stack is Not Empty");
						}
					}
					break;
				case 5:
					{
						System.out.println("Size : " + s.size());
					}
					break;
				case 6:
					{
						s.printStack();
					}
					break;
				default:
					{
						System.out.println("Wrong Choice");
						break;
					}
			}

			System.out.println("Do You Want To Continue..?");
			ch = br.readLine().charAt(0);

		}while(ch == 'y' || ch == 'Y');
		*/

	}
}

