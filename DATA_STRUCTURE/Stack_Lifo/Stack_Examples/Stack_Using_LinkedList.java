import java.io.*;
/*class Node{
	int data;
	Node next =  null;

	Node(int data){
		this.data = data;
	}
}
*/
class Stack{
class Node{
	int data;
	Node next =  null;

	Node(int data){
		this.data = data;
	}
}
	Node top = null;
	

	void push(int data){
		Node newNode = new Node(data);

		if(top == null){
			top = newNode;
			
		}else{
			newNode.next = top;
			top = newNode;
		}
	}

	int pop(){
		int ret = top.data;
		top=top.next;
		return ret;
	}

	int peek(){
		return top.data;
	}

	boolean empty(){
		if(top == null){
			return true;
		}else{
			return false;
		}
	}

	void print(){
		if(top == null){
			System.out.println("Stack Underflow");
			return;
		}else{
			System.out.print("[");
			Node temp = top;
			while(temp.next!=null){
				System.out.print(temp.data+" , ");
				temp = temp.next;
			}
			System.out.println(temp.data+"]");
		}
	}
}
class Client{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		char ch;
		Stack st = new Stack();

		do{
			System.out.println("Stack By Using LinkedList");
			System.out.println("1.push");
			System.out.println("2.pop");
			System.out.println("3.empty");
			System.out.println("4.print");

			System.out.println("Enter Your Choice");
			int choice = Integer.parseInt(br.readLine());

			switch(choice){
				case 1:
					{
						System.out.println("Enter Data");
						st.push(Integer.parseInt(br.readLine()));
					}
					break;
				case 2:
					{
						if(st.empty()){
							System.out.println("Stack is Underflow");
						}else{
							System.out.println(st.pop()+" Popped");
						}
					}
					break;
				case 3:
					{
						if(st.empty()){
							System.out.println("Stack is Underflow");
						}else{
							System.out.println(st.peek() + " Peek");
						}
					}
					break;
				case 4:
					{
						st.print();
					}
					break;
				default:
					{
						System.out.println("Wrong Choice");
					}
					break;
			}
			System.out.println("Do You Want to Continue...?");
			ch = br.readLine().charAt(0);
		}while(ch=='y' || ch == 'Y');
	}
}

						



