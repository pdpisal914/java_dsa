//Implement the Two Stacks using SIngle Array


import java.io.*;
class TwoStack{
	int size;
	int arr[];
	int top1 = -1;
	int top2;

	TwoStack(int size){
		this.size = size;
		arr=new int[size];
		top2 = size;
	}
	boolean empty1(){
		if(top1+1 != top2){
			return true;
		}else{
			return false;
		}
	}
	boolean empty2(){
		if(top2-1 != top1){
			return true;
		}else{
			return false;
		}
	}
	
	void push1(int data){
		if(empty1()){
			top1++;
			arr[top1]=data;
		}else{
			System.out.println("Stack Overflow");
		}
	}
	void push2(int data){
		if(empty2()){
			top2--;
			arr[top2]=data;
		}else{
			System.out.println("Stack 2 Overflow");
		}
	}

	int pop1(){
		int ret = arr[top1];
		top1--;
		return ret;
	}
	int pop2(){
		int ret = arr[top2];
		top2++;
		return ret;
	}

	int peek1(){
		return arr[top1];
	}
	int peek2(){
		return arr[top2];
	}

	void print1(){
		if(top1 == -1){
			System.out.println("Stack 1 Empty");
			return;
		}else{
			System.out.print("[");
			for(int i=0;i<=top1;i++){
				if(i!=top1){
					System.out.print(arr[i]+" , ");
				}else{
					System.out.print(arr[i]);
				}
			}
			System.out.println("]");
		}
	}	

	void print2(){
		if(top2 == size){
			System.out.println("Stack 2 Empty");
			return;
		}else{
			System.out.print("[");
			for(int i=size-1;i>=top2;i--){

				if(i!=top2){
					System.out.print(arr[i]+" , ");
				}else{
					System.out.print(arr[i]);
				}
			}
			System.out.println("]");
		}
	}
}
class Client{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter The Size of Array");
		int size = Integer.parseInt(br.readLine());

		TwoStack ts = new TwoStack(size);
		char ch;

		do{
			System.out.println("Two Stacks On Single Array");
			System.out.println("1.push1");
			System.out.println("2.push2");
			System.out.println("3.pop1");
			System.out.println("4.pop2");
			System.out.println("5.peek1");
			System.out.println("6.peek2");
			System.out.println("7.empty1");
			System.out.println("8.empty2");
			System.out.println("9.print1");
			System.out.println("10.print2");

			System.out.println("\nEnter Your Choice");
			int choice = Integer.parseInt(br.readLine());

			switch(choice){
				case 1:
					{
						System.out.println("Enter data");
						ts.push1(Integer.parseInt(br.readLine()));
					}
					break;
				case 2:
					{
						System.out.println("Enter data");
						ts.push2(Integer.parseInt(br.readLine()));
					}
					break;
				case 3:
					{
						if(ts.empty1() != true){
							System.out.println(ts.pop1() + " popped");
						}else{
							System.out.println("Stack Underflow");
						}
					}
					break;
				case 4:
					{
						if(ts.empty2() != true){
							System.out.println(ts.pop2() + " popped");
						}else{
							System.out.println("Stack Underflow");
						}
					}
					break;
				case 5:
					{
						if(ts.empty1()){
							System.out.println("Stack 1 Empty");
						}else{
							System.out.println(ts.peek1()+" peek 1");
						}
					}
					break;
				case 6:
					{
						if(ts.empty2()){
							System.out.println("Stack 2 Empty");
						}else{
							System.out.println(ts.peek2()+" peek 2");
						}
					}
					break;
				case 7:
					{
						if(ts.empty1()){
							System.out.println("Stack 1 Empty");
						}else{
							System.out.println("Stack 1 not Empty");
						}
					}
					break;
				case 8:
					{
						if(ts.empty2()){
							System.out.println("Stack 2 Empty");
						}else{
							System.out.println("Stack 2 not Empty");
						}
					}
					break;
				case 9:
					{
						ts.print1();
					}
					break;
				case 10:
					{
						ts.print2();
					}
					break;
				default:
					{
						System.out.println("Wrong Choice");
					}
					break;

			}

			System.out.println("Do You Want to Continue...?");
			ch = br.readLine().charAt(0);
		}while(ch == 'y' || ch == 'Y');
	}
}



