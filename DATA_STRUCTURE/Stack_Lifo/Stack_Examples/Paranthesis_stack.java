import java.io.*;
import java.util.*;
class Paranthesis{
	String str = null;
	Paranthesis(String str){
		this.str = str;
	}

	boolean checkExpression(){

		Stack<Character> s = new Stack<Character>();


		for(int i=0;i<str.length();i++){
			char ch = str.charAt(i);

			if(ch == '(' || ch == '{' || ch == '['){
				s.push(ch);
			}else{
				if(s.empty()){
					return false;
				}else{
					char p = s.peek();
					if(( p == '(' && ch == ')') || (p == '[' && ch == ']') || (p == '{' && ch == '}')){
						s.pop();
					}else{
						return false;
					}
				}
			}
		}

		if(s.empty()){
			return true;
		}else{
			return false;
		}
		
	}

}
class Client{
	public static void main(String [] pdp) throws IOException{
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter The Expression");
		String str = sc.nextLine();

		Paranthesis ps = new Paranthesis(str);
		if(ps.checkExpression()){
			System.out.println("Balanced Expression");
		}else{
			System.out.println("Not Balanced Expression");
		}
	}
}
