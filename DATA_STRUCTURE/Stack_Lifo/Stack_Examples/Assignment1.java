//Immediate smaller element in array

import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader( new InputStreamReader(System.in));
		System.out.println("Enter The Array Size");
		int arr[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter The Array Elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());

			if(i!=0){
				if(arr[i-1] > arr[i]){
					arr[i-1]=arr[i];
				}else{
					arr[i-1]=-1;
				}
			}
			if(i==arr.length-1){
				arr[i]=-1;
			}
		}

		System.out.println("Output : ");
		for(int i=0;i<arr.length;i++){
			System.out.println(arr[i]);
		}
	}
}



