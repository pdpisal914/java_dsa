//Implement the stack by using array

import java.io.*;
class Stack{
	int top = -1;
	int size;
	int arr[];

	Stack(int size){
		this.size = size;
		arr=new int[size];
	}

	void push(int data){

		if(top != size){
			top++;
			arr[top]=data;
		}else{
			System.out.println("Stack Overflow");
		}
	}
	int pop(){
			int ret = arr[top];
			top--;
			return ret;
	}

	int peek(){
		return arr[top];
	}
	void print(){
		if(empty()){
			System.out.println("Stack Is Empty");
			return;
		}else{
			System.out.print("[");
			for(int i=0;i<=top;i++){
				if(i!=top){
					System.out.print(arr[i]+" , ");
				}else{
					System.out.print(arr[i]);
				}
			}
			System.out.println("]");
		}
	}

	boolean empty(){
		if(top != size-1){
			return true;
		}else{
			return false;
		}
	}
}

class Client{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter The Size OF Array");
		Stack s = new Stack(Integer.parseInt(br.readLine()));

		char ch;

		do{
			System.out.println("Implementation Of Stack");
			System.out.println("1.push");
			System.out.println("2.pop");
			System.out.println("3.peek");
			System.out.println("4.empty");
			System.out.println("5.print");

			System.out.println("Enter Yopur Choice");
			int choice = Integer.parseInt(br.readLine());

			switch(choice){
				case 1:
					{
						System.out.println("Enter The data");
						int data = Integer.parseInt(br.readLine());
						s.push(data);
					}
				       break;
				case 2:
				       {
					       if(s.empty() != true){
						       int ret = s.pop();
						       System.out.println(ret+" popped");
					       }else{
						       System.out.println("Stack is Empty");
					       }
				       }
				       break;
				case 3:
				       {
					       if(s.empty() != true){
						       int ret = s.peek();
						       System.out.println(ret+" is peek");
					       }else{
						       System.out.println("Stack is Empty");
					       }
				       }
				       break;
				case 4:
				       {
					       if(s.empty()){
						       System.out.println("Stack is Empty");
					       }else{
						       System.out.println("Stack is Not empty");
					       }
				       }
				       break;
				case 5:
				       {
					       s.print();
				       }
				       break;
				default:
				       {
					       System.out.println("Wrong Choice");
				       }
				       break;
			}

			System.out.println("Do you want to continue...?");
			System.out.println(" Yes : 'y' or 'Y' ");
			ch = br.readLine().charAt(0);
		}while(ch == 'y' || ch == 'Y');
	}
}


