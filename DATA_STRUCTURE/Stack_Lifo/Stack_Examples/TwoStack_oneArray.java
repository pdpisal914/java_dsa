//Create two stacks on single array


import java.io.*;

class TwoStacks{
	int size ;
	int array[];
	int top1 = -1;
	int top2;

	TwoStacks(int size){
		this.size = size;
		array = new int[size];
		top2 = size;
	}

	void push1(int data){
		if(top1+1 == top2){
			System.out.println("Stack 1 Overflow");
			return;
		}else{
			top1++;
			array[top1]=data;
			System.out.println(array[top1]);
		}
	}

	void push2(int data){
		if(top1+1 == top2){
			System.out.println("Stack 2 Overflow");
			return;
		}else{
			top2--;
			array[top2]=data;
			System.out.println(array[top2]);
		}
	}

	int pop1(){
		if(top1 == -1){
			System.out.println("Stack UnderFlow");
			return -1;
		}else{
			int ret = array[top1];
			top1--;
			return ret;
		}
	}
	int pop2(){
		if(top2 == size){
			System.out.println("Stack 2 Underflow");
			return -1;
		}else{
			int ret = array[top2];
			top2++;
			return ret;
		}
	}

	boolean empty1(){

		if(top1 == -1){
			return true;
		}else{
			return false;
		}
	}
	boolean empty2(){

		if(top2 == size){
			return true;
		}else{
			return false;
		}
	}
	int peek1(){
		if(empty1()){
			System.out.println("Stack 1 is Empty");
			return -1;
		}else{
			return array[top1];
		}
	}

	int peek2(){
		if(empty2()){
			System.out.println("Stack 2 is Empty");
			return -1;
		}else{
			return array[top2];
		}
	}
	void print1(){
		if(empty1()){
			System.out.println("Stack 1 Empty");
			return;
		}else{
			System.out.print("[");
			for(int i=0;i<=top1;i++){
				if(i<top1){
					System.out.print(array[i]+" , ");
				}else{
					System.out.print(array[i]);
				}
			}
			System.out.println("]");
		}
	}
	void print2(){
		if(empty2()){
			System.out.println("Stack 2 Empty");
			return;
		}else{
			System.out.print("[");
			for(int i=size-1;i>=top2;i--){
				if(i>top2){
					System.out.print(array[i]+" , ");
				}else{
					System.out.print(array[i]);
				}
			}
			System.out.println("]");
		}
	}
}

class Client{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		char ch;
		System.out.println("Enter The Array Size");
		int size = Integer.parseInt(br.readLine());

		TwoStacks ts = new TwoStacks(size);


		do{
			System.out.println("Two Stacks Using One Array");
			System.out.println("1.push1");
			System.out.println("2.push2");
			System.out.println("3.pop1");
			System.out.println("4.pop2");
			System.out.println("5.peek1");
			System.out.println("6.peek2");
			System.out.println("7.empty1");
			System.out.println("8.empty2");
			System.out.println("9.print1");
			System.out.println("10.print2");

			System.out.println("Enter Your Chpoice");
			int choice = Integer.parseInt(br.readLine());

			switch(choice){
				case 1:
					{
					       System.out.println("Enter data");
					       ts.push1(Integer.parseInt(br.readLine()));
					}
					break;
				case 2:
					{
						System.out.println("Enter Data");
						ts.push2(Integer.parseInt(br.readLine()));
					}
					break;
				case 3:
					{
						int ret = ts.pop1();
						if(ret != -1){
							System.out.println(ret+" popped 2");
						}
					}
					break;
				case 4:
					{
						int ret = ts.pop2();
						if(ret != -1){
							System.out.println(ret+" popped 2");
						}
					}
					break;
				case 5:
					{
						int ret = ts.peek1();
						if(ret != -1){
							System.out.println(" peek1 : "+ ret);
						}
					}
					break;
				case 6:
					{
						int ret = ts.peek2();
						if(ret != -1){
							System.out.println(" peek2 : "+ret);
						}
					}
					break;
				case 7:
					{
						if(ts.empty1()){
							System.out.println("Stack 1 Empty");
						}else{
							System.out.println("Stack 1 Not Empty");
						}
					}
					break;
				case 8:
					{
						if(ts.empty2()){
							System.out.println("Stack 2 Empty");
						}else{
							System.out.println("Stack 2 Not Empty");
						}
					}
					break;
				case 9:
					{
					       ts.print1();
					}	
					break;
				case 10:
					{
						ts.print2();
					}
					break;
				default:
					{
						System.out.println("Wrong Choice");
					}
					break;
			}

			System.out.println("Do You Want To Continue...?");
			System.out.println("Yes = y || Y ");
			ch = br.readLine().charAt(0);

		}while(ch == 'y' || ch == 'Y');
	}
}





