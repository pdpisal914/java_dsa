//Print The Product Of The Digits Of The Given Number

import java.util.*;
class Demo{
	static int pro(int num){
		if(num==0){
			return 1;
		}

		return pro(num/10)*num%10;
	}
	public static void main(String []pdp){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter The Number");
		int num = sc.nextInt();

		System.out.println("Product : "+pro(num));
	}
}
