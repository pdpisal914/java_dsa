//WAP to check the given Number Is Composite Number or Not

import java.util.*;
class Demo{
	static int x =2;
	static boolean Compo(int num){
		if(x>num/2){
			return false;
		}
		if(num%x==0 && x<=num/2){
			return true;
		}
		++x;
		return Compo(num);
	}

	public static void main(String [] pdp){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter The nUmber");
		
		int num = sc.nextInt();

		if(Compo(num)){
			System.out.println(num+" is Composite Number");
		}else{
			System.out.println(num+" is Not Composite Number");
		}
	}
}
