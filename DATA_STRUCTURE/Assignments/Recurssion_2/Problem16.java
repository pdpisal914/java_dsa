//WAP to check the Number is Cubic number or not

import java.util.*;
class Demo{
	static int x =1;
	static boolean Cubic(int num){
		if(x*x*x > num){
			return false;
		}

		if(x*x*x==num){
			return true;
		}
		++x;
		return Cubic(num);
	}
	public static void main(String [] pdp){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter The Number");
		int num = sc.nextInt();

		if(Cubic(num)){
			System.out.println(num+" is Cubic Number");
		}else{
			System.out.println(num+" is Not Cubic Number");
		}
	}
}
