//Print The Factorial of Given Number

import java.util.*;
class Demo{
	static int fact(int num ){
		if(num==0){
			return 1;
		}
		return num*(fact(num-1));
	}
	public static void main(String [] pdp){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter The Number");
		int num = sc.nextInt();

		System.out.println("Factorial = "+fact(num));
	}
}
