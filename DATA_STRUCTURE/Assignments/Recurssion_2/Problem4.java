//Print the sum of the odd numbers up to the given number

import java.util.*;
class Demo{
	static int x = 1;
	static int sum=0;
	static int Odd(int num){
		if(x>num){
			return sum;
		}
		if(x%2!=0){
			sum = sum+x;
		}
		++x;
		return Odd(num);
	}
	public static void main(String [] pdp){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter The Number");
		int num = sc.nextInt();

		System.out.println("Sum Of Odd Digits : "+Odd(num));
	}
}
