//Print Maximum digit in thge Number

import java.util.*;
class Demo{
	static int max = Integer.MIN_VALUE;
	static int Max(int num){
		if(num==0){
			return max;
		}
		if(max<num%10){
			max=num%10;
		}
		return Max(num/10);
	}
	public static void main(String [] pdp){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter The Number");
		int num = sc.nextInt();

		System.out.println("Maximum Digit = "+Max(num));
	}
}
