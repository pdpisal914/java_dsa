//WAP to check whether the given number is Magic Number or not
//magic num=1234=1+2+3+4=10=1+0=1 hence it is magic num
import java.util.*;
class Demo{
	static int Sum(int num){
		if(num==0){
			return 0;
		}
		return num%10+Sum(num/10);
	}
	static boolean Magic(int num){
		int sum = Sum(num);
		System.out.println(sum);
		if(sum==1){
			return true;
		}else if(sum>1 && sum<10){
			return false;
		}else{
			return Magic(sum);
		}
	}
	public static void main(String [] pdp){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter The Number");
		int num = sc.nextInt();

		if(Magic(num)){
			System.out.println(num+" is Magic Number");
		}else{
			System.out.println(num+" is Not Magic Number");
		}
	}
}
