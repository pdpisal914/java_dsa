//WAP to check the given number is Triangular Number or Not

import java.util.*;
class Demo{
	static int n =1;
	static boolean Tri(int num){
		int pro=(n*(n+1))/2;

		if(pro>num){
			return false;
		}
		if(pro==num){
			return true;
		}
		++n;

		return Tri(num);
	}
	public static void main(String [] pdp){
		Scanner sc = new Scanner(System.in);
		System.out.println("{Enter The Number");
		int num = sc.nextInt();

		if(Tri(num)){
			System.out.println(num+" is Triangular Number");
		}else{
			System.out.println(num+" is Not Triangular Number");
		}
	}
}
