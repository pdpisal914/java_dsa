//WAP to check the given number is AutoMorphic Number or Not

import java.util.*;
class Demo{
	static boolean Auto(int num,int square){
		if(num==0){
			return true;
		}

		if(num%10 != square%10){
			return false;
		}else{
			return Auto(num/10,square/10);

		}
	}
	public static void main(String [] pdp){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter The Number");
		int num=sc.nextInt();

		if(Auto(num,num*num)){
			System.out.println(num+" is Automorphic Number");
		}else{
			System.out.println(num+" is Not Automorphic");
		}
	}

}

