//WAP to Check the given number is Happy Number or Not
//

import java.util.*;
class Demo{
	static int Sum(int num){
		if(num==0){
			return 0;
		}
		return (num%10)*(num%10) + Sum(num/10);
	}
	static boolean Happy(int num){
		int sum = Sum(num);
		if(sum==1){
			return true;
		}else if(sum>1&&sum<10){
			return false;
		}else{
			return Happy(sum);
		}
	}
	public static void main(String [] pdp){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter The Number");
		int num = sc.nextInt();

		if(Happy(num)){
			System.out.println(num+" is Happy Number");
		}else{
			System.out.println(num+" is Not Happy Number");
		}
	}
}
