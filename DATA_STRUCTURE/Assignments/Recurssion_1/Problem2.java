//WAP to print 10 natural numbers in reverse order

import java.io.*;
class Demo{
	void fun(int num){
		if(num == 0){
			return ;
		}
		System.out.println(num);

		fun(num-1);
	}
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter The Number");
		int num=Integer.parseInt(br.readLine());

		Demo obj = new Demo();
		obj.fun(num);
	}
}
