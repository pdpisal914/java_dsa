//WAP to print check the number is prime or not

import java.io.*;
class Demo{
	int x = 2;
	boolean Prime(int num){
		if(num%x==0 && x<=num/2){
			return false;
		}else if(x==num/2){
			return true;
		}else{
			++x;
			return Prime(num);
		}

	}
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter The Number");
		int num=Integer.parseInt(br.readLine());

		Demo obj = new Demo();
		if(obj.Prime(num)){
			System.out.println("It Is Prime Number");
		}else{
			System.out.println("It Is Not Prime Number");
		}
	}
}
