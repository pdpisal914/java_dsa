//WAP to print the count of specific digit in the given Number

import java.io.*;
class Demo{
	int rev =0;
	int Rev(int num){
		if(num==0){
			return rev;
		}

		rev = rev*10 + num%10;
		return Rev(num/10);
	}
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter The Number");
		int num=Integer.parseInt(br.readLine());

		

		Demo obj = new Demo();
		int rev = obj.Rev(num);
		if(rev==num){
			System.out.println(num+" is Pallindrome Number");
		}else{
			System.out.println(num+" is Not Pallindrome");
		}
	}
}
