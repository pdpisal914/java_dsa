//WAP to print the sun of n numbers

import java.io.*;
class Demo{
	int fact(int num){
		if(num == 1){
			return 1;
		}

		return fact(num-1)*num;
	}
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter The Number");
		int num=Integer.parseInt(br.readLine());

		Demo obj = new Demo();
		System.out.println("Factorial = "+obj.fact(num));
	}
}
