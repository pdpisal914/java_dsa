//WAP to print the count of specific digit in the given Number

import java.io.*;
class Demo{
	int cnt =0;
	String Rev(String str){
		if(str==""){
			return str;
		}

		return Rev(str.substring(1))+str.charAt(0);
	}
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter The String");
		String str=br.readLine();

		Demo obj = new Demo();
		String rev = obj.Rev(str);
		System.out.println("Reverse String = "+rev);
	}
}
