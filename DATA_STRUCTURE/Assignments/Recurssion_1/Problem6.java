//WAP to print 10 natural numbers in reverse order

import java.io.*;
class Demo{
	int fun(int num){
		if(num == 0){
			return 0 ;
		}
		return num%10+fun(num/10);
	}
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter The Number");
		int num=Integer.parseInt(br.readLine());

		Demo obj = new Demo();
		int sum =obj.fun(num);

		System.out.println("Sum = "+sum);
	}
}
