//WAP to print the count of specific digit in the given Number

import java.io.*;
class Demo{
	int cnt =0;
	int Count(int num,int digit){
		if(num==0){
			return cnt;
		}

		if(num%10==digit){
			cnt++;
		}
		return Count(num/10,digit);
	}
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter The Number");
		int num=Integer.parseInt(br.readLine());

		System.out.println("Enter The Digit");
		int digit = Integer.parseInt(br.readLine());

		Demo obj = new Demo();
		int count = obj.Count(num,digit);
		System.out.println("Count = "+count);
	}
}
