//WAP to print the numbers from 1 to 100

import java.io.*;
class Demo{
	int fun(int num){
		if(num == 1){
			return 1;
		}

		return fun(num-1)+num;
	}
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter The Number");
		int num=Integer.parseInt(br.readLine());

		Demo obj = new Demo();
		System.out.println("Sum = "+obj.fun(num));
	}
}
