import java.io.*;

class Node{
	int data;
	Node next = null;

	Node(int data){
		this.data = data;
	}
}

class LinkedList{
	Node head = null;

	void addLast(int data){
		Node newNode = new Node(data);

		if(head == null){
			head = newNode;
		}else{
			Node temp = head;
			while(temp.next!=null){
				temp = temp.next;
			}
			
			temp.next = newNode;
		}
	}

	void printLL(){
		if(head == null){
			System.out.println("Linked List is Empty");
		}else{
			Node temp = head;

			while(temp.next != null){
				System.out.print(temp.data+" -> ");
				temp = temp.next;
			}
			System.out.println(temp.data);
		}
	}

	Node impRev(Node currentNode){
		if(currentNode.next == null){
			head = currentNode;
			return currentNode;
		}else{
			return impRev(currentNode.next).next=currentNode;
		//	Node temp = impRev(currentNode.next);
		//	temp.next = currentNode;
		//	return currentNode;
		}
	}
	void ImplaceReverse(){
		Node lastNode=impRev(head);
		lastNode.next = null;
	}

	Node kReverse(Node head , int k){

		Node prev = null;
		Node curr = head;
		Node forword = null;
		int cnt =0;

		while(curr != null && cnt<k){
			forword = curr.next;
			curr.next = prev;
			prev = curr;
			curr = forword;
			cnt++;
		}

		if(curr != null){
			head.next = kReverse(forword,k);
		}
		return prev;
	}
}
class Client{
	public static void main(String [] pdp) throws IOException{
		LinkedList ll = new LinkedList();
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter The Number of Nodes");
		int cnt = Integer.parseInt(br.readLine());
		for(int i=1;i<=cnt;i++){
			System.out.println("Enter the data of Node "+ i);
			ll.addLast(Integer.parseInt(br.readLine()));
		}
		ll.printLL();

		//ll.ImplaceReverse();

		//ll.printLL();

		ll.head = ll.kReverse(ll.head,2);

		ll.printLL();
	}
}
			
