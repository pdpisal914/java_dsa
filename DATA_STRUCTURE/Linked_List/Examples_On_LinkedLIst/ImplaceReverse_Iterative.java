import java.io.*;

class Node{
	int data;
	Node next = null;

	Node(int data){
		this.data = data;
	}
}

class LinkedList{
	Node head = null;

	void addLast(int data){
		Node newNode = new Node(data);

		if(head == null){
			head = newNode;
		}else{
			Node temp = head;
			while(temp.next!=null){
				temp = temp.next;
			}
			
			temp.next = newNode;
		}
	}

	void printLL(){
		if(head == null){
			System.out.println("Linked List is Empty");
		}else{
			Node temp = head;

			while(temp.next != null){
				System.out.print(temp.data+" -> ");
				temp = temp.next;
			}
			System.out.println(temp.data);
		}
	}

	
	void ImplaceReverse(){

		if(head == null){
			System.out.println("Linked List is Empty");
			return ;
		}else{
			Node prev = null;
			Node current = head;
			Node forword = null;
	
			while(current != null){
				forword = current.next;
				current.next = prev;
				prev = current;
				current = forword;
			}
			head = prev;
		}
	}
}
class Client{
	public static void main(String [] pdp) throws IOException{
		LinkedList ll = new LinkedList();
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter The Number of Nodes");
		int cnt = Integer.parseInt(br.readLine());
		for(int i=1;i<=cnt;i++){
			System.out.println("Enter the data of Node "+ i);
			ll.addLast(Integer.parseInt(br.readLine()));
		}
		ll.printLL();

		ll.ImplaceReverse();

		ll.printLL();
	}
}
			
