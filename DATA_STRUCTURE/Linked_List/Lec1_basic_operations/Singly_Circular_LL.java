import java.io.*;
class Node{
	int data;
	Node next = null;

	Node(int data){
		this.data = data;
	}
}
/*
class LinkedList{

	Node head = null;
	
	void addFirst(int data){
		Node newNode = new Node(data);

		if(head == null){
			head = newNode;
		}else{
			newNode.next = head;
			head = newNode;
		}
	}

	void addLast(int data){
		Node newNode = new Node(data);

		if(head == null){
			head = newNode;
		}else{
			Node temp = head;

			while(temp.next !=null){
				temp=temp.next;
			}
			temp.next=newNode;
		}
	}

	void addAtPos(int pos,int data){
		int count = countNode();

		if(pos<1 || pos>count+1){
			System.out.println("Invalid Node Position");
			return ;
		}

		Node newNode = new Node(data);

		if(pos == 1){
			addFirst(data);
		}else if(pos == count+1){
			addLast(data);
		}else{
			Node temp = head;

			while(pos-2 != 0){
				temp=temp.next;
			}
			newNode.next = temp.next;
			temp.next = newNode;
		}
	}

	int countNode(){
		int cnt = 0;
		if(head == null){
			return 0;
		}else{
			Node temp = head;

			while(temp != null){
				temp=temp.next;
				cnt++;
			}
		}
		return cnt;
	}

	void printLL(){
		if(head == null){
			System.out.println("Linked List is Empty");
		}else{
			Node temp = head;
			
			while(temp!=null){
				if(temp.next!=null){
					System.out.print(temp.data+" -> ");
				}else{
					System.out.println(temp.data);
				}
				temp=temp.next;
			}

		}
	}

	void deletFirst(){
		if(head == null){
			System.out.println("Invalid Operation");
		}else{
			head = head.next;
		}
	}
	void deletLast(){
		if(head == null){
			System.out.println("Invalid Operation(i.e.Empty LL)");
		}else{
			Node temp = head;
			while(temp.next.next != null){
				temp=temp.next;
			}
			temp.next = null;
		}
	}
	 
	void deletAtPos(int pos){
		if(head == null){
			System.out.println("Invalid Operation On Empty LL");
		}else{
			Node temp = head;
			while(pos-2 != 0){
				temp=temp.next;
			}
			temp.next = temp.next.next;
		}
	}
}
*/

class LinkedList{
	Node head = null;

	void addFirst(int data){
		Node newNode = new Node(data);
		if(head == null){
			head = newNode;
			newNode.next = head;
		}else{
			Node temp = head;
			while(temp.next != head){
				temp=temp.next;
			}
			temp.next = newNode;
			newNode.next = head;
			head = newNode;
		}
	}

	void addLast(int data){
		Node newNode = new Node(data);

		if(head == null){
			head = newNode;
			newNode.next = head;
		}else{
			Node temp = head;

			while(temp.next != head){
				temp = temp.next;
			}
			newNode.next = head;
			temp.next = newNode;
		}
	}

	void addAtPos(int pos,int data){
		Node newNode = new Node(data);

		int count = countNode();
		if(pos<1 || pos>count+1){
		       System.out.println("Invalid Position");
			return ;
		}

		if(head == null){
			head = newNode;
			newNode.next = head;
		}else{
			Node temp = head;
			while(pos-2 != 0){
				temp = temp.next;
				pos--;
			}
			newNode.next=temp.next;
			temp.next=newNode;
		}
	}

	 void deletFirst(){

		 if(head == null){
			 System.out.println("Empty Linked List");
			 return;
		 }else{
			 if(head.next == head){
				 head = null;
			 }else{
			 	Node temp = head;
			 	while(temp.next != head){
					 temp = temp.next;
				 }
				 temp.next = head.next;
				 head = head.next;
		 	}
		 }
	 }
	 void deletLast(){
		 if(head == null){
			 System.out.println("Empty Linked List");
			 return;
		 }else if(head.next == null){
			 head = null;
		 }else{
			 Node temp = head;
			 while(temp.next.next != head){
				 temp=temp.next;
			 }

			 temp.next = head;
		 }
	 }

	 void deletAtPos(int pos){
		 int count = countNode();

		 if(pos<1 || pos>count){
			 System.out.println("Invalid Position");
			 return;
		 }

		 if(head == null){
			 System.out.println("Empty LL");
		 }else if(pos == 1){
			 deletFirst();
		 }else if(pos == count){
			 deletLast();
		 }else{
			 Node temp = head;
			 while(pos-2 != 0){
				 temp = temp.next;
				 pos--;
			 }
			 temp.next = temp.next.next;
		 }
	 }

	 int countNode(){
		 int count = 1;

		 if(head == null){
			 return 0;
		 }else{
			 Node temp = head;

			 while(temp.next != head){
				System.out.println(count);
				 temp=temp.next;
				 count++;
			 }

			 return count;
		 }
	 }

	 void printLL(){
		 if(head == null){
			 System.out.println("Linked List is Empty");
		 }else{
			 Node temp = head;

			 while(temp.next != head){
				 System.out.print(temp.data+" -> ");
				 temp = temp.next;
			 }
			 System.out.println(temp.data);
		 }
	 }
}




			 


class Client{
	public static void main(String [] pdp) throws IOException{
		LinkedList ll = new LinkedList();
		char ch;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		

		do{
			System.out.println("Singly Linked List");
			System.out.println("1.addFirst");
			System.out.println("2.addLast");
			System.out.println("3.addAtPos");
			System.out.println("4.deletFirst");
			System.out.println("5.deletLast");
			System.out.println("6.deletAtPos");
			System.out.println("7.countNode");
			System.out.println("8.printNode");

			System.out.println("Enter Your Choice");
			int choice = Integer.parseInt(br.readLine());

		switch(choice){
			case 1:{
			       System.out.println("Enter Data");
				int data = Integer.parseInt(br.readLine());
		 		ll.addFirst(data);
				}
	       			break;
			case 2:{
				       System.out.println("Enter Data");
				       int data = Integer.parseInt(br.readLine());
				       ll.addLast(data);
				}
				break;
			case 3:{
				       System.out.println("Enter Position");
				       int pos = Integer.parseInt(br.readLine());
				       System.out.println("Enter The data");
				       int data = Integer.parseInt(br.readLine());

				       ll.addAtPos(pos,data);
				}
			       break;
			case 4:
			       {
				       ll.deletFirst();
			       }
			       break;
			case 5:
			       {
				       ll.deletLast();
			       }
			       break;
			case 6:
			       {
				       System.out.println("Enter The Position");
				       int pos = Integer.parseInt(br.readLine());
				       ll.deletAtPos(pos);
			       }
			       break;
			case 7:{
				       System.out.println("Count = "+ll.countNode());
				}
				break;
			case 8:
				{
					ll.printLL();
				}
				break;
			default:
				System.out.println("Wrong Choice");
				break;
		}

		System.out.println("Do You Want To Continue...?");
		System.out.println("Yes : 'y' || 'Y'");
		System.out.println("No : 'n' || 'N'");

		ch = br.readLine().charAt(0);

	}while(ch=='y' || ch == 'Y');
	}
}



	       							       
			
