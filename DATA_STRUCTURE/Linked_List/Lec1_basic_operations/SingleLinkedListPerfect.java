import java.util.*;
import java.lang.*;
import java.io.*;
class Node{
	int data;
	Node next = null;

	Node(int data){
		this.data = data;
	}
}

class LinkedList{
	Node head = null;

	void addFirst(int data){
		Node newNode = new Node(data);

		if(head == null){
			head = newNode;
		}else{
			newNode.next = head;
			head=newNode;
		}
	}

	int countNode(){
		int count =0;
		if(head == null){
			return 0;
		}else{
			Node temp = head;
			while(temp != null){
				temp = temp.next;
				count++;
			}
			return count;
		}
	}

	void printLinkedList(){
		if(head==null){
			System.out.println("Empty LinkedList");
		}else{
			Node temp = head;
			while(temp != null){
				if(temp.next!=null){
					System.out.print(temp.data+" -> ");
				}else{
					System.out.println(temp.data);
				}
				temp=temp.next;
			}
		}
	}

	void addLast(int data){
		Node newNode = new Node(data);

		if(head==null){
			head=newNode;
		}else{
			Node temp = head;
			while(temp.next!=null){
				temp=temp.next;
			}
			temp.next = newNode;
		}
	}

	void addAtPos(int pos,int data){
		int count = countNode();

		if(pos<1 || pos>count+1){
			System.out.println("Invalid Position to Insert Data");
			return;
		}

		if(pos ==1 ){
			addFirst(data);
		}else if(pos == count+1){
			addLast(data);
		}else{
			Node temp = head;
			Node newNode = new Node(data);

			while(pos-1 != 0){
				temp = temp.next;
				pos--;
			}
			newNode.next = temp.next;
			temp.next=newNode;
		}
	}



	void deletLast(){
		if(head==null){
			System.out.println("LinkedList Is Empty");
			return;
		}
		Node temp=head;
		if(temp.next==null){
			head = null;
		}else{
			while(temp.next.next != null){
				temp=temp.next;
			}
			temp.next = null;
		}
	}

	void deletFirst(){
		if(head == null){
			System.out.println("Linked List Is Empty");
			return;
		}
		head = head.next;
	}

	void deletAtPos(int pos){

		int count = countNode();
		if(pos<1 || pos>count){
			System.out.println("Invalid Position to Delete Node");
			return;
		}

		if(pos == 1){
			deletFirst();
		}else if(pos == count){
			deletLast();
		}else{
			Node temp = head;
			while(pos-2 != 0){
				temp=temp.next;
				pos--;
			}
			temp.next = temp.next.next;
		}
	}

}
class Client{
	public static void main(String [] pdp) throws IOException{
		LinkedList ll = new LinkedList();
		System.out.println("Enter Number of nodes");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		char ch;
		do{
			System.out.println("Singly Linked List");
			System.out.println("1.addFirst");
			System.out.println("2.addLast");
			System.out.println("3.addAtPos");
			System.out.println("4.deletFirst");
			System.out.println("5.deletLast");
			System.out.println("6.deletAtPos");
			System.out.println("7.countNode");
			System.out.println("8.printLinkedList");

			System.out.println("Enter Your Choice");
			int choice = Integer.parseInt(br.readLine());

			switch(choice){
				case 1:{
					       System.out.println("Enter The Data");
					       int data = Integer.parseInt(br.readLine());
					       ll.addFirst(data);
					}
					break;
				case 2:{
					       System.out.println("Enter The data");
					       int data = Integer.parseInt(br.readLine());
					       ll.addLast(data);
					}
				       break;
				case 3:
				       {
					       System.out.println("Enter The data");
					       int data = Integer.parseInt(br.readLine());

					       System.out.println("Enter The Position");
					       int pos = Integer.parseInt(br.readLine());

					       ll.addAtPos(pos,data);
				       }
				       break;
				case 4:
				       ll.deletFirst();
				       break;
				case 5:
				       ll.deletLast();
				       break;
				case 6:
				       {
					       System.out.println("Enter The Position");
					       int pos = Integer.parseInt(br.readLine());
					       ll.deletAtPos(pos);
				       }
				       break;
				case 7:
				       System.out.println(ll.countNode());
				       break;
				case 8:
				       ll.printLinkedList();
				       break;
				default :
				       System.out.println("Invalid Chice");
				       break;
			}

			System.out.println("\n\nDo You Want To Continue ? \nIf Yes ='Y' or 'y'");
			ch = br.readLine().charAt(0);

		}while(ch == 'Y' || ch == 'y');
	}
	
}
