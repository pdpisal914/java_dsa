//Find the Maximum sum of the Subarray by using The Prefix Sum Approach


class PrefixSum{
	public static void main(String [] pdp){
		int arr[] = new int[]{-2,1,-3,4,-1,2,1,-5,4};

		for(int i=1;i<arr.length;i++){
			arr[i]=arr[i]+arr[i-1];
		}

		int maxSum = Integer.MIN_VALUE;

		for(int i=0;i<arr.length;i++){
			int sum =0;
			for(int j=i;j<arr.length;j++){
				if(i==0){
					sum = arr[j];
				}else{
					sum=arr[j]-arr[i-1];
				}

				if(maxSum<sum){
					maxSum = sum;
				}
			}
		}
		System.out.println("MaxSum By PrefixSum : "+maxSum);
	}
}
