//Print The Maximum sum of the Subarray
//find the contingious subarray containing at least one element which has the largest sum and return its sum
//i/p -{-2,1,-3,4,-1,2,1,-5,4}
//o/p - 6;

class SubArray{
	public static void main(String [] pdp){
		int arr[]={-2,1,-3,4,-1,2,1,-5,4};

		int maxSum = Integer.MIN_VALUE;
		
		for(int i=0;i<arr.length;i++){
			int sum =0;
			for(int j=i;j<arr.length;j++){
				sum=sum+arr[j];
				if(maxSum<sum){
					maxSum = sum;
				}
			}
		}

		System.out.println("MaxSum : "+maxSum);
	}
}
