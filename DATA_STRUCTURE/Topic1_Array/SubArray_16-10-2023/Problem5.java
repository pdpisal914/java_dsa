//By Using Prefix Sum Print Sum Of SubArrays

class ArrayDemo{
	public static void main(String [] pdp){
		int arr[] = {2,4,1,3};

		for(int i=1;i<arr.length;i++){
			arr[i]=arr[i]+arr[i-1];
		}

		for(int i=0;i<arr.length;i++){

			for(int j=i;j<arr.length;j++){

				if(i==0){
					System.out.println(arr[j]);
				}else{
					System.out.println(arr[j]-arr[i-1]);
				}
			}
		}
	}
}

//PREFIX SUM APPROACH
