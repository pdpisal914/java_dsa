//Find the Second maximum Number

class ArrayDemo{
	public static void main(String [] pdp){
		int arr[]=new int[]{8,4,1,3,9,2,6,7};

		int max = Integer.MIN_VALUE;
		int secmax = Integer.MIN_VALUE-1;

		for(int i=0;i<arr.length;i++){

			if(max<arr[i]){
				secmax=max;
				max=arr[i];
			}else if(arr[i]>secmax){
				secmax=arr[i];
			}
		}

		System.out.println("Max : "+max);
		System.out.println("SecMax : "+secmax);
	}
}
