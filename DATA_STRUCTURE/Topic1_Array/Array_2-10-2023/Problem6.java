//print the sum of array elements start from first and last


class ArrayDemo{
	public static void main(String [] pdp){
		int arr[] = new int[]{1,2,3,4,5,6,7};
		int i=0;
		int j = arr.length-1;

		while(i<=j){
			if(i==j){
				System.out.println(arr[i]);
			}else{
				System.out.println(arr[i]+arr[j]);
			}
			j--;
			i++;
		}
	}
}
