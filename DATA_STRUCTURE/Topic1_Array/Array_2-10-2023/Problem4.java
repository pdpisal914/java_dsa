//reverse the array without using extra space

class ArrayDemo{
	public static void main(String [] pdp){
		int arr[]=new int[]{1,2,3,4,5,6};

		int j = arr.length-1;
		int i=0;

		while(i<j){
			int temp = arr[i];
			arr[i]=arr[j];
			arr[j]=temp;
			j--;
			i++;
		}

		for(i=0;i<arr.length;i++){
			System.out.println(arr[i]);
		}
	}
}
