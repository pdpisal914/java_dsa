//Return the count of pairs of (i,j) with array arr[i]+arr[j]=k
//k=10;
//
class ArrayDemo{
	public static void main(String [] pdp){
		int arr[] = new int[]{3,5,2,1,-3,7,8,15,6,13};
		int k=10;
		int cnt=0;

		for(int i=0;i<arr.length;i++){

			for(int j=i+1;j<arr.length;j++){

				if(i!=j && arr[i]+arr[j]==k){
					cnt++;
				}
			}
		}

		System.out.println(cnt*2);
	}
}
