/*
 1).Left Max of i contain the maxium for the index 0 to the index i
arr:[-3,6,2,4,5,2,8,-9,3,1];
n=10;
LeftMax:[-3,6,6,6,6,6,8,8,8,8]
*/

class ArrayDemo{
	static int[] LeftMax(int [] arr){
		int LMax[]=new int[arr.length];

		for(int i=0;i<arr.length;i++){
			int Large = Integer.MIN_VALUE;
			for(int j=0;j<=i;j++){
				if(Large < arr[j]){
					Large = arr[j];
				}
			}
			LMax[i]=Large;
		}

		return LMax;
	}

	public static void main(String [] pdp){

		int arr[] = new int[]{-3,6,2,4,5,2,8,-9,3,1};

		int LMax[]=LeftMax(arr);

		for(int i=0;i<LMax.length;i++){
			System.out.println(LMax[i]);
		}
	}
}

//TC=O(N^2);
//SC=O(N);
