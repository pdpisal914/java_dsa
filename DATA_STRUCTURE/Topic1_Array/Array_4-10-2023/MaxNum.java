//FindMaximum Number

class ArrayDemo{
	static int MaxNum(int arr[],int Index){
		int num=0;
		if(Index<arr.length){
			num=Index;
		}else{
			num=arr.length-1;
		}

		int max = Integer.MIN_VALUE;

		for(int i=0;i<=num;i++){
			if(max<arr[i]){
				max=arr[i];
			}
		}
		return max;
	}
	public static void main(String [] pdp){
		int arr[]=new int[]{3,4,5,1,2,7,9,8};

		int i=5;

		System.out.print("Max Num : "+MaxNum(arr,i));
	}
}

