/*
 1).Left Max of i contain the maxium for the index 0 to the index i
arr:[-3,6,2,4,5,2,8,-9,3,1];
n=10;
LeftMax:[-3,6,6,6,6,6,8,8,8,8]
*/

class ArrayDemo{
	static int[] LeftMax(int [] arr){
		int max= Integer.MIN_VALUE;

		for(int i=0;i<arr.length;i++){
			if(max<arr[i]){
				max=arr[i];
			}else{
				arr[i]=max;
			}

		}

		return arr;
	}

	public static void main(String [] pdp){

		int arr[] = new int[]{-3,6,2,4,5,2,8,-9,3,1};

		int LMax[]=LeftMax(arr);

		for(int i=0;i<LMax.length;i++){
			System.out.println(LMax[i]);
		}
	}
}

//TC+O(N);
//SC+O(1);
