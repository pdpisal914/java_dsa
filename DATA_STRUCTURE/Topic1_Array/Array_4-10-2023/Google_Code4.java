/*
Return the count of pairs(i,j) such that
a)i<j
b)arr[i]='a';
c)arr[j]='g';

arr:[a,b,e,g,a,g]
*/

class ArrayDemo{
	public static void main(String [] pdp){

		char arr[] = new char[]{'a','b','e','g','a','g'};
		int cnt=0;

		for(int i=0;i<arr.length;i++){

			if(arr[i]=='a'){

				for(int j=i+1;j<arr.length;j++){
					if(arr[j]=='g'){
						cnt++;
					}
				}
			}
		}

		System.out.println("O/p : "+cnt);
	}
}
