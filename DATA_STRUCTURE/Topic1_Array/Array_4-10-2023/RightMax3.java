/*
 1).Left Max of i contain the maxium for the index 0 to the index i
arr:[-3,6,2,4,5,2,8,-9,3,1];
n=10;
LeftMax:[-3,6,6,6,6,6,8,8,8,8]
*/

class ArrayDemo{
	static int[] RightMax(int [] arr){
		int max= Integer.MIN_VALUE;

		for(int i=arr.length-2;i>=0;i--){
			if(max<arr[i]){
				max=arr[i];
			}else{
				arr[i]=max;
			}

		}

		return arr;
	}

	public static void main(String [] pdp){

		int arr[] = new int[]{-3,6,2,4,5,2,8,-9,3,1};

		int RMax[]=RightMax(arr);

		for(int i=0;i<RMax.length;i++){
			System.out.println(RMax[i]);
		}
	}
}

//TC+O(N);
//SC+O(1);
