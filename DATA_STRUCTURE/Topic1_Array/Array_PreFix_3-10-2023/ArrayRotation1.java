/*
Array Rotation

Given an array A of size N an integer B,you have to return the same array after nrotating it B times toward the right

CONSTRAINT
	1<=N<=10^5
	1<=A[i]<=10^9
	1<=B<=!0^9
*/

import java.util.*;
class ArrayDemo{
	public static void main(String [] pdp){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter The Array Length");
		int arr[]=new int[sc.nextInt()];

		System.out.println("Enter The Elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}

		System.out.println("Enter The Rotation Count");
		int B = sc.nextInt();

		for(int i=1;i<=B;i++){
			int temp=arr[0];

			for(int j=1;j<arr.length;j++){
				arr[j-1]=arr[j];
			}
			arr[arr.length-1]=temp;
		}
		
		System.out.println("Rotating Array");
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+"   ");
		}
		System.out.println("");
	}
}
