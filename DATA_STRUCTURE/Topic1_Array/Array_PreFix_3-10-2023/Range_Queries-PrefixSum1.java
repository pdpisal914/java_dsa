//Prefix Sum Code

import java.util.*;
class ArrayDemo{
	public static void main(String [] pdp){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter The Array Length");
		int arr[] = new int[sc.nextInt()];

		System.out.println("Enter The Array Elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();

			if(i!=0){
				arr[i]=arr[i]+arr[i-1];
			}
		}

		System.out.println("Enter The Total Number Of Queries");
		int Q = sc.nextInt();

		for(int i=1;i<=Q;i++){
			System.out.println("Enter The Starting Index");
			int s =sc.nextInt();
			System.out.println("Enter The Ending Index");
			int e = sc.nextInt();
			
			int sum =0;
			if(s==0){
				sum = arr[e];
			}else{
				sum=arr[e]-arr[s-1];
			}

			System.out.println("Sum : "+sum);
		}
	}
}

