/* 
Given an array of N size & Q number of queries.
Queries contain two parameters (s,e) s=start,e=end
for all queries ,pribt the sum of all elements from index s to index e
arr:[-3,6,2,4,5,2,8,-9,3,1];
n=10;
q=3;
*/

import java.util.*;
class ArrayDemo{
	public static void main(String [] pdp){
		int arr[] = new int[]{-3,6,2,4,5,2,8,-9,3,1};

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter The No. Of Queries");
		int Q = sc.nextInt();
		
		
		for(int i=0;i<Q;i++){
			System.out.println("Enter The Starting Index");
			int s = sc.nextInt();
			System.out.println("Enter The Ending Index");
			int e = sc.nextInt();
			
			int sum=0;
			for(int j=s;j<=e;j++){
				sum=sum+arr[j];
			}

			System.out.println(sum);
		}
	}
}


