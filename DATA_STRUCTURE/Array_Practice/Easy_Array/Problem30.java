//Arrange the alternative positive and negative elements

import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter The Array Length");
		int arr[] = new int[Integer.parseInt(br.readLine())];
		
		int Negative=0,Positive=0;
		System.out.println("Enter The Array Elements");
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		int arr1[] = new int[arr.length];
		for(int i=1;i<arr.length-1;i++){
			if(arr[i-1]<0 && arr[i]<0){
				int temp1 = arr[i];
				arr[i] = arr[i+1];
				arr[i+1] = temp1;
			}else if(arr[i-1]>=0 && arr[i]>=0){
				int temp2 = arr[i];
				arr[i] = arr[i+1];
				arr[i+1] = temp2;
			}
		}
		
		System.out.println();
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+"   ");
		}
		System.out.println();
	}
}
