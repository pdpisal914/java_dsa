//Find the Leaders in array
//The Leader elements should be greater than its right elements

import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter The Array Length");
		int arr[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter The Array Elements");
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		int c =0;
		for(int i=0;i<arr.length;i++){
			int flag = 0;

			for(int j=i+1;j<arr.length;j++){
				if(arr[i]<arr[j]){
					flag = 1;
					break;
				}
			}

			if(flag == 0){
				c=1;
				System.out.print(arr[i]+"   ");
			}
		}
		if(c==0){
			System.out.println("Leaders Are Not Present");
		}else{
			System.out.println();
		}

	}
}
