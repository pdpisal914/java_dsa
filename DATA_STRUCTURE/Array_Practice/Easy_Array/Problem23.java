//Print The Multiplication Of Three Great/Larger Candidate

import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter The Array Length");
		int arr[] = new int[Integer.parseInt(br.readLine())];
		int Large=0,secLarge=0,thirdLarge=0;
		System.out.println("Enter The Array Elements");
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
			if(i==0){
				Large=arr[i];
			}
			if(Large<arr[i]){
				thirdLarge=secLarge;
				secLarge=Large;
				Large=arr[i];
			}else if(arr[i]<Large && arr[i]>secLarge){
				thirdLarge=secLarge;
				secLarge=arr[i];
			}else if(arr[i]<secLarge && arr[i]>thirdLarge){
				thirdLarge=arr[i];
			}
		}

		System.out.println("Large : "+Large);
		System.out.println("SecLarge : "+secLarge);
		System.out.println("ThirdLarge : "+thirdLarge);
		System.out.println("Output : "+(Large*secLarge*thirdLarge));
	}
}
