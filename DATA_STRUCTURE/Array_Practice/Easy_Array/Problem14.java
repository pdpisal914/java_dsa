//Find The Intersection Element Count Between 2 arrays

import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter The First Array Length");
		int arr1[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter The First Array Elements");
		for(int i=0;i<arr1.length;i++){
			arr1[i] = Integer.parseInt(br.readLine());
		}
		
		System.out.println("Enter The Second Array Length");
		int arr2[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter The Second Array Elements");
		for(int i=0;i<arr2.length;i++){
			arr2[i] = Integer.parseInt(br.readLine());
		}

		int cnt =0;
		int flag =0;
		int j =0;
		for(int i=0;i<arr1.length;i++){
			if(arr2[j] == arr1[i] && flag == 0){
				cnt++;
				flag =1;
			}

			if(i==arr1.length-1){
				i=-1;
				j++;
				flag = 0;
			}

			if(j==arr2.length){
				break;
			}
		}

		System.out.println("Count of Intersecting Elements : "+cnt);
		
			
	}
}
