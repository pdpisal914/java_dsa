//Remove the duplicate element from the sorted array

import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter The Array Length");
		int arr[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter The Array Elements");
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		for(int i=0;i<arr.length;i++){
			for(int j=i+1;j<arr.length;j++){

				if(arr[i]==arr[j]){
					arr[j]=-1;
				}
			}
		}
		
		System.out.println("Output : ");
		for(int i=0;i<arr.length;i++){
			if(arr[i] != -1){
				System.out.print(arr[i]+"   ");
			}
		}
		System.out.println();
	}
}
