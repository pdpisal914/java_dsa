//Product of Array Puzzle
//arr[2,3,4,5]
//o/p : itr1 = 3*4*5 = 60
//	itr2 = 2*4*5 = 40
//	itr3 = 2*3*5 = 30
//	itr4 = 2*3*4 = 24
//o/p = 60 40 30 20

import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter The Array Length");
		int arr[] = new int[Integer.parseInt(br.readLine())];
		int pro = 1;
		System.out.println("Enter The Array Elements");
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
			pro = pro*arr[i];
		}
		
		System.out.println("Output : ");
		for(int i=0;i<arr.length;i++){
			System.out.print((pro/arr[i])+ "   ");
		}
		System.out.println();


	}
}
