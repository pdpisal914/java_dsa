//Facing the Sun
//Given an array of H length represent the hights of buildings.You have to count the buildings which will be see sunrise
//(Assume:Sun is rises on the side of array starting point)
//Note:Hight of building should be strictly greater than hight of buildings in the left in order to see the sun

import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter The Array Length");
		int arr[] = new int[Integer.parseInt(br.readLine())];

		int temp=0;
		int cnt=1;
		System.out.println("Enter The Array Elements");
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
			if(i==0){
				temp=arr[i];
			}
			if(temp<arr[i]){
				cnt++;
			}
		}

		if(cnt!=0){
			System.out.println("Count Of Buildings : "+cnt);
		}else{
			System.out.println("No any building can see sunrise");
		}
	}
}
