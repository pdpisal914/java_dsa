//Key Pair
//Take the input sum from user and check thesum of 2 elements in array is equal to the given sum or not is present then print Yes and else print No

import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter The Array Length");
		int arr[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter The Array Elements");
		
		for(int i=0;i<arr.length;i++){
			arr[i]= Integer.parseInt(br.readLine());
		}

		System.out.println("Enter The Sum");
		int sum = Integer.parseInt(br.readLine());

		int flag =0;
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr.length;j++){
				if(i!=j && arr[i]+arr[j]==sum){
					flag = 1;
					break;
				}
			}
		}

		if(flag == 1){
			System.out.println("YES");
		}else{
			System.out.println("NO");
		}
	}
}
