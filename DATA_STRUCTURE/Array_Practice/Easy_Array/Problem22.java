//Left Most And Right Most Index

import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter The Array Length");
		int arr[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter The Array Elements");
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter The Element X");
		int X = Integer.parseInt(br.readLine());
		int First = -1;
		int Last = -1;
		int flag = 0;

		for(int i=0;i<arr.length;i++){
			if(arr[i]==X && flag == 0){
				First = i;
				flag = 1;
			}

			if(arr[i]==X && flag == 1){
				Last = i;
			}
		}

		if(First != -1 && Last != -1){
			System.out.println(First+" - "+Last);
		}else{
			System.out.println("Not Found");
		}
	}
}
