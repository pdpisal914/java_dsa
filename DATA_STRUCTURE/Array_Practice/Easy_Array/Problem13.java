//Find the minimum distance between two numbers in array

import java.io.*;
class ArrayDemo{
	static int minDiff(int arr[],int Num1,int Num2){
		
		int min = arr.length+1;
		for(int i=0;i<arr.length;i++){
			if(arr[i] == Num1){
				for(int j=i+1;j<arr.length;j++){
					if(Num2==arr[j] && min>j-i){
						min = j-i;
						
					}
				}
			}
		}
		return min;
	}


	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter The Array Length");
		int arr[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter The Array Elements");
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		System.out.println("Enter The Num1 ");
		int Num1 = Integer.parseInt(br.readLine());

		System.out.println("Enter The Num2");
		int Num2 = Integer.parseInt(br.readLine());

		int min = minDiff(arr,Num1,Num2);
		if(min>arr.length){
			System.out.println(" -1 ");
		}else{
			System.out.println("Min Difference : "+min);
		}
	}
}
