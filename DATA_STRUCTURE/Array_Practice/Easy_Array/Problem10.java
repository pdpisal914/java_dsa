//First Repeating Element

import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter The Array Length");
		int arr[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter The Array Elements");
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		int Ele =0,flag =0;
		for(int i=0;i<arr.length;i++){

			for(int j = i+1;j<arr.length;j++){
				if(arr[i] == arr[j]){
					Ele = arr[i];
					flag = 1;
					break;
				}
			}
			if(Ele != 0){
				break;
			}
		}

		if(flag != 0){
			System.out.println("Ele : "+Ele);
		}else{
			System.out.println("-1");
		}

	}
}
