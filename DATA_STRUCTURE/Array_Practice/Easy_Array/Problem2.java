//Sort an array of 0s ,1s,2s

import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter The Array Length");
		int arr[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter The Array Elements");
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		for(int i=0;i<=arr.length/2;i++){

			for(int j=i+1;j<arr.length;j++){

				if(arr[i]>arr[j]){

					int temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
				}
			}
		}

		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+"   ");
		}
		System.out.println("");


	}
}
