//Find Transition Point
//ex arr[0,0,0,1,1]
//O/p = Index 3 is transition point

import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter The Array Length");
		int arr[] = new int[Integer.parseInt(br.readLine())];
		int flag =0,Index=0,check=0;
		System.out.println("Enter The Array Elements 0's and 1's");
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
			if(i==0){
				check = arr[i];
			}else if(check!=arr[i] && flag == 0){
				Index = i;
				flag = 1;
			}
		}

		if(flag == 1){
			System.out.println("Transition Index : "+Index);
		}else{
			System.out.println("No Transition Occured");
		}
	}
}
