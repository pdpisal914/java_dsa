//Place D no of elements of array at the end of array

import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter The Array Length");
		int arr[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter The Array Elements");
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter The Value Of D");
		int D = Integer.parseInt(br.readLine());

		for(int i=0;i<D;i++){

			for(int j=0;j<arr.length-1;j++){
				int temp = arr[j];
				arr[j]=arr[j+1];
				arr[j+1]=temp;
			}
		}

		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+"   ");
		}
		System.out.println();

	}
}
