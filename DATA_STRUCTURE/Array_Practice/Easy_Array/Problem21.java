//non repeating element

import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter The Array Length");
		int arr[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter The Array Elements");
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		int cnt = 0;
		int j = 0;
		for(int i=0;i<arr.length;i++){
			if(i!=j && arr[j]==arr[i] ){
				cnt++;
			}
			if(i==arr.length-1){
				if(cnt == 0){
					System.out.print(arr[j]+"   ");
				}
				i=-1;
				j++;
				cnt=0;
			}

			if(j==arr.length-1){
				break;
			}
		}
	}
}
