//Find all pairs with given sum

import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter The First Array Length");
		int arr1[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter The First Array Elements");
		for(int i=0;i<arr1.length;i++){
			arr1[i] = Integer.parseInt(br.readLine());
		}
		System.out.println("Enter The Second Array Length");
		int arr2[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter The Second Array Elements");
		for(int i=0;i<arr2.length;i++){
			arr2[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter the given sum");
		int sum = Integer.parseInt(br.readLine());

		for(int i =0;i<arr1.length;i++){

			for(int j=0;j<arr2.length;j++){

				if(arr1[i]+arr2[j]==sum){
					System.out.println(arr1[i]+"   "+arr2[j]);
				}
			}
		}

	}
}
