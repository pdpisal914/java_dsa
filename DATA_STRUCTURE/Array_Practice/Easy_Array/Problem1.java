//Find Missing Element in array
import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter The Array Length N");
		int arr[] = new int[(Integer.parseInt(br.readLine()) - 1)];

		System.out.println("Enter N-1 Elements");
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		for(int i=1;i<=arr.length+1;i++){
			int flag =0;
			for(int j=0;j<arr.length;j++){

				if(i == arr[j]){
					flag =1;
					break;
				}
			}

			if(flag == 0){
				System.out.println("Missing Element : "+i);
				break;
			}
		}
	}
}
