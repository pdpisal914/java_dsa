//Union Of Two Arrays

import java.io.*;
import java.util.*;

class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		TreeSet hs = new TreeSet();
		System.out.println("Enter The First Array Length");
		int arr1[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter The Array Elements");
		for(int i=0;i<arr1.length;i++){
			arr1[i] = Integer.parseInt(br.readLine());
			hs.add(arr1[i]);
		}
	
		System.out.println("Enter The Second Array Length");
		int arr2[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter The Second Array Elements");
		for(int i=0;i<arr2.length;i++){
			arr2[i] = Integer.parseInt(br.readLine());
			hs.add(arr2[i]);
		}
		
		System.out.println();
		Iterator itr = hs.iterator();
		while(itr.hasNext()){
			System.out.print(itr.next()+"   ");
		}
		
		System.out.println();

	//	System.out.println(hs);
		


			
	}
}
