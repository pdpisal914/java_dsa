//Even Occuring Elements

import java.io.*;
import java.util.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the length of Array");
		
		int arr[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter the Elements");

		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		LinkedHashSet ss = new LinkedHashSet();
		
		for(int i=0;i<arr.length;i++){
			int count =0;
				
			for(int j=0;j<arr.length;j++){
				if(arr[i]==arr[j]){
					count++;
				}
			}
			if(count%2 == 0){
				ss.add(arr[i]);
			}
		}
		
		System.out.println("\n==================================================\n");
		if(ss.isEmpty() != true){
			
			Object Arr[] = ss.toArray();	
			for(Object i:Arr){
				System.out.print(i+"\t");
			}
			System.out.println("\n");
		}else{
			System.out.println("\t -1 \t");;
		}

	}
}



