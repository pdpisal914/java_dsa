//Minimum Product of K integers

import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the Length Of Array");
		int arr[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter The Array Elements");
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter the value of K");
		int k = Integer.parseInt(br.readLine());
		
		int Pro = arr[0];
		int PrevPro=1;
		for(int i=1;i<arr.length;i++){		
			
			Pro=Pro*arr[i];
			if(i+1==k){
				PrevPro=Pro;
			}else if(PrevPro>Pro){
				PrevPro=Pro;
			}

			if(i+1>=k){
				Pro=Pro/arr[i-k+1];
			}
		}

		System.out.println("Product :"+PrevPro);
	}
}
