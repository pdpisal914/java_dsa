//Count the elements in array between the two given numbers

import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the Length Of Array");
		int arr[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter The Array Elements");
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter The NUM1");
		int Num1 = Integer.parseInt(br.readLine());
		System.out.println("Enter The NUM2");
		int Num2 = Integer.parseInt(br.readLine());

		int LowIndex = 0;
		int HighIndex = 0;
		for(int i=1;i<=arr.length;i++){
			if(Num1 == arr[i-1]){
				LowIndex=i;
			}
			if(Num2 == arr[i-1]){
				HighIndex = i;
			}
		}

		if(LowIndex!=0 && HighIndex!=0){
			System.out.println("Count Of Elements : "+(HighIndex-LowIndex-1));
		}else{
			System.out.println("Wrong Input");
		}
	}
}

