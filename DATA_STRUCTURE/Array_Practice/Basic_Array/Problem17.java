//Product of Maximum in First Array and Minimum in Secound Array

import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the Length of First Array");
		int arr1[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter the Length of Secound Array");
		int arr2[] = new int[Integer.parseInt(br.readLine())];
		
		System.out.println("Enter the elements of First Array");
		int max =0;
		for(int i=0;i<arr1.length;i++){
			arr1[i]=Integer.parseInt(br.readLine());
			if(max<arr1[i]){
				max = arr1[i];
			}
		}

		System.out.println("Enter the elements of Second Array");
		int min =0;
		for(int i=0;i<arr2.length;i++){
			arr2[i]=Integer.parseInt(br.readLine());
			if(i==0 || min>arr2[i]){
				min = arr2[i];
			}
		}

		System.out.println("Min in 2nd: "+min);
		System.out.println("Max in 1st: "+max);
		System.out.println("Product : "+(max*min));
	}
}
