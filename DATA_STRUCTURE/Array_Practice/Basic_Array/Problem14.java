//Maximum Repeating Element

import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader( new InputStreamReader(System.in));
		System.out.println("Enter the length of array");
		int arr[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter the elements");
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		int PreCount =0;
		int MaxRepEle = 0;
		for(int i=0;i<arr.length;i++){
			int Count =0;
			for(int j=0;j<arr.length;j++){

				if(arr[i] == arr[j]){
					Count++;
				}
			}
			if(Count == PreCount && MaxRepEle > arr[i]){
				MaxRepEle = arr[i];
			}else if(Count>PreCount){
				PreCount = Count;
				MaxRepEle = arr[i];
			}
		}

		System.out.println("OutPut : "+MaxRepEle);
	}
}
				

