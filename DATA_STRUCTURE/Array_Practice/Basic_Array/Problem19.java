//Find Common Element In 3 Sorted Array

import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the 1st array length");
		int arr1[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter the 2nd array length");
		int arr2[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter the 3rd array length");
		int arr3[] = new int[Integer.parseInt(br.readLine())];
		
		System.out.println("Enter the 1st array elements");
		for(int i=0;i<arr1.length;i++){
			arr1[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter the 2nd array elements");
		for(int i=0;i<arr2.length;i++){
			arr2[i] = Integer.parseInt(br.readLine());
		}
		
		System.out.println("Enter the 3rd array elements");
		for(int i=0;i<arr3.length;i++){
			arr3[i] = Integer.parseInt(br.readLine());
		}
		int flag = 0;
		for(int i=0;i<arr1.length;i++){
		 flag = 0;
			for(int j=0;j<arr2.length;j++){
				for(int k=0;k<arr3.length;k++){
					if(arr1[i] == arr2[j] && arr1[i] == arr3[k] && arr2[j] == arr3[k]){
						flag =1;
						System.out.println("Element is: "+arr1[i]);
					}
				}
			}
		}
		if(flag == 0){
			System.out.println("No Common Element");
		}
	}
}

