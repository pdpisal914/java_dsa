

import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the Length Of 1st Array");
		int arr1[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter The 1st Array Elements");
		for(int i=0;i<arr1.length;i++){
			arr1[i] = Integer.parseInt(br.readLine());
		}
		
		System.out.println("Enter the Length Of 2nd Array");
		int arr2[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter The 2nd Array Elements");
		for(int i=0;i<arr2.length;i++){
			arr2[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter The Product For Checking");
		int Product = Integer.parseInt(br.readLine());

		int Cnt=0;
		for(int i=0;i<arr1.length;i++){

			for(int j=0;j<arr2.length;j++){

				if(arr1[i]*arr2[j] == Product){
					Cnt++;
				}
			}
		}

		System.out.println("Count : "+Cnt);

	}
}

