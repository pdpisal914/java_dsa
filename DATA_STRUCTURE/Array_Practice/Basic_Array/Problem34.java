//Arrange the sorted array elements in the Pendulum style
//arr[4,2,1,5,3];
//output=arr[5,3,1,2,4]

import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the Length Of Array");
		int arr[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter The Array Elements");
		

		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
			
		}

		int arr1[] = new int[arr.length];
		
		for(int i=0;i<arr.length;i++){
			for(int j=1;j<arr.length;j++){

				if(arr[j-1] >= arr[j]){
				       int temp = arr[j-1];
				       arr[j-1]=arr[j];
				       arr[j]=temp;
				}
			}
		}
			
		int Place = (arr.length-1)/2;
		for(int i=0;i<arr.length;i++){
			 if(i%2!=0){
				arr1[Place]=arr[i];
				Place=Place-(i+1);
				
			}else if(i%2==0){
				arr1[Place]=arr[i];
				Place = Place + (i+1);
			
			}
		
		}

		for(int i=0;i<arr.length;i++){
			System.out.print(arr1[i]+"   ");
		}
		System.out.println("");
	}

}

