//Find Product of given Array Elements

import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the number of rows");
		
		int arr[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter the Elements");

		for(int i=0;i<arr.length;i++){

			arr[i] = Integer.parseInt(br.readLine());
		
		}
		
		System.out.println("Enter the Removing Index ");
		int Remove = Integer.parseInt(br.readLine());

		if(Remove>=0 && Remove<arr.length){

			for(int i=Remove+1;i<arr.length;i++){
				arr[i-1]=arr[i];
			}
			arr[arr.length-1]=(-1);
		}

		for(int num : arr){
			if(num!= -1){
			System.out.print(num+"\t");
			}
		}
			System.out.print("\n");
		
	}
}



