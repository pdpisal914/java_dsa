//Find Peak Element
//if the element is greater than its left and right element then print that element

import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the Length Of Array");
		int arr[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter The Array Elements");
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		int flag =0;
		for(int i=1;i<arr.length-1;i++){
			if(arr[i]>arr[i-1] && arr[i]>arr[i+1]){
				if(flag ==0){
					System.out.println("OutPut : ");
					flag = 1;
				}

				System.out.print(arr[i]+"   ");
			}
		}
		
		if(flag == 0){
			System.out.println("Not Found");
		}else{
			System.out.println();
		}
	}
}

