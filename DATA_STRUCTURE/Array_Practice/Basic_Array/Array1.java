//Search the element in the array

import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the Number of Rows");

		int arr[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter the Elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		System.out.println("Enter the Element For Search In Array");
		int searchEle = Integer.parseInt(br.readLine());
		
		int flag =0;
		for(int i=0;i<arr.length;i++){
			if(arr[i] == searchEle){
				flag =1;
				System.out.println("At Index : "+i);
			}
		}

		if(flag ==0){
			System.out.println("Element Is Not Present In Given Array");
		}
	}
}
