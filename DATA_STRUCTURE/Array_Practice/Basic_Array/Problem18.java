//Find subarray with given sum
import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the length of Array");
		int arr[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter the elements");
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter The Sum");
		int SCheck = Integer.parseInt(br.readLine());
		int flag =0;
		for(int i=0;i<arr.length;i++){
			int Sum =0;
			for(int j=i;j<arr.length;j++){
				Sum = Sum + arr[j];
				if(Sum == SCheck && i==j){
					System.out.println("Sum is Present at Index "+i);
					flag =1;
				}else if(Sum==SCheck){
					flag = 1;
					System.out.println("Sum if Found between Index "+i+" To "+j);

				}
			}
		}

		if(flag == 0){
			System.out.println("SubArray is Not Found");
		}

	}
}
