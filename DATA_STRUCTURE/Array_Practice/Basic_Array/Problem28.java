//Remove repeating elements from array
import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the Array Length");
		int arr[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter The Elements");
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}


		int Count = 0;

		for(int i=0;i<arr.length;i++){

			for(int j=i;j<arr.length;j++){

				if((i != j) && arr[i] == arr[j]){
				//	System.out.println("In : "+arr[j]+" Index i :"+i+"Index j :"+j);
					Count++;
					for(int k=j+1;k<arr.length;k++){
						int temp = arr[k-1];
						arr[k-1]=arr[k];
						arr[k] = temp;
						if(k==arr.length-1){
							arr[k]=-1;
						}
					}
					
				}
			}
		}
		
		System.out.println("Count : "+Count);

		if(Count != 0){
			for(int i=0;arr[i]!= -1;i++){
				System.out.print(arr[i]+"   ");
			}

			System.out.println("");
		}else{
			System.out.println("Not Found Duplicates");
		}
	}
}

