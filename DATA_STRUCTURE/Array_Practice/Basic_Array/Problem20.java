//Check if pair with given sum is present in array or not

import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the Array  Length");
		int arr[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter the elements");
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter the Sum");
		int SCheck = Integer.parseInt(br.readLine());
		
		int flag =0;
		for(int i=0;i<arr.length;i++){
			int Sum = arr[i];
			for(int j=0;j<arr.length;j++){
				if(i!=j){
					Sum = Sum + arr[j];

					if(Sum == SCheck){
						flag =1;
						System.out.print(arr[i] + " " + arr[j]);
					}
				}
			}
		}
		if(flag ==0){
			System.out.println("Not Found");
		}else{
			System.out.println("\n Yes Found");
		}
	}
}
