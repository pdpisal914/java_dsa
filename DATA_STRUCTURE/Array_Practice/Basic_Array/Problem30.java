//Find The Closest Number
import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter The Length Of array");
		int arr[] = new int[Integer.parseInt(br.readLine())];


		System.out.println("Enter The Elements");
		int Max =0;
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
			if(i==0){
				Max=arr[i];
			}
			if(Max<arr[i]){
				Max=arr[i];
			}
		}

		System.out.println("Enter The Target");
		int Target = Integer.parseInt(br.readLine());
			
		if(Max>Target){
			int PreValue = 0;
			for(int i=0;i<arr.length;i++){
				if(arr[i]<=Target && Target-arr[i]<Target-PreValue){
					PreValue = arr[i];
				}
			}
			System.out.println("Closest Value : "+PreValue);
		}else{
			System.out.println("Closest Value : "+Max);
		}

		
	}
}
