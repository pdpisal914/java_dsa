//Maximize sum(arr[i]*i) of an Array

import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the Length Of Array");
		int arr[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter The Array Elements");
		
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		for(int i=0;i<arr.length;i++){
			for(int j=1;j<arr.length;j++){
				if(arr[j-1]>arr[j]){
					int temp = arr[j-1];
					arr[j-1]=arr[j];
					arr[j]=temp;
				}
			}

		}
		int Pro = 0;
		for(int i=0;i<arr.length;i++){
			Pro = Pro+(arr[i]*i);
		}

		
		System.out.println("Product Output:"+Pro);


	}
}

