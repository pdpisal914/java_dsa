//Find Product of given Array Elements

import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the Length of First Array");
		int arr1[] = new int[Integer.parseInt(br.readLine())];
		System.out.println("Enter the Elements");
		int max =0;
		for(int i=0;i<arr1.length;i++){
			arr1[i] = Integer.parseInt(br.readLine());
			if(i==0){
				max = arr1[i];
			}
			if(max<arr1[i]){
				max = arr1[i];
			}
		}
		
		System.out.println("Enter the Length of Second Array");
		int arr2[] = new int[Integer.parseInt(br.readLine())];
		System.out.println("Enter the Elements");
		int min =0;
		for(int i=0;i<arr2.length;i++){
			arr2[i] = Integer.parseInt(br.readLine());
			if(i==0){
				min =arr2[i];
			}
			if(min>arr2[i]){
				min=arr2[i];
			}

		}

		System.out.println("Product is : "+ (max*min));
		
	}
}



