//Elements in Range
import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter The Number of Elements");
		int arr[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter the elements");
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter The Lower Limit");
		int low = Integer.parseInt(br.readLine());
		System.out.println("Enter The Higher Limit");
		int high = Integer.parseInt(br.readLine());

		if(arr.length < (high - low +1)){
			System.out.println("NO");
		}else{
			int flag =0;
			for(int i=low ; i<= high ; i++){
				flag = 0;
				for(int j=0 ;j<arr.length;j++){
					if(arr[j] == i){
						flag = 1;
						break;
					}
				}

				if(flag == 0){
					System.out.println("OutPut : NO");
				}
			}

			if(flag == 1){
					System.out.println("OutPut : YES");
			}
		}
	}
}

