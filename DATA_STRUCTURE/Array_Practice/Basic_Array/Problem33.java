//Multiply Left and Right Array Sum

import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the Length Of Array");
		int arr[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter The Array Elements");
		int LeftSum =0,lc=0;
		int RightSum =0,rc=0;
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());

			if(i<(arr.length)/2){
				LeftSum = LeftSum+arr[i];
				lc++;
			}else{
				RightSum = RightSum+arr[i];
				rc++;
			}

		}

		System.out.println("Multiplication : "+(LeftSum*RightSum));
		System.out.println("Left : "+lc);
		System.out.println("Right : "+rc);
	}
}

