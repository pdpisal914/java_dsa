//Que : Leaders in array
//if the any element is greater than all its right elements then print that element

import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the Length Of Array");
		int arr[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter The Array Elements");
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		for(int i=0;i<arr.length;i++){
			int cnt =0;
			for(int j=i+1;j<arr.length;j++){
				if(arr[i]>arr[j]){
					cnt++;
				}
			}
	
			if(cnt==arr.length-i-1){
				System.out.print(arr[i]+"\t");
			}
		}
		System.out.println();
	}
}

