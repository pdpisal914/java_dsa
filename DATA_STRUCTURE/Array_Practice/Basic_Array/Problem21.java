//First Element to occur K times
import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the Array Length");
		int arr[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter the Array Elements");
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter The Repeating Count");
		int K = Integer.parseInt(br.readLine());

		int PreIndex = arr.length,flag =0;
		for(int i=0;i<arr.length;i++){
			int count=0,index = arr.length;
			for(int k=0;k<arr.length;k++){
				if(arr[i] == arr[k]){
					count++;
					if(count == 2){
						index = k;
					}
				}
			}
			if(count == K && PreIndex > index){
				flag = 1;
				PreIndex = index;
				}
		}
		if(flag == 1){
			System.out.println(arr[PreIndex]);
		}else{
			System.out.println("Not Found");
		}
	}
}





