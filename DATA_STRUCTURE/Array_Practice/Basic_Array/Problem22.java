//Exceptionally Odd

import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the Length Of Array");
		int arr[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter The Array Elements");
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		int PreOdd = 0;
		for(int i=0;i<arr.length;i++){
			int count =0;
			for(int j=0;j<arr.length;j++){
				if(arr[i] == arr[j]){
					count++;
				}
			}
			if(count%2!=0){
				PreOdd = arr[i];
			}
		}

		if(PreOdd == 0){
			System.out.println("Not Found");
		}else{
			System.out.println("Exceptionally Odd :"+PreOdd);
		}


	}
}

