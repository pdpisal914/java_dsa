//Find Product of given Array Elements

import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the number of rows");
		
		int arr[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter the Elements");
		int mult =1;

		for(int i=0;i<arr.length;i++){

			arr[i] = Integer.parseInt(br.readLine());
			mult = mult * arr[i];
		}
		System.out.println("Product : "+mult);
	}
}



