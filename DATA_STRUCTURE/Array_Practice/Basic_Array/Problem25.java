//Maximum Product of two numbers

import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the Length Of Array");
		int arr[] = new int[Integer.parseInt(br.readLine())];
		
		System.out.println("Enter The Array Elements");
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		int Product = arr[0];
		for(int i=1;i<arr.length;i++){
			
			for(int j=0;j<arr.length;j++){
				if(i!=j && Product<(arr[i]*arr[j])){
					Product = arr[i]*arr[j];
				}
			}
		}

		System.out.println("MAX PRODUCT :"+Product);


	}
}

