//Find Minimum and maximum Number in given Array

import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the number of rows");
		
		int arr[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter the Elements");
		int min =0;
		int max =0;

		for(int i=0;i<arr.length;i++){

			arr[i] = Integer.parseInt(br.readLine());
			if(i==0){
				min =arr[i];
				max = arr[i];
			}else{
				if(min>arr[i]){
					min = arr[i];
				}
				if(max < arr[i]){
					max = arr[i];
				}
			}

		}

		System.out.println("Minimum Number : "+min);
		System.out.println("Maximum Number : "+max);
	}
}



