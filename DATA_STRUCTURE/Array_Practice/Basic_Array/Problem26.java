//Positive and Negative Elements

import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the Length Of Array");
		int arr[] = new int[Integer.parseInt(br.readLine())];
		int PosIndex =0;
		System.out.println("Enter The Array Elements");
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
			if(arr[i] < 0){
				PosIndex++;
			}
		}

		for(int i=0;i<arr.length;i++){

			for(int j=i;j<arr.length;j++){
				
				if(arr[i] > arr[j] && i!=j){
					int temp = arr[i];
					arr[i] = arr[j];
					arr[j]=temp;
				}
			}
		}
		int NegIndex = PosIndex-1;
		int flag = 0;
		for(int i=0;i<arr.length;i++){
			if( flag ==0 && PosIndex < arr.length){
				System.out.print(arr[PosIndex]+"   ");
				flag = 1;
				PosIndex++;
				
			}
			if( flag == 1 && NegIndex>=0){
				System.out.print(arr[NegIndex]+"   ");
				flag =0;
				NegIndex--;
			}
		}
		System.out.println("");

	}
}

