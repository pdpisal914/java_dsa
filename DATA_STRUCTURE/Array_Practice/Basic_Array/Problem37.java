//Move all negative Numbers at left side and all positive numbers at right side in array

import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the Length Of Array");
		int arr[] = new int[Integer.parseInt(br.readLine())];
		int Large=0;
		System.out.println("Enter The Array Elements");
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
			if(arr[i]>0){
				Large++;
			}
		}

		for(int i=0;i<(arr.length-Large);i++){
			if(arr[i] > 0){
				for(int j=i+1;j<arr.length;j++){
					int temp = arr[j-1];
					arr[j-1]=arr[j];
					arr[j]=temp;
				}
			}
			if(arr[i]>0){
				i--;
			}
				
		}

		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+"   ");
		}
		System.out.println();

	}
}

