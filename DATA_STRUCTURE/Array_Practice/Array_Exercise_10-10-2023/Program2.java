import java.util.*;
class ArrayDemo{
	public static void main(String [] pdp){
		System.out.println("Enter The Array Length");
		Scanner sc = new Scanner(System.in);
		int arr[] = new int[sc.nextInt()];

		System.out.println("Enter The Array Elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}

		System.out.print("Enter The Element");
		int num = sc.nextInt();

		int cnt=0;
		for(int i=0;i<arr.length;i++){
			if(num == arr[i]){
				cnt++;
			}
		}
		System.out.println("Count : "+cnt);
	}
}
//TC = O(N);
//SC = O(1)
