import java.util.*;
class ArrayDemo{
	public static void main(String [] pdp){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter The Array length");
		int arr[] = new int[sc.nextInt()];

		System.out.println("Enter The Array Elements");
		int max = Integer.MIN_VALUE;
		int min = Integer.MAX_VALUE;

		for(int i=0;i<arr.length;i++){

			arr[i]=sc.nextInt();
			
			if(arr[i]<=min){
				min = arr[i];
			}else if(arr[i]>=max){
				max = arr[i];
			}
		}
		System.out.println("Sum "+(max+min));
	}
}


