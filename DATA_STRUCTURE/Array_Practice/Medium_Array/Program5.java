//kth smalles element in array

import java.io.*;
import java.util.*;
class ArrayDemo{

	static int findSmallest(int [] arr,int k){
		Arrays.sort(arr);
		return arr[k-1];
	}

	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Array Length");
		int arr[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter The Array Elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		System.out.println("Enter The Value Of K");
		int k = Integer.parseInt(br.readLine());

		System.out.println("o/p : "+findSmallest(arr,k));

	}
}
