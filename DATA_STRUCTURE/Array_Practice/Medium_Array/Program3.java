//Zero Sum SubArray

import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Array Length");
		int arr[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter The Array Elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		int cnt =0;
		for(int i=0;i<arr.length;i++){
			
			int Sum = arr[i];
			if(Sum == 0){
				cnt++;
			}
			for(int j=i+1;j<arr.length;j++){
				Sum = Sum+arr[j];
				if(Sum == 0){
					cnt++;
		//			System.out.println("Sum : "+Sum+" arr[j] : "+arr[j]);
				}
			}
		}
		if(cnt != 0){
			System.out.println("Count : "+cnt);
		}else{
			System.out.println("Not Found");
		}
			


	}
}
