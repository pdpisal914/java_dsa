

import java.io.*;
class ArrayDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Array Length");
		int arr[] = new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter The Array Elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		int Max = 0;
		for(int i=0;i<arr.length;i++){

			for(int j=i+1;j<arr.length;j++){

				if(arr[i] <= arr[j] && Max <(j-i)){
					Max = j-i;
				}
			}
		}

		System.out.println("Maximum Index : "+Max);
	}
}
