import java.util.*;

class ArrayDemo{

	static int FindIndex(int arr[],int target){
		for(int i=1;i<arr.length;i++){
			if(target>=arr[i-1] && target<=arr[i]){
				return i;
			}else if(target<arr[i-1]){
				return 0;
			}else if(target >arr[arr.length-1]){
				return arr.length;
			}

		}
		return -1;

	}
	public static void main(String [] pdp){
		System.out.println("Enter the array Size");
		Scanner sc = new Scanner(System.in);
		int arr[] = new int[sc.nextInt()];

		System.out.println("Enter The Sorted Array Elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		System.out.println("Enter The Target");
		int target = sc.nextInt();

		System.out.println(FindIndex(arr,target));
	}

}
