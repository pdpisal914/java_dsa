//LINEAR SEARCH ALGORITHAM

import java.util.*;
class LinearSearch{
	static int linearSearch(int []arr,int search){
		for(int i=0;i<arr.length;i++){
			if(arr[i]==search){
				return i;
			}
		}
		return -1;
	}

	public static void main(String [] pdp){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter The Array Length");
		int arr[]=new int[sc.nextInt()];

		System.out.println("Enter The Elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}

	
		System.out.println("Enter The Number You Want To Search");
		int search=sc.nextInt();

		int ret = linearSearch(arr,search);
		if(ret==-1){
			System.out.println("Element Not Found");
		}else{
			System.out.println("Element Found At : "+ret);
		}

	}
}

