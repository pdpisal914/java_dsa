import java.util.*;
class BinarySearch{
	static int start =0;
	static int end = 0;
	static int BS(int arr[],int search){
		if(start>end){
			return -1;
		}

		int mid = (start+end)/2;

		if(arr[mid]==search){
			return mid;
		}else if(arr[mid]<search){
			start = mid+1;
		}else{
			end = mid-1;
		}
		return BS(arr,search);
	}
	public static void main(String [] pdp){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter The Array Length");
		int arr[] = new int[sc.nextInt()];

		System.out.println("Enter The Elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		
		System.out.println("Enter The Searching Element");
		int search = sc.nextInt();
		
		end = arr.length-1;

		int ret = BS(arr,search);
		if(ret==-1){
			System.out.println("Not Found");
		}else{
			System.out.println("Found At :"+ret);
		}
	}
}


