import java.util.*;
class BinarySearch{
	static int BS(int arr[],int start,int end,int search){
		if(start>end){
			return -1;
		}

		int mid = (start+end)/2;
		int index =0;

		if(arr[mid]==search){
			return mid;
		}else if(arr[mid]<search){
			index= BS(arr,mid+1,end,search);
		}else{
			index= BS(arr,start,mid-1,search);
		}
		return index;
	}
	public static void main(String [] pdp){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter The Array Length");
		int arr[] = new int[sc.nextInt()];

		System.out.println("Enter The Elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		
		System.out.println("Enter The Searching Element");
		int search = sc.nextInt();

		int ret = BS(arr,0,arr.length-1,search);
		if(ret==-1){
			System.out.println("Not Found");
		}else{
			System.out.println("Found At :"+ret);
		}
	}
}


