//BINARY SEARCH BY USING WHILE LOOP


import java.util.*;
class BinarySearch{
	static int BS(int [] arr,int search){
		int start =0;
		int end = arr.length-1;

		while(start<= end){
			int mid = (start+end)/2;

			if(arr[mid]==search){
				return mid;
			}
			if(arr[mid]<search){
				start=mid+1;
			}
			if(arr[mid]>search){
				end = mid-1;
			}
		}
		return -1;
	}
	public static void main(String [] pdp){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter The Array LEngth");
		int arr[]=new int[sc.nextInt()];

		System.out.println("Enter The Array Elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}

		System.out.println("Enter The Searching Element");
		int search = sc.nextInt();

		int ret = BS(arr,search);
		if(ret==-1){
			System.out.println("Not Found");
		}else{
			System.out.println("Found At : "+ret);
		}
	}
}

