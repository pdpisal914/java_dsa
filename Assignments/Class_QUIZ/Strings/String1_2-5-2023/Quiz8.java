class Core2Web{
	public static void main(String [] pdp){
		String str1=new String("Java");//Heap
		String str2="Java";//SCP

		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str2));

		System.out.println(str1 == str2);//Addresses of str1 and str2 are different hence 
						 // == return the false value 
	}
}


