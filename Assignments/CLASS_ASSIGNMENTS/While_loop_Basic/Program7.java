//take number from user and if number is even then print the number in reverse order and if the number is odd number then print the number in reverse order by difference 2

import java.io.*;
class Demo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the number");
		int num=Integer.parseInt(br.readLine());

		if(num%2==0){
			while(num!=0){
				System.out.println(num--);
			}
		}else{
			while(num>=0 && num-2>=0){
				System.out.println(num=num-2);
			}
		}
	}
}

