//Count the odd digits in given number
import java.io.*;
class Demo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter number");
		int x=Integer.parseInt(br.readLine());
			int cnt=0;
		while(x!=0){
			if((x%10)%2 != 0){
				cnt++;
			}
			x=x/10;
		}
		System.out.println(cnt);
	}
}
