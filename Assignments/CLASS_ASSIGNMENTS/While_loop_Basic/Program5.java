//Print the square of even digits
import java.io.*;
class Demo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the number");
		int x=Integer.parseInt(br.readLine());

		while(x!=0){
			if((x%10)%2 == 0){
				System.out.println((x%10)*(x%10));
			}
			x=x/10;
		}
	}
}

