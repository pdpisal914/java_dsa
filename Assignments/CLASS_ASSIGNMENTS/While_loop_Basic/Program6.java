//Print sum of all even numbers and print multiplication of all odd digits between given range
import java.io.*;
class Demo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the starting number");
		int start=Integer.parseInt(br.readLine());

		System.out.println("Enter the ending number");
		int end = Integer.parseInt(br.readLine());

		int sum=0,mult=1;

		while(start!=end+1){
			if(start%2==0){
				sum=sum+start;
			}else{
				mult=mult*start;
			}
			start++;
		}
		System.out.println("Even sum ="+sum);
		System.out.println("Multiplication odd="+mult);
	}
}

