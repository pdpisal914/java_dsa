//Take the string and print the string is Alphanumeric or not
import java.io.*;
class AlphaNum{
	static boolean Alphanumeric(char [] arr){
		int fl1=0,fl2=0;
		for(char ch : arr){
			if(ch >=48 && ch<=57){
				fl1=1;
			}
			if(ch >=65 && ch <=90 || ch>=97 && ch<122){
				fl2=1;
			}
		}
		if(fl1==1 && fl2==1){
			return true;
		}else{
			return false;
		}
	}

	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the String");
		String str = br.readLine();

		if(Alphanumeric(str.toCharArray())){
			System.out.println("String is Alphanumeric");
		}else{
			System.out.println("String is Not-Alphanumeric");
		}
	}
}

