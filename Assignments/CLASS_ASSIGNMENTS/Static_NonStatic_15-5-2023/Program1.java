//Take string from user and find the count of vowels
import java.io.*;
class Vowels{

	static int VowelsDemo(char [] arr){
		int cnt=0;
		for(char ch :arr){
			if(ch=='a' ||ch == 'A'||ch=='e'||ch=='E'||ch=='i'||ch=='I'||ch=='o'||ch=='O'||ch=='u'||ch=='U'){
				cnt++;
			}
		}
		return cnt;
	}
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the String");
		String str = br.readLine();

		int ret = VowelsDemo(str.toCharArray());
		if(ret !=0){
			System.out.println("Number of vowels in the String are="+ret);
		}else{
			System.out.println("Their is no vowels");
		}
	}
}

