//Take the string from user and print the largest ASCII character from it.
import java.io.*;
class LargestAscii{
	static char LargeChar(char [] arr){
		char large=arr[0];

		for(int i=1;i<arr.length;i++){
			if(arr[i]>=large){
				large=arr[i];
			}
		}
		return large;
	}

	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the String");
		StringBuffer str = new StringBuffer(br.readLine());

		System.out.println(LargeChar(str.toString().toCharArray()));
	}
}
