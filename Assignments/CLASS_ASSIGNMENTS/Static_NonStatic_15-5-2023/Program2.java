//take the string from user and Toggle the characters
import java.io.*;
class ToggleChar{
	static String Toggeling(char [] arr){
		for(int i=0;i<arr.length;i++){
			char ch = arr[i];
			if(ch>=65 && ch<=90){
				arr[i]=(char)(arr[i]+32);
			}else{
				arr[i]=(char)(arr[i]-32);
			}
		}
		String str = new String(arr);
		return str;
	}

	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the String");
		String str = br.readLine();

		System.out.println(Toggeling(str.toCharArray()));
	}
}
