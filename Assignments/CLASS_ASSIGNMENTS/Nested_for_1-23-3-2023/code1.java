/*PRINT
c2w c2w c2w
c2w c2w c2w
c2w c2w c2w
*/

import java.util.*;
class Pattern{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int row=sc.nextInt();

		for(int i=1;i<=row*row;i++){
			System.out.print("c2w ");
			if(i%row==0){
				System.out.println();
			}
		}
	}
}
