/*
PRINT
1A 1A 1A 
1A 1A 1A
1A 1A 1A
*/

import java.util.*;
class Code{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int row=sc.nextInt();

		for(int i=1;i<=row*row;i++){
			System.out.print("1A ");
			if(i%row==0){
				System.out.println();
			}
		}
	}
}
