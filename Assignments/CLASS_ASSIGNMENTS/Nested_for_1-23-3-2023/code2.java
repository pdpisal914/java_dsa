/*PRINT
 * 4 5 6 7
 * 4 5 6 7
 * 4 5 6 7
 * 4 5 6 7
 */

import java.util.*;
class Pattern{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int row=sc.nextInt();
		int num=row;
		for(int i=1;i<=row*row;i++){
			System.out.print(num++ +" ");
			if(i%row==0){
				System.out.println();
				num=row;
			}
		}
	}
}

