/*PRINT
1 2 3 4
2 3 4 5
3 4 5 6
4 5 6 7*/

import java.util.*;
class Code{
	public static void main(String [] dpdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int row=sc.nextInt();
		int k=1;	
		for(int i=1;i<=row*row;i++){
			System.out.print(k++ + " ");
			if(i%row==0){
				System.out.println();
				k=k-(row-1);
			}
		}
	}
}

