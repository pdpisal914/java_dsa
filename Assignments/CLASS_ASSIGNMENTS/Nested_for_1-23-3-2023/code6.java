/*PRINT
9 8 7
9 8 7
9 8 7*/

import java.util.*;
class Code{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int row=sc.nextInt();
		int num=row*row;

		for(int i=1;i<=row*row;i++){
			System.out.print(num-- +" ");
			if(i%row==0){
				System.out.println();
				num=row*row;
			}
		}
	}
}
