/*PRINT
14 14 14 14
15 15 15 15
16 16 16 16
17 17 17 17
*/

import java.util.*;
class Code{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int row=sc.nextInt();

		int num=14;
		for(int i=1;i<=row*row;i++){
			System.out.print(num +" ");
			if(i%row==0){
				System.out.println();
				num++;
			}
		}
	}
}
