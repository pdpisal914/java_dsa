/*STATEMENT=WAP to print the index of Armstroong number in array*/

import java.io.*;
class Code{

	void Armstrong(int [] arr1){

		for(int i=0;i<arr1.length;i++){

			int num=arr1[i],sum=0;

			while(num!=0){

				int temp=arr1[i],rem=num%10,fact=1;;

				while(temp!=0){
					fact=fact*rem;
					temp=temp/10;
				}

				sum=sum+fact;
				num=num/10;
			}
			if(sum==arr1[i]){
				System.out.println("ARMSTRONG number "+arr1[i]+"is at Index "+i);
			}
		}
	}

	public static void main(String [] pdp)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the size of array");
		int arr[]=new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter the element");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		Code cd=new Code();
		cd.Armstrong(arr);
	}
}


