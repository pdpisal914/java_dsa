/*STATEMENT=WAP to print find the index of strong number in array*/

import java.io.*;

class Code{

	void Strong(int [] arr1){

		for(int i=0;i<arr1.length;i++){

			int num=arr1[i],sum=0;

			while(num!=0){
				int rem=num%10;
				int fact=1;

				while(rem>0){
					fact=fact*rem;
					rem--;
				}

				sum=sum+fact;
				num=num/10;
			}
			if(sum==arr1[i]){
				System.out.println("Strong Number "+arr1[i]+"is at Index "+i);
			}

		}
	}

	public static void main(String [] pdp)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the size of array");
		int arr[]=new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter the elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		Code cd=new Code();
		cd.Strong(arr);
	}
}

