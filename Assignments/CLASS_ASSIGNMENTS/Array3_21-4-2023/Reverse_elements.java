/*Ststement=Reverse each elements of array*/

import java.io.*;
class Code{
	public static void main(String [] pdp)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the array size");
		int arr[]=new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter the elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());

			int rev=0;
			while(arr[i]!=0){
				rev=rev*10+(arr[i]%10);

				arr[i]=arr[i]/10;
			}

			arr[i]=rev;

		}

		System.out.println("REVERSE ARRAY ELEMENT");
		for(int i=0;i<arr.length;i++){
			System.out.println(arr[i]);
		}


	}
}


