/*STATEMENT=WAP to print the {PERFECT numbers in array */

import java.io.*;
class Code{
	void Perfect(int per[]){
		
		for(int i=0;i<per.length;i++){
			
			int sum=0;
			for(int j=1;j<=per[i]/2;j++){
				if(per[i]%j==0){
					sum=sum+j;
				}
			}
			if(sum==per[i]){
				System.out.println("Prime number "+per[i]+"is at index "+i);
			}
		}
	}

	
	
	public static void main(String [] pdp)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the size of array");
		int arr[]=new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter the elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		Code cd=new Code();

		cd.Perfect(arr);
	}
}

