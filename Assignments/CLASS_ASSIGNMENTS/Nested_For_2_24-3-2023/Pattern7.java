/*PRINT
1 2 9
4 25 6
46 8 81*/

class Code{
	public static void main(String [] pdp){
		int row=3,col=3;

		for(int i=1;i<=row*col;i++){
			if(i%2==0){
				System.out.print(i + " ");
			}else{
				System.out.print(i*i + " ");
			}

			if(i%col==0){
				System.out.println();
			}
		}
	}
}

