/*PRINT
F 5 D 3 B 1
F 5 D 3 B 1
F 5 D 3 B 1
F 5 D 3 B 1
F 5 D 3 B 1
F 5 D 3 B 1*/

class Code{
	public static void main(String [] pdp){

		int row=6,col=6;
		char ch='F';
		int num=col;

		for(int i=1;i<=row*col;i++){
			if(i%2!=0){
				System.out.print(ch + " ");
			}else{
				System.out.print(num + " ");
			}
			ch--;
			num--;

			if(i%col==0){
				System.out.println();
				ch='F';
				num=num+col;
			}
		}
	}
}

