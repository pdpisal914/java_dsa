/*PRINT
26 Z 25 Y
24 X 23 W
22 V 21 U
20 T 19 S*/

class Code{
	public static void main(String [] pdp){
		int x=26;
		char ch='Z';

		int row=4,col=4;

		for(int i=1;i<=row;i++){
			for(int j=1;j<=col;j++){
				if(j%2==0){
					System.out.print(ch-- + " ");
				}else{
					System.out.print(x-- + " ");
				}

				if(j%col==0){
					System.out.println();
				}
			}
		}
	}
}

