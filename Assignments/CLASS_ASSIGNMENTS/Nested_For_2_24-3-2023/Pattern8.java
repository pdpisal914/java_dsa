/*PRINT
1C3 4B2 9A1
16C3 25B2 36A1
49C3 64B2 81A1*/

class Code{
	public static void main(String [] pdp){
		int row=3,col=3;
		int x=1;
		for(int i=1;i<=row;i++){
			char ch='C';
			for(int j=col;j>=1;j--){
				System.out.print(x*x);
				System.out.print(ch--);
				System.out.print( j + " ");
				x++;
			}
			System.out.println();
		}
	}
}
