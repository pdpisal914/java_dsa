/*PRINT
A b C d
E f G h
I j K l
M n O p
*/

class Code{
	public static void main(String [] pdp){
		int row=4,col=4;
		char ch1=65,ch2=97;
		for(int i=1;i<=row*col;i++){

			if(i%2!=0){
				System.out.print(ch1 + " ");
			}else{
				System.out.print(ch2 + " ");
			}
			ch1++;
			ch2++;

			if(i%col==0){
				System.out.println();
			}
		}
	}
}


