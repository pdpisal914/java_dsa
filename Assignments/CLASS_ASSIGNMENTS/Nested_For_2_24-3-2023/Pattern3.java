/*PRINT
14 15 16 17
15 16 17 18
16 17 18 19
17 18 19 20
*/

class Code{
	public static void main(String [] pdp){

		int x=14;
		int row=4,col=4;

		for(int i=1;i<=row*col;i++){
			System.out.print(x++ + " ");

			if(i%col==0){
				System.out.print("\n");
				x=x-3;
			}
		}
		}
}
