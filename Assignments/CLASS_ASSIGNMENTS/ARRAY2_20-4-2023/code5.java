/*STATEMENT=WAP to take size and elements of array from user and find the minimum element from the array*/

import java.io.*;
class Code{
	public static void main(String [] pdp)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the size of array");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter the elements");
		int min=0,flag=0;;

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
			if(flag==0){
				min=arr[i];
				flag=1;
			}
			if(min>arr[i]){
				min=arr[i];
			}
		}

		System.out.println("MINIMUM number in array="+min);
	}
}

