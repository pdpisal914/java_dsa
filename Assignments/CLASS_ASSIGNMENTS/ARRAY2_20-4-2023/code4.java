/*Ststement=WAP to take size and elements of array from user and search the specific element in array and print its INDEX*/

import java.io.*;
class Code{
	public static void main(String [] pdp)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the size of array");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter the elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		System.out.println("Enter the elements to be search in array");
		int ele=Integer.parseInt(br.readLine());
		int flag=0;

		for(int i=0;i<arr.length;i++){
			if(ele==arr[i]){
				System.out.println("Index of "+ele+"is "+i);
				flag=1;
			}
		}

		if(flag==0){
			System.out.println("Elements is Not Present in array");
		}
	}
}

