/*Statement=WAP to take size and elements of array from user and Merge the 2 array"*/

import java.io.*;
class Code{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the size of array One");
		int arr1[]=new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter the size of array Second");
		int arr2[]=new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter the first array element");
		for(int i=0;i<arr1.length;i++){
			arr1[i]=Integer.parseInt(br.readLine());
		}

		System.out.println("Enter the second array elements");
		for(int i=0;i<arr2.length;i++){
			arr2[i]=Integer.parseInt(br.readLine());
		}

		int arr3[]=new int[(arr1.length+arr2.length)];

		for(int i=0;i<arr1.length;i++){
			arr3[i]=arr1[i];
		}

		for(int i=0;i<arr2.length;i++){
			arr3[arr1.length+i]=arr2[i];
		}



		System.out.println("MERGED ARRAY");
		for(int i=0;i<arr3.length;i++){
			System.out.print(arr3[i]+"   ");
		}
	}
}
