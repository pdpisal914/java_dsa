/*STATEMENT=WAP to take size and elements of array from user and Print the common elements in the array*/

import java.io.*;
class Code{
	public static void main(String [] pdp)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the size of array 1 ");
		int size1=Integer.parseInt(br.readLine());
		System.out.println("Enter the size of array 2 ");
		int size2=Integer.parseInt(br.readLine());

		int arr1[]=new int[size1];
		int arr2[]=new int[size2];

		System.out.println("Enter the elements of array 1");
		for(int i=0;i<arr1.length;i++){
			arr1[i]=Integer.parseInt(br.readLine());
		}

		System.out.println("Enter the elements of array 2");
		for(int i=0;i<arr2.length;i++){
			arr2[i]=Integer.parseInt(br.readLine());
		}

		System.out.println("COMMONN Elements are:");
		for(int i=0;i<arr1.length;i++){
			for(int j=0;j<arr2.length;j++){
				if(arr1[i]==arr2[j]){
					System.out.println(arr1[i]);
				}
			}
		}
	}
}

