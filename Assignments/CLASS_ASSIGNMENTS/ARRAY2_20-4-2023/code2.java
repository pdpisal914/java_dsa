/*STATEMENT=WAP to take array from user and print the sum of odd numbers and sum of even numbers*/

import java.io.*;
class Code{
	public static void main(String [] pdp)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the size of array");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter the elements");
		int Esum=0,Osum=0;
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
			if(arr[i]%2==0){
				Esum=Esum+arr[i];
			}else{
				Osum=Osum+arr[i];
			}
		}
		System.out.println("Sum of even numbers="+Esum);
		System.out.println("Sum of odd numbers="+Osum);
	}
}


