/*STATEMENT=WAP to take size and elements of array from user and Print the elements whose addition of digits is even*/

import java.io.*;
class Code{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the size of array");
		int arr[]=new int[Integer.parseInt(br.readLine())];

		System.out.println("Enter the elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		System.out.println("Numbers whose digit sum is even");
		for(int i=0;i<arr.length;i++){
			int num=arr[i];
			int sum=0;

			while(num!=0){
				sum=sum+(num%10);
				num=num/10;
			}
			if(sum%2==0){
				System.out.println(arr[i]+"   ");
			}
		}
	}
}

