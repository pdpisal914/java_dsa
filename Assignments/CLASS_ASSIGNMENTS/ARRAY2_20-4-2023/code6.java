/*STATEMENT=WAP to take size and elements of array from user and Print the MAXIMUM elements from the array*/

import java.io.*;
class Code{
	public static void main(String [] pdp)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the size of array");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter the elements");
		int max=0,flag=0;

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());

			if(flag==0){
				max=arr[i];
				flag=1;
			}
			if(max<arr[i]){
				max=arr[i];
			}
		}

		System.out.println("MAXIMUM number in array="+max);
	}
}


