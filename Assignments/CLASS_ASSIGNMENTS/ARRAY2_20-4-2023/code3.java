/*STATEMENT=AP to take size and elements of array from user and print the total even and total odd numbers count*/

import java.io.*;
class Code{
	public static void main(String [] pdp)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the size of array");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter the elements of array");
		
		int Ocnt=0,Ecnt=0;

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());

			if(arr[i]%2==0){
				Ecnt++;
			}else{
				Ocnt++;
			}
		}
		System.out.println("Total even numbers="+Ecnt);
		System.out.println("Totral odd numbers="+Ocnt);
	}
}
