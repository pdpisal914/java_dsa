//Statement=Take size and elements of character array from user and print the vowels

import java.util.*;
class Code{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the size of array");
		int size=sc.nextInt();

		char arr[]=new char[size];

		System.out.println("Enter the elements");
		for(int i=0;i<arr.length;i++){
		
			arr[i]=sc.next().charAt(0);
		}

		System.out.println("Vowels are=");
		for(int i=0;i<arr.length;i++){
			if(arr[i]=='a' || arr[i]=='A'){
				System.out.println(arr[i]);
			}
			if(arr[i]=='e' || arr[i]=='E'){
				System.out.println(arr[i]);
			}
			if(arr[i]=='i' || arr[i]=='I'){
				System.out.println(arr[i]);
			}
			if(arr[i]=='o' || arr[i]=='O'){
				System.out.println(arr[i]);
			}
			if(arr[i]=='u' || arr[i]=='U'){
				System.out.println(arr[i]);
			}
		}
	}
}

