/*Ststement=Take the size and elements of array from the user and print the product of even elements from the array*/
import java.io.*;
class Code{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the size of array");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter the elements");
		
		int mult=1;
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
			if(arr[i]%2==0){
				mult=mult*arr[i];
			}
		}
		System.out.println("Product of even numbers="+mult);
	}
}

