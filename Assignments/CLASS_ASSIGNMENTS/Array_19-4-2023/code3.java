/*Statement=WAP to take size and elements from user for array print the odd index only*/
import java.io.*;
class OddIndex{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the size of array");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter the array elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());

		}
		System.out.println("Elements of odd index");

		for(int i=1;i<arr.length;i=i+2){
			System.out.print(arr[i] + "\t");
		}
	}
}


