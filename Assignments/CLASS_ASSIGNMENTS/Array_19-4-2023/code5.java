/*Statement=Take size and elements of array from user and print the elements which are divisible by 5*/

import java.io.*;
class Code{
	public static void main(String [] pdp)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the size of array");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter the elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		System.out.println("Elements Divisible by 5 are=");
		for(int i=0;i<arr.length;i++){
			if(arr[i]%5==0){
				System.out.print(arr[i]+"\t");
			}
		}
	}
}


