//Statement=Find the GCD between two numbers

import java.util.*;
class Gcd{
	public static void main(String [] pdp){
		int num1,num2;
		Scanner sc=new Scanner(System.in);
		
		System.out.println("Enter the num1");
		num1=sc.nextInt();
		System.out.println("Enter the num2");
		num2=sc.nextInt();
		
		int i;
		if(num1<num2){
			i=num1;
		}else{
			i=num2;
		}

		for(;i>=1;i--){

			if(num1%i==0 && num2%i==0){
				System.out.println("GREATEST COMMON DIVISOR IS="+i);
				i=1;
			}
		}
	}
}

