//Statement=WAP to print the given number is automorphic number or not an automorphic number

import java.util.*;
class Automorphic{
	public static void main(String [] pdp){
		int num;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number");
		num=sc.nextInt();
		int sq=num*num;
		int flag=0;

		for(int i=num;i!=0;i=i/10){

			if((i%10) == (sq%10)){
				flag=1;
				sq=sq/10;
			}else{
				flag=0;
				break;
			}
		}

		if(flag==1){
			System.out.println(num+"is automorphic number");
		}else{
			System.out.println(num+"is not an automorphic number");
		}
	}
}

