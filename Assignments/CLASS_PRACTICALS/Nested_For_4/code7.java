/*PRINT
F
E 1
D 2 E
C 3 D 4
B 5 C 6 D
A 7 B 8 C 9*/

import java.util.*;
class Code{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int n=sc.nextInt();
		int x=1;

		for(int i=1;i<=n;i++){
			char ch=(char)(65+n-i);
			for(int j=1;j<=i;j++){
				if(j%2!=0){
					System.out.print(ch++ + " ");
				}else{
					System.out.print(x++ + " ");
				}
			}
			System.out.println();
		}
	}
}



