/*PRINT
1 2 3 4
2 3 4
3 4
4*/

import java.util.*;
class Code{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int n=sc.nextInt();


		for(int i=1;i<=n;i++){
			int x=i;
			for(int j=1;j<=n-i+1;j++){
				System.out.print(x++ + " ");
			}
			System.out.println();
		}
	}
}

