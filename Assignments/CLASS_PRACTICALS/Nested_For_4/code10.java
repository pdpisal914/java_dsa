/*PRINT
1
8 9
9 64 25
64 25 216 49*/

import java.util.*;
class Code{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int n=sc.nextInt();
		

		for(int i=1;i<=n;i++){
			int x=i;
			for(int j=1;j<=i;j++){

				if(x%2!=0){
					System.out.print(x*x + " ");
				}else{
					System.out.print(x*x*x + " ");
				}
				x++;
			}
			System.out.println();
		}
	}
}


