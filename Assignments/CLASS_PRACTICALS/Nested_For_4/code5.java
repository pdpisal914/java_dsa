/*PRINT
A B C D
B C D
C D
D*/

import java.util.*;
class Code{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int n=sc.nextInt();

		for(int i=1;i<=n;i++){
			char ch=(char)(64+i);
			for(int j=1;j<=n-i+1;j++){
				System.out.print(ch++ + " ");
			}
			System.out.println();
		}
	}
}

