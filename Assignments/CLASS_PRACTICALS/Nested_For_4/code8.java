/*PRINT
10
I H
7 6 5
D C B A*/

import java.util.*;
class Code{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int n=sc.nextInt();
		int x=(n*(n+1))/2;
		char ch=(char)(64+x);

		for(int i=1;i<=n;i++){

			for(int j=1;j<=i;j++){

				if(i%2!=0){
					System.out.print(x + " ");
				}else{
					System.out.print(ch + " ");
				}
				x--;
				ch--;
			}
			System.out.println();
		}
	}
}



