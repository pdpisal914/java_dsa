//Statement=WAP to check the day number is between 1 to 7 and print the corresponding day of week


class Week{
	public static void main(String [] pdp){

		int day=5;

		if(day<=0 || day>7){
			System.out.println("Invalid day number");
		}else{
			if(day==1){
				System.out.println("Monday");
			}else if(day==2){
				System.out.println("Tuesday");
			}else if(day==3){
				System.out.println("Wednesday");
			}else if(day==4){
				System.out.println("Thursday");
			}else if(day==5){
				System.out.println("Friday");
			}else if(day==6){
				System.out.println("Saturday");
			}else{
				System.out.println("Sunday");
			}
		}
	}
}

