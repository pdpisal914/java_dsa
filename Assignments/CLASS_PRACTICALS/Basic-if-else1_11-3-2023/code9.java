//Statement=WAP to accept the three numbers & check where they are pythagorian triplets or not

class Pythagoras{
	public static void main(String [] pdp){

		float a=3f;
		float b=5f;
		float c=54f;

		if(a<0 || b<0 || c<0){
			System.out.println("Invalid triplet values");
		}else if(a*a + b*b==c || a*a+c*c==b*b || b*b+c*c==a*a){
			System.out.println("Its a Pythagorian triplets");
		}else{
			System.out.println("Its not a Pythagorian triplets");
		}
	}
}

