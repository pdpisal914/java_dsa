/*STATEMENT=WAP
# = = = =
= # = = =
= = # = =
= = = # =
= = = = #
*/

import java.io.*;
class Code{
	public static void main(String [] pdp)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the number of rows");
		int row=Integer.parseInt(br.readLine());

		for(int i=1;i<=row;i++){
			for(int j=1;j<=row;j++){
				if(i==j){
					System.out.print("#\t");
				}else{
					System.out.print("=\t");
				}
			}
		System.out.println();
		}
	}
}

