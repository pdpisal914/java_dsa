/*Statement=WAP to print the odd numbers in reverse order and even numbers normally between the range*/

import java.io.*;
class Code{
	public static void main(String [] pdp)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the starting point");
		int start=Integer.parseInt(br.readLine());

		System.out.println("Enter the ending point");
		int end=Integer.parseInt(br.readLine());

		System.out.println("Even num=");
		for(int i=start;i<=end;i++){

			if(i%2==0){
				System.out.print(i+"\t");
			}
		}
		System.out.println("\nOdd num=");
		for(int i=end;i>=start;i--){
			if(i%2!=0){
				System.out.print(i+"\t");
			}
		}
	}
}


