import java.io.*;
class Code{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the number of rows");
		int row=Integer.parseInt(br.readLine());
		int x=0;

		for(int i=1;i<=row;i++){
			x++;
			for(int j=1;j<=i;j++){
				if(x==1){
					System.out.print("$\t");
				}else if(x==2){
					System.out.print("@\t");
				}else if(x==3){
					System.out.print("&\t");
				}else if(x==4){
					System.out.print("#\t");
				}
			}
			System.out.println();
			if(x==4){
				x=0;
			}
		}
	}
}
