/*STATEMENT=WAP to take two characters,if these characters are equal then print them as it is but if they are unequal then print their difference*/
import java.io.*;
class Code{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the first character");
		char ch1=(char)br.read();
		br.skip(1);

		System.out.println("Enter the second character");
		char ch2=(char)br.read();

		if(ch1==ch2){
			System.out.println(ch1);
		}else{
			int num=0;
			if(ch2>ch1){
				num=(int)ch2-ch1;
			System.out.println("Difference="+num);
			}else{
				num=(int)ch1-ch2;
				System.out.println("Difference="+num);
			}
		}
	}
}
