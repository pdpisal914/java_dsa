/*Statement=WAP
5 4 3 2 1
8 6 4 2
9 6 3
8 4
5*/

import java.io.*;
class Code{
	public static void main(String [] pdp)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the number of rows");
		int row=Integer.parseInt(br.readLine());

		for(int i=1,x=0;i<=row;i++,x++){
			int num=i*(row-x);
			for(int j=i;j<=row;j++){
				System.out.print(num+"\t");
				num=num-i;
			}
			System.out.println();
		}
	}
}


