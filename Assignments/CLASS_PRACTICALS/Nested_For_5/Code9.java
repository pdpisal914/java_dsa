/*STATEMENT=WAP to print the sum of factorials of every number in the given range*/
import java.io.*;
class Code{
	public static void main(String [] pdp)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the number");
		int num=Integer.parseInt(br.readLine());
		int sum=0;

		while(num!=0){
			int rem=num%10;
			int fact=1;

			while(rem>0){
				fact=fact*rem;
				rem--;
			}
			sum=sum+fact;
			num=num/10;
		}
		System.out.println("Sum of digits are="+sum);
	}
}

	


