/*STATEMENT=WAP to print the series of prime number between the given range*/

import java.io.*;
class Code{
	public static void main(String [] pdp)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Starting value of range");
		int start=Integer.parseInt(br.readLine());

		System.out.println("Enter the Ending value of range");
		int end=Integer.parseInt(br.readLine());

		for(int i=start;i<=end;i++){
			int cnt=0;

			for(int j=2;j<=(i/2);j++){
				if(i%j==0){
					cnt++;
					break;
				}
			}
			if(cnt==0){
				System.out.print(i+"\t");
			}
		}
		System.out.println();
	}
}

