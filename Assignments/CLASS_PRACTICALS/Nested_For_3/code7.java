/*PRINT
F
E F
D E F
C D E F
B C D E F
A B C D E F
*/
import java.util.*;
class Code{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int n=sc.nextInt();

		for(int i=1;i<=n;i++){
			char ch=(char)(65+n-i);
			for(int j=1;j<=i;j++){
				System.out.print(ch++ + " ");
			}
			System.out.println();
		}
	}
}


