/*PRINT
J
I H
G F E
D C B A*/

import java.util.*;
class Code{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int n=sc.nextInt();
					
		char ch=(char)(64+(n*(n+1))/2);

		for(int i=1;i<=n;i++){
			for(int j=1;j<=i;j++){
				System.out.print(ch-- + " ");
			}
			System.out.println();
		}
	}
}


