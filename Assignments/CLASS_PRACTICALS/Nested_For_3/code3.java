/*PRINT
10
9 8
7 6 5
4 3 2 1*/
import java.util.*;
class Code{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int n=sc.nextInt();
		int num=(n*(n+1))/2;

		for(int i=1,x=1;i<=x;i++){
			System.out.print(num-- + " ");
			if(i==x){
				System.out.println();
				i=0;
				x++;
			}
			if(x==n+1){
				break;
			}
		}
	}
}


