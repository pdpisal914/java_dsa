/*PRINT
10 10 10 10
11 11 11
12 12
13*/

import java.util.*;

class Code{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int row=sc.nextInt();
		int x=(row*(row+1))/2;

		for(int i=1;i<=row;i++){
			System.out.print(x + " ");
			if(i==row){
				System.out.println();
				i=0;
				row--;
				x++;
			}
		}
	}
}


