/*PRINT
1
8 27
64 125 216
*/

import java.util.*;
class Code{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int n=sc.nextInt();
		int x=1;

		for(int i=1;i<=n;i++){
			for(int j=1;j<=i;j++){
				System.out.print(x*x*x + " ");
				x++;
			}
			System.out.println();
		}
	}
}

