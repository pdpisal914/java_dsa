/*PRINT
9
9 8
9 8 7
9 8 7 6*/

import java.util.*;
class Code{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int N=sc.nextInt();

		for(int i=1;i<=N;i++){
			int x=(N*(N+1))/2;
			for(int j=1;j<=i;j++){
				System.out.print(--x + " ");
			}
			System.out.println();
		}
	}
}

