class Demo{
	String str;
	Demo(String str){
		this.str=str;
	}

	public String toString(){
		return str;
	}
	public void finalize(){
		System.out.println("Object Deleted");
	}
}
class GCDemo{
	public static void main(String [] pdp){

		Demo obj1 = new Demo("Core2Web");
		Demo obj2 = new Demo("Binecaps");
		Demo obj3 = new Demo("Incubator");
		Demo obj4 = new Demo("C2W");

		System.out.println(obj1);
		System.out.println(obj2);
		System.out.println(obj3);
		System.out.println(obj4);

		obj1=null;
		obj2=null;

		System.gc();

	}
}
