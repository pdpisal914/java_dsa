import java.util.*;
class HashMapDemo{
	public static void main(String [] pdp){
		HashSet hs = new HashSet();
		hs.add("Pranav");
		hs.add("Sagar");
		hs.add("Vaibhav");
		hs.add("Pranav");

		System.out.println(hs);

		HashMap hm = new HashMap(100);
		System.out.println(hm.size());//3
		//System.out.println(hm.capacity());
		
		hm.put("Pranav","Sangamner");
		hm.put("Vaibhav","ShriGonda");
		hm.put("Sagar","Bid");
		hm.put("Pranav","Ghargaon");

		System.out.println(hm);
		System.out.println(hm.size());//3
		//System.out.println(hm.capacity());

	}
}
