import java.util.*;
class LinkedHashmap{
	public static void main(String [] pdp){
		LinkedHashMap hm = new LinkedHashMap();
		hm.put("Java",".java");
		hm.put("Python",".py");
		hm.put("Dart",".dart");
		

		System.out.println(hm);
		//get();
		System.out.println(hm.get("Python"));

		//keySet
		System.out.println(hm.keySet());

		//Values
		System.out.println(hm.values());

		//EntrySet
		System.out.println(hm.entrySet());
	}
}
