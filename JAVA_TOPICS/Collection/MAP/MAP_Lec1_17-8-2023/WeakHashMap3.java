import java.util.*;
class Demo{
	String str;

	Demo(String str){
		this.str = str;
	}

	public String toString(){
		return str;
	}

	public void finalize(){
		System.out.println("Object Deleted");
	}
}
class GCDemo{
	public static void main(String [] pdp){
		Demo obj1 = new Demo("Core2Web");
		Demo obj2 = new Demo("Binecaps");
		Demo obj3 = new Demo("Incubator");
		Demo obj4 = new Demo("C2W");

		WeakHashMap hm = new WeakHashMap();
		hm.put(obj1,2016);
		hm.put(obj2,2019);
		hm.put(obj3,2023);
		hm.put(obj4,2025);

		obj1 = null;
		System.gc();

		System.out.println(hm);
	}
}
