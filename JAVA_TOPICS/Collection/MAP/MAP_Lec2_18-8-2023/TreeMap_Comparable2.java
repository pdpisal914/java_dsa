import java.util.*;
class Demo implements Comparable {
	String str=null;
	int Year=0;
	Demo(String str,int Year){
		this.str=str;
		this.Year=Year;
	}
	public int compareTo(Object obj){
		return (this.str).compareTo(((Demo)obj).str);
	}

	public String toString(){
		return "{"+str+":"+Year+"}";
	}
}
class TreeMapDemo{

	public static void main(String [] pdp){
		TreeMap ts = new TreeMap();

		ts.put(new Demo("Kanha",2005),"Infosys");
		ts.put(new Demo("Ashish",2008),"BarClays");
		ts.put(new Demo("Badhe",2010),"CarPro");
		ts.put(new Demo("Rahul",2022),"BMC");

		System.out.println(ts);
	}
}
