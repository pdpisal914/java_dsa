import java.util.*;
import java.io.*;
class PropertiesDemo{
	public static void main(String [] pdp) throws IOException{

		Properties obj = new Properties();
		FileInputStream fobj =new FileInputStream("abc.properties");
		obj.load(fobj);

		String name = obj.getProperty("Ashish");
		System.out.println(name);

		obj.setProperty("Shashi","Biencaps");

		FileOutputStream outObj=new FileOutputStream("abc.properties");
		obj.store(outObj,"Updated By Shashi");
	}
}


