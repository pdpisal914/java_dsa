import java.util.*;
class Demo{
	String str=null;
	int Year=0;
	Demo(String str,int Year){
		this.str=str;
		this.Year=Year;
	}
/*	public int compareTo(Object obj){
		return (this.str).compareTo(((Demo)obj).str);
	}

*/	public String toString(){
		return "{"+str+":"+Year+"}";
	}
}
class SortByName implements Comparator{
	public int compare(Object obj1,Object obj2){
		return ((Demo)obj1).str.compareTo(((Demo)obj2).str);
	}
}

class SortByYear implements Comparator{
	public int compare(Object obj1,Object obj2){
		return ((Demo)obj1).Year - (((Demo)obj2).Year);
	}
}

class TreeMapDemo{

	public static void main(String [] pdp){
		TreeMap ts = new TreeMap(new SortByName());
		ts.put(new Demo("Kanha",2005),"Infosys");
		ts.put(new Demo("Ashish",2008),"BarClays");
		ts.put(new Demo("Badhe",2010),"CarPro");
		ts.put(new Demo("Rahul",2022),"BMC");

		TreeMap ts1 = new TreeMap(new SortByYear());
		ts1.put(new Demo("Kanha",2005),"Infosys");
		ts1.put(new Demo("Ashish",2008),"BarClays");
		ts1.put(new Demo("Badhe",2010),"CarPro");
		ts1.put(new Demo("Rahul",2022),"BMC");
		
		System.out.println(ts);
		System.out.println(ts1);
	}
}
