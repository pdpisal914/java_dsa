import java.util.*;
class SortedmapDemo{
	public static void main(String [] pdp){
		SortedMap st = new TreeMap();
		st.put("Ind","India");
		st.put("Pak","Pakistan");
		st.put("Aus","Australia");
		st.put("SL","SriLanka");
		st.put("Ban","Bangladesh");

		System.out.println(st);

		//SortedMap submap(k,k);
		System.out.println(st.subMap("Aus","Pak"));

		//SortedMap headMap(k);
		System.out.println(st.headMap("Pak"));

		//SortedMap tailMap(k);
		System.out.println(st.tailMap("Pak"));

		//k firstKey();
		System.out.println(st.firstKey());

		//k lastKey();
		System.out.println(st.lastKey());

		//Set keySet();
		System.out.println(st.keySet());

		//Collection values();
		System.out.println(st.values());

		//Set entrySet();
		System.out.println(st.entrySet());
	}
}
