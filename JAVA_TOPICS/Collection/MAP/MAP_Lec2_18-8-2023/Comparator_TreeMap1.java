import java.util.*;
class PlatForm{
	String str=null;
	int Year=0;
	PlatForm(String str,int Year){
		this.str=str;
		this.Year=Year;
	}
	public String toString(){
		return "{"+str+":"+Year+"}";
	}
}
class SortByName implements Comparator{
	public int compare(Object obj1,Object obj2){
		return (((PlatForm)obj1).str).compareTo(((PlatForm)obj2).str);
	}
}
class SortByYear implements Comparator{
	public int compare(Object obj1,Object obj2){
		return (((PlatForm)obj1).Year)-(((PlatForm)obj2).Year);

	}
}

class TreeMapDemo{
	public static void main(String [] pdp){
		TreeMap tm = new TreeMap(new SortByName());
		tm.put(new PlatForm("YouTube",2005),"Google");
		tm.put(new PlatForm("Instagram",2013),"Meta");
		tm.put(new PlatForm("FaceBook",2004),"Meta");
		tm.put(new PlatForm("ChatGpt",2022),"OpenAI");

		System.out.println(tm);
		
		TreeMap tm1 = new TreeMap(new SortByYear());
		tm1.put(new PlatForm("YouTube",2005),"Google");
		tm1.put(new PlatForm("Instagram",2013),"Meta");
		tm1.put(new PlatForm("FaceBook",2004),"Meta");
		tm1.put(new PlatForm("ChatGpt",2022),"OpenAI");

		System.out.println(tm1);
	}
}
		
