import java.util.*;
class PlatForm implements Comparable{
	String str=null;
	int Year=0;
	PlatForm(String str,int Year){
		this.str=str;
		this.Year=Year;
	}

	public String toString(){
		return "{"+str+":"+Year+"}";
	}

	public int compareTo(Object obj){
		return (this.str).compareTo(((PlatForm)obj).str);
	}
}

class TreeMapDemo{
	public static void main(String [] pdp){
		TreeMap tm = new TreeMap();

		tm.put(new PlatForm("YouTube",2005),"Google");
		tm.put(new PlatForm("Instagram",2013),"Meta");
		tm.put(new PlatForm("FaceBook",2004),"Meta");
		tm.put(new PlatForm("ChatGpt",2022),"OpenAI");
		
		System.out.println(tm);
	}
}
