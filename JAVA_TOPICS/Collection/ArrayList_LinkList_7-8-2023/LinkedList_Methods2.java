import java.util.*;
 
class LinkedListDemo{
	public static void main(String [] pdp){

		LinkedList ll = new LinkedList();
		ll.add(20);
		ll.add(30);
		ll.add(40);
		ll.add(50);

		System.out.println(ll);

		ll.addFirst(10);
		ll.addLast(60);

		System.out.println(ll);

		System.out.println(ll.getFirst());
		System.out.println(ll.getLast());

		ll.removeFirst();
		ll.removeLast();

		System.out.println(ll);
	}
}
