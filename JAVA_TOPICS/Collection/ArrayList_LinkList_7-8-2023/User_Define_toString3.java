import java.util.*;
class College{
	String clgnm=null;
	int clgId=0;
	float area=0f;
	 College(String str,int id,float x){
		 clgnm=str;
		 clgId=id;
		 area=x;
	 }
	 public String toString(){
		 return String.valueOf(area);
	 }
}
class ArrayDemo{
	public static void main(String [] pdp){
		ArrayList al = new ArrayList();
		al.add(new College("xyz",16,87.6f));
		al.add(new College("pqr",12,65.6f));
		al.add(new College("abc",20,90.6f));

		System.out.println("toString returns float in the form of string=AREA :");
		System.out.println(al);
	}
}
