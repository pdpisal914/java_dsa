import java.util.*;
class ArrayListDemo{
	public static void main(String [] pdp){
		ArrayList al = new ArrayList();
		//Every e;lement in the ArrayList is Object of class
		//it will internally goes an opbject
		//Wrapper classes
		al.add(new Integer(10));
		al.add(new Float(20.76f));
		al.add(new Double(675.65));
		al.add(new String("Pranav"));

		System.out.println(al);//=> al.toString();
				       //every wrapper class overloads the toString() method of object class hence 
				       //thier out put will pe present in the proper format
	}
}

