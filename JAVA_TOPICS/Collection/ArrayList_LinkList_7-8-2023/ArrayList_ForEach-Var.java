import java.util.*;
import java.io.*;

class ArrayListDemo{
	public static void main(String [] pdp){
		ArrayList al = new ArrayList();
		al.add(10);
		al.add(20);
		al.add(30);
		al.add(40);

		/*for(Integer x:al){//ERROR=Incompatible types Integerclass Cannot be converted into int
			System.out.println(x);
		}*/
		System.out.println(al);
		System.out.println("BY Using For Each :");
		/*for(Object ob:al){//No error - its working
			System.out.println(ob);
		}*/

		for(var x:al){
			System.out.println(x);
		}

	}
}

