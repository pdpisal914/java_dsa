import java.util.*;
import java.util.concurrent.*;
class Producer implements Runnable{
	BlockingQueue bq=null;

	Producer(BlockingQueue bq){
		this.bq=bq;
	}

	public void run(){

		for(int i=1;i<=10;i++){
			try{
				bq.put(10);
				System.out.println("Producer = "+i);
			}catch(InterruptedException ie){
			}

			try{
				Thread.sleep(10);
			}catch(InterruptedException ie){
			}
		}
	}
}
class Customer implements Runnable{
	BlockingQueue bq=null;
	Customer(BlockingQueue bq ){
		this.bq=bq;
	}

	public void run(){

		for(int i=1;i<=10;i++){
			try{
				bq.take();
				System.out.println("Consume = "+i);
			}catch(InterruptedException ie){
			}

			try{
				Thread.sleep(5000);
			}catch(InterruptedException ie){
			}
		}
	}
}

class ProducerCustomer{
	public static void main(String [] pdp) throws InterruptedException{

		BlockingQueue bq = new ArrayBlockingQueue(10);

		Producer pd = new Producer(bq);
		Customer cs = new Customer(bq);

		Thread  pThread = new Thread(pd);
		Thread cThread = new Thread(cs);

		pThread.start();
		cThread.start();
	}
}
