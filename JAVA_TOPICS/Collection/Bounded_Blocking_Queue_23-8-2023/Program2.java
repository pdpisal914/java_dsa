import java.util.*;
import java.util.concurrent.*;

class PriorityBlockingQueueDemo{
	public static void main(String [] pdp) throws InterruptedException{

		BlockingQueue bq = new PriorityBlockingQueue(3);

		for(int i=0;i<=2147483000;i++){
		bq.put(10);
		}

		System.out.println(bq);

		bq.offer(40,5,TimeUnit.SECONDS);

		System.out.println(bq);

		bq.take();

		System.out.println(bq);

		ArrayList al = new ArrayList();
		System.out.println("ArrayList = "+al);

		bq.drainTo(al);

		System.out.println("ArrayList ="+al);
		System.out.println(bq);
	}
}



