import java.util.concurrent.*;
class ArrayBlockingDemo{
	public static void main(String [] pdp) throws InterruptedException{

		BlockingQueue bq = new ArrayBlockingQueue(3);

		bq.offer(10);
		bq.offer(20);
		bq.offer(30);
		System.out.println(bq);
		bq.put(40);
		System.out.println(bq);
	}
}

