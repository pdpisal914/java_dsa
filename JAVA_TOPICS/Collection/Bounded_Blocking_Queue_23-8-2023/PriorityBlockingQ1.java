import java.util.*;
import java.util.concurrent.*;

class PriorityBlockingQueueDemo{
	public static void main(String [] pdp) throws InterruptedException{

		BlockingQueue bq = new PriorityBlockingQueue(3);
		bq.put(10);
		bq.put(20);
		bq.put(30);

		System.out.println(bq);

		bq.offer(40,5,TimeUnit.SECONDS);

		System.out.println(bq);

		bq.take();

		System.out.println(bq);

		ArrayList al = new ArrayList();
		System.out.println("ArrayList = "+al);

		bq.drainTo(al);

		System.out.println("ArrayList ="+al);
		System.out.println(bq);
	}
}



