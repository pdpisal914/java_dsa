import java.util.*;

class ArrayListDemo{
	public static void main(String [] pdp){
		ArrayList al = new ArrayList();
		al.add(10);
		al.add(20.5f);
		al.add("Pranav");
		al.add(10);
		al.add(20.5f);

		System.out.println(al);
		System.out.println("int size()="+al.size());

		System.out.println("Boolean contains(Object) = " + al.contains("Pranav"));
		System.out.println("Boolean contains(Object) = " + al.contains(15));
		
		System.out.println("int indexOf(Object)="+al.indexOf(10));
		System.out.println("int indexOf(Object)="+al.indexOf("Pranav"));
		
		System.out.println("int lastIndexOf(Object)="+al.lastIndexOf(10));
		System.out.println("int lastIndexOf(Object)="+al.lastIndexOf(20.5f));
		
		System.out.println("E get(Object)="+ al.get(3));
		System.out.println("E get(Object)="+ al.get(2));
	}
}
