import java.util.*;

class ArrayListDemo extends ArrayList{
	public static void main(String [] pdp){
		ArrayListDemo al = new ArrayListDemo();
		al.add(10);
		al.add(20.5f);
		al.add("Pranav");
		al.add(10);
		al.add(20.5f);

		System.out.println(al);
		System.out.println("int size()="+al.size()+"\n");

		System.out.println("Boolean contains(Object) = " + al.contains("Pranav"));
		System.out.println("Boolean contains(Object) = " + al.contains(15)+"\n");
		
		System.out.println("int indexOf(Object)="+al.indexOf(10));
		System.out.println("int indexOf(Object)="+al.indexOf("Pranav")+"\n");
		
		System.out.println("int lastIndexOf(Object)="+al.lastIndexOf(10));
		System.out.println("int lastIndexOf(Object)="+al.lastIndexOf(20.5f)+"\n");
		
		System.out.println("E get(Object)="+ al.get(3));
		System.out.println("E get(Object)="+ al.get(2)+"\n");
	
		System.out.println("E set(int,E)="+ al.set(3,"Rohit"));
		System.out.println("E set(int,E)="+ al.set(4,"Dnyanu")+"\n");

		al.add(5,"Adesh");
		System.out.println("o/p By =void add(int,E) :"+al+"\n");

		ArrayList al2 = new ArrayList();
		al2.add("Pranav");
		al2.add("Rohit");
		al2.add("Adesh");

		al.addAll(al2);
		System.out.println("o/p By =void addAll(Collection) :"+al);
		al.addAll(2,al2);
		System.out.println("o/p By =void addAll(int,Collection) :"+al+"\n");

		//al.removeRange(1,4);//removeRange() method is present in Another package Not in same file
		////this method is private in the ArrayList hence it is not accessible out side the 
					//the arraylist class,it is not assessible by using parent reference 
					//when the child class extends the parent ArrayList then by using the object of ArrayList
					//Child want to access the protected removeRange() in child class then also it is 
					//not accessible in child class
					
					//removeRange(int,int); it is only accessible in child class when child class 
					//extends ArrayList and  by using the object of child class we are able to access 
					//this protected method in child class
					

					//When proteced method in same file
					//Then we are able to access that method by using any way,parent ref,child ref,
					//out side the class also access is provided

		al.removeRange(1,4);
		System.out.println("Protected void removeRange(int,int); = "+ al + "\n");
	}
}
