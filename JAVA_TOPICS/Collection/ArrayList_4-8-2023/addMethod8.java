import java.util.*;

class ArrayListDemo{
	public static void main(String [] pdp){
		ArrayList al = new ArrayList();
		al.add(10);
		al.add(20.5f);
		al.add("Pranav");
		al.add(10);
		al.add(20.5f);

		System.out.println(al);
		System.out.println("int size()="+al.size()+"\n");

		System.out.println("Boolean contains(Object) = " + al.contains("Pranav"));
		System.out.println("Boolean contains(Object) = " + al.contains(15)+"\n");
		
		System.out.println("int indexOf(Object)="+al.indexOf(10));
		System.out.println("int indexOf(Object)="+al.indexOf("Pranav")+"\n");
		
		System.out.println("int lastIndexOf(Object)="+al.lastIndexOf(10));
		System.out.println("int lastIndexOf(Object)="+al.lastIndexOf(20.5f)+"\n");
		
		System.out.println("E get(Object)="+ al.get(3));
		System.out.println("E get(Object)="+ al.get(2)+"\n");
	
		System.out.println("E set(int,E)="+ al.set(3,"Rohit"));
		System.out.println("E set(int,E)="+ al.set(4,"Dnyanu"));
		
		al.add(5,"Adesh");
		System.out.println("o/p By =void add(int,E) :"+al+"\n");

		
	}
}
