import java.util.*;

class ArrayListDemo extends ArrayList{
	public static void main(String [] pdp){
		ArrayListDemo al = new ArrayListDemo();
		al.add(10);
		al.add(20.5f);
		al.add("Pranav");
		al.add(10);
		al.add(20.5f);

		System.out.println(al);
		System.out.println("int size()="+al.size()+"\n");

		System.out.println("Boolean contains(Object) = " + al.contains("Pranav"));
		System.out.println("Boolean contains(Object) = " + al.contains(15)+"\n");
		
		System.out.println("int indexOf(Object)="+al.indexOf(10));
		System.out.println("int indexOf(Object)="+al.indexOf("Pranav")+"\n");
		
		System.out.println("int lastIndexOf(Object)="+al.lastIndexOf(10));
		System.out.println("int lastIndexOf(Object)="+al.lastIndexOf(20.5f)+"\n");
		
		System.out.println("E get(Object)="+ al.get(3));
		System.out.println("E get(Object)="+ al.get(2)+"\n");
	
		System.out.println("E set(int,E)="+ al.set(3,"Rohit"));
		System.out.println("E set(int,E)="+ al.set(4,"Dnyanu")+"\n");

		al.add(5,"Adesh");
		System.out.println("o/p By =void add(int,E) :"+al+"\n");

		ArrayList al2 = new ArrayList();
		al2.add("Pranav");
		al2.add("Rohit");
		al2.add("Adesh");

		al.addAll(al2);
		System.out.println("o/p By =void addAll(Collection) :"+al);
		al.addAll(2,al2);
		System.out.println("o/p By =void addAll(int,Collection) :"+al+"\n");

		al.removeRange(1,4);
		System.out.println("Protected void removeRange(int,int); = "+ al + "\n");
		

		System.out.println("E remove(int) ="+al.remove(1));//Removed elements returns by this method
		System.out.println(al);

	}
}
