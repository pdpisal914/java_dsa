import java.util.*;
class Project {
	String pName=null;
	int team=0;
	int dur=0;

	Project(String pName,int team,int dur){
		this.pName=pName;
		this.team=team;
		this.dur=dur;
	}

	public String toString(){
		return "{"+pName+":"+team+":"+dur+"}";
	}
}
class SortByPName implements Comparator{

	public int compare(Object obj1,Object obj2){
		System.out.println(((Project)obj1).dur);
		return (((Project)obj1).pName).compareTo(((Project)obj2).pName);
	}
}
class PQDemo{
	public static void main(String [] pdp){
		PriorityQueue pq = new PriorityQueue(new SortByPName());
		pq.offer(new Project("Sagar",10,1));
		pq.offer(new Project("Krushna",10,2));
		pq.offer(new Project("Pranav",10,3));
		pq.offer(new Project("Vaibhav",10,4));
		pq.offer(new Project("Sanket",10,5));

		System.out.println(pq);
	}
}
