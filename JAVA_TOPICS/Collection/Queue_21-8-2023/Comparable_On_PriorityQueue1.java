import java.util.*;
class Project implements Comparable{
	String projName=null;
	int teamSize=0;
	int duration=0;

	Project(String projName,int teamSize,int duration){
		this.projName=projName;
		this.teamSize=teamSize;
		this.duration=duration;
	}

	public String toString(){
		return "{"+projName+":"+teamSize+":"+duration+"}";
	}
	public int compareTo(Object obj){
		System.out.println(duration);
		//stem.out.println(((Project)obj).projName);
		return projName.compareTo(((Project)obj).projName);
	}
}
class PQueueDemo{
	public static void main(String [] pdp){
		PriorityQueue pq = new PriorityQueue();

		pq.offer(new Project("Pranav",10,1));
		pq.offer(new Project("Vaibhav",20,2));
		pq.offer(new Project("Sagar",30,3));
		pq.offer(new Project("Rohit",30,4));
		pq.offer(new Project("Krushna",30,5));

		System.out.println(pq);
	}
}

