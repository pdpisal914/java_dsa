import java.util.*;
class QueueDemo{
	public static void main(String [] pdp){
		Queue que = new LinkedList();
		que.offer(10);
		que.offer(50);
		que.offer(40);
		que.offer(80);
		que.offer(20);

		System.out.println(que);

		que.poll();
		que.remove();

		System.out.println(que);

		System.out.println(que.peek());
		System.out.println(que.element());

		System.out.println(que);
	}
}
