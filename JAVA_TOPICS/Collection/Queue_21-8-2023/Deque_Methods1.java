//import java.util.ArrayDeque;
import java.util.*;
class DequeDemo1{
	public static void main(String [] pdp){
		Deque obj = new ArrayDeque();

		obj.offer(10);
		obj.offer(40);
		obj.offer(20);
		obj.offer(30);

		System.out.println(obj);

		//offerFirst()
		//offerLast()

		obj.offerFirst(5);
		obj.offerLast(50);

		System.out.println(obj);

		//pollFirst();
		//pollLast();
		
		obj.pollFirst();
		obj.pollLast();

		System.out.println(obj);

		//peekFirst();
		//peekLast();

		System.out.println(obj.peekFirst());
		System.out.println(obj.peekLast());

		System.out.println(obj);//[10,40,20,30]

		//iterator()
		Iterator itr = obj.iterator();
		while(itr.hasNext()){
			System.out.println(itr.next());
		}

		Iterator itr2 = obj.descendingIterator();

		while(itr2.hasNext()){
			System.out.println(itr2.next());
		}

	}
}

