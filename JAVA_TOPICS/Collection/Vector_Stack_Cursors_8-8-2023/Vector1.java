import java.util.*;

class VectorDemo{
	public static void main(String [] pdp){
		Vector vc = new Vector();

		vc.addElement(10);
		vc.addElement(20);
		vc.addElement(30);
		vc.addElement(40);
		vc.addElement(50);

		System.out.println(vc);
		System.out.println("Capacity ="+vc.capacity());
		
		vc.addElement(60);
		vc.addElement(70);
		vc.addElement(80);
		vc.addElement(90);
		vc.addElement(100);
		vc.addElement(110);
		
		System.out.println(vc);
		System.out.println("Capacity ="+vc.capacity());
	}
}

