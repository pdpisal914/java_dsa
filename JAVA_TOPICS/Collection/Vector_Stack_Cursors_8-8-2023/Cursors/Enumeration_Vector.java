import java.util.*;
class Enumeration_Vector{
	public static void main(String [] pdp){
		Vector vc = new Vector();
		vc.addElement(10);
		vc.addElement(100);
		vc.addElement(1000);
		vc.addElement(10000);

		System.out.println(vc);

		Enumeration curser = vc.elements();
		System.out.println(curser.getClass().getName());

		while(curser.hasMoreElements()){
			System.out.println(curser.nextElement());
		}
	}
}
