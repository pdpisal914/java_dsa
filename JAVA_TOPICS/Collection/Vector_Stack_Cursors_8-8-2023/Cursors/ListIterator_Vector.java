import java.util.*;
class ListIterator_Vector{
	public static void main(String [] pdp){
		Vector vc = new Vector();
		vc.addElement(10);
		vc.addElement(20);
		vc.addElement(30);
		vc.addElement(40);
		vc.addElement(50);
		System.out.println(vc);

		ListIterator curser = vc.listIterator();
		System.out.println("Type of Curser = "+curser.getClass().getName());
		
		while(curser.hasNext()){
			System.out.println(curser.next());
		}

		System.out.println("HasPrevious()");
		while(curser.hasPrevious()){
			System.out.println(curser.previous());
		}
	}
}
