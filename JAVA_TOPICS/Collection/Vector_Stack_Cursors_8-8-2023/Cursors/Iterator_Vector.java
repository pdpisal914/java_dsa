import java.util.*;

class Iterator_Vector{
	public static void main(String [] pdp){
		Vector vc = new Vector();
		vc.addElement(10);
		vc.addElement(20);
		vc.addElement(30);
		vc.addElement(40);
		vc.addElement(50);

		System.out.println(vc);

		Iterator curser = vc.iterator();
		System.out.println("Type of Curser ="+curser.getClass().getName());

		while(curser.hasNext()){
			System.out.println(curser.next());
		}
	}
}

