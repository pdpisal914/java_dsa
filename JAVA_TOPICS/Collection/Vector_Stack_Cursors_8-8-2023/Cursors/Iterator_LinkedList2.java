import java.util.*;

class IteratorLinkedListDemo{
	public static void main(String [] pdp){
		LinkedList ll = new LinkedList();
		ll.addFirst(10);
		ll.add(30);
		ll.addLast(65);
		ll.add(876);

		System.out.println(ll);

		Iterator curser = ll.iterator();
		System.out.println("Type of Curser="+ll.getClass().getName());

		while(curser.hasNext()){
			System.out.println(curser.next());
		}
	}
}
