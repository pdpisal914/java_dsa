import java.util.*;
class ListIterator_Stack{
	public static void main(String [] pdp){
		Stack st = new Stack();
		st.push(10);
		st.push(20);
		st.push(30);
		st.push(40);
		st.push(50);

		System.out.println(st);

		ListIterator curser = st.listIterator();
		System.out.println("Type of Curser ="+curser.getClass().getName());

		while(curser.hasNext()){
			System.out.println(curser.next());
		}

		System.out.println("hasPrevious()");
		while(curser.hasPrevious()){
			System.out.println(curser.previous());
		}
	}
}
