import java.util.*;

class ListIterator_LinkedList{
	public static void main(String [] pdp){
		LinkedList ll = new LinkedList();

		ll.add(10);
		ll.add(20);
		ll.add(30);
		ll.add(40);
		ll.add(50);

		System.out.println(ll);

		ListIterator curser = ll.listIterator();

		System.out.println("Type of curser = "+curser.getClass().getName());

		while(curser.hasNext()){
			System.out.println(curser.next());
		}

		System.out.println("hasPrevious()");
		while(curser.hasPrevious()){
			System.out.println(curser.previous());
		}
	}
}


