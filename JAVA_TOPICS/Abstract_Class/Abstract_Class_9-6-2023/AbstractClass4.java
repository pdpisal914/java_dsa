abstract class Parent{
	void Carrer(){
		System.out.println("Doctor");
	}
	abstract void Marry();
}
abstract class Child extends Parent{
	void Money(){
		System.out.println("100000000");

	}
	abstract void Job();
}
class Child1 extends Child{
	void Marry(){
		System.out.println("Rashmika Mandanna");
	}
	void Job(){
		System.out.println("Backend Developer");
	}
}
class Client{
	public static void main(String [] pdp){
		Child1 obj = new Child1();
		obj.Carrer();
		obj.Money();
		obj.Marry();
		obj.Job();
	}
}

