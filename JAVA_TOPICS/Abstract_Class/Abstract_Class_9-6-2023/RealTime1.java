abstract class Mobile{
	void FixedFeatures(){
		System.out.println("Display,Charger,Sound,Camera,Mike,Sim");
	}
	abstract void ExtraFeatures();
}
class User1 extends Mobile{
	void ExtraFeatures(){
		System.out.println("8gb RAM,256gb ROM,128mp Camera,Dolby Sound,Amulated Display");
	}
}
class User2 extends Mobile{
	void ExtraFeatures(){
		System.out.println("16gb RAM,128gb ROM,64mp Camera,Double Sound,Oled Display");
	}
}
class Customer{
	public static void main(String [] pdp){
		User1 obj1 = new User1();
		obj1.FixedFeatures();
		obj1.ExtraFeatures();

		User2 obj2 = new User2();
		obj2.FixedFeatures();
		obj2.ExtraFeatures();
	}
}

