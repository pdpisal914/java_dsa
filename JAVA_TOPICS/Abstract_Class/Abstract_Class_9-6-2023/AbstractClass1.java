abstract class Demo{
	void Carrer(){
		System.out.println("Doctor");
	}
	abstract void marry();
}


//Abstract class havin 0% to 100% abstraction

/*Parent knows which which methods of the parent class is override by the child class 
...then parent does not give the body to that overriden methods...
Only The declaration of this methods is present in the parent class../*

/*At least One method in the class does not have body ,only the declaration of this method is present in the class
then this method in class is called Abstract Method,
If At least one method in class is Abstract Then the whole class is becomes Abstract class*/

/*The method which are Abstract in the Parent class the Child Have an responsibility to give the body to that method
If the Child class does not givce the body to the Abstract method then Compiler tells us to make the Child class as an Abstract*/

//We are not able to make the object of Any Abstract class...

//INTERFACE = All methods in the class is Abstract then this class is called INTERFACE
