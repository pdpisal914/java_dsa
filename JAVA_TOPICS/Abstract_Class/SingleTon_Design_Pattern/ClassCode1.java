class Singleton{
	static Singleton obj = new Singleton();
	private Singleton(){
		System.out.println("In Singleton Constructor");
	}
	static Singleton GetObj(){
		return obj;
	}
}
class Client{
	public static void main(String [] pdp){
		Singleton obj1 = Singleton.GetObj();
		System.out.println(obj1);
		Singleton obj2 = Singleton.GetObj();
		System.out.println(obj2);
		Singleton obj3 = Singleton.GetObj();
		System.out.println(obj3);
	}
}
