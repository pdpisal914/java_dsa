class SingleTon{
	static SingleTon obj = new SingleTon();
	private SingleTon(){
		System.out.println("SingleTon Constructor");
	}
	static SingleTon GetObj(){
		return obj;
	}
	void Info(){
		System.out.println("Only Single Object is Created");
	}


}
class Client{
	public static void main(String [] pdp){
		System.out.println("In Main");
	SingleTon obj1 = SingleTon.GetObj();
	obj1.Info();
	SingleTon obj2 = SingleTon.GetObj();
	obj2.Info();
	SingleTon obj3 = SingleTon.GetObj();
	obj3.Info();
	}
}
