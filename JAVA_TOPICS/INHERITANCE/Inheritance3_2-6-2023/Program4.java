class Parent{
	int x=10;
	static int y=20;
	static{
		System.out.println("In Parent Static Block");
	}
	Parent(){
		System.out.println("In Parent Constructor");
	}

	void Method(){
		System.out.println(x);
		System.out.println(y);
	}
	static void MethodTwo(){
		System.out.println(y);
	}
}
class Child extends Parent{
	static{
		System.out.println("In Child Static Block");
	}
	Child(){
		System.out.println("In Child Constructor");
	}
	{
		
	//	super();
		super.Method();

	}
}


class Client{
	public static void main(String [] pdp){
		Child obj = new Child();
		obj.Method();
		obj.MethodTwo();
	}
}

