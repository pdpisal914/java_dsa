class Parent{
	static int x=10;
	static{
		System.out.println("In Parent Static Block");
	}

	static void Access(){
		System.out.println(x);
	}
}
class Child extends Parent{
	static{
		System.out.println("In Child Static Block");
		System.out.println(x);
		Access();
	}
}
class Client{
	public static void main(String [] pdp){
		Child obj = new Child();
	}
}

