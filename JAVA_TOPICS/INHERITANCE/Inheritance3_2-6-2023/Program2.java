class Parent{
	static{
		System.out.println("In Static Block of Parent");
	}

}
class Child extends Parent{
	static{
		System.out.println("In Child Static Block");
	}
}

class Client{
	public static void main(String [] pdp){
		Child obj = new Child();
	}
}

