class Parent{
	int x=10;
	static int y=20;
	Parent(){
		System.out.println("In Parent Constructor");
	}
}
class Child extends Parent{
	int x=100;
	static int y=200;
	Child(){
		System.out.println("In Child Constructor");
	}
	void Access(){
		System.out.println("In Client Fun");
		System.out.println(super.x);
		System.out.println(super.y);
		System.out.println(x);
		System.out.println(y);
	}

}
class Child1 extends Child{
	int x=1000;
	static int y=2000;

	Child1(){
		System.out.println("In Child 1 Constructor");
	}
	void Access1(){
		System.out.println("In Client 1 Fun");
	//	System.out.println(super.x);
	//	System.out.println(super.y);
		System.out.println(super.x);
		System.out.println(super.y);
		System.out.println(x);
		System.out.println(y);
	}
}
class Client{
	public static void main(String [] pdp){
		Child1 obj = new Child1();
		obj.Access();
		obj.Access1();
	}

}
