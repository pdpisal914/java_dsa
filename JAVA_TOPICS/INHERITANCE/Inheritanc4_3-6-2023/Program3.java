//Super = if Parent class And Child class Having Same variables and Same methods then by using super we can able to access the variables and methods of Parent class
class Parent{
	int x=100;
	static int y=200;

	void Method(){
		System.out.println(x);
		System.out.println(y);
	}
}
class Child1 extends Parent{
	int x=10;
	static int y=20;
	Child1(){
		super.Method();
		Method();
	}

	void Method(){
		System.out.println(super.x);
		System.out.println(super.y);
		System.out.println(x);
		System.out.println(y);
	}
}
class Client{
	public static void main(String [] pdp){
		Child1 obj = new Child1();
	}
}



