//Static modifier is not allowed to the Constructor
class Player{
	static Player(){
		System.out.println("Static Constructor");
	}
	public static void main(String [] pdp){
		Player obj = new Player();
	}
}

