class Player{
	private int jerNo=0;
	private String name=null;

	Player(int jerNo,String name,String str){
		this.jerNo=jerNo;
		this.name=name;
		System.out.println("In Constructor");
		System.out.println("name = " +System.identityHashCode(name));
		System.out.println("str = " +System.identityHashCode(str));
		System.out.println("this.name = " +System.identityHashCode(this.name));
	}

	void info(){
		//me="MSD";
		String str1="Virat";
		String str2="MSD";
		String str3="Rohit";

		System.out.println("this.name = " +System.identityHashCode(this.name));
		System.out.println("Virat = "+System.identityHashCode(str1));
		System.out.println("MSD = "+System.identityHashCode(str2));
		System.out.println("Rohit = "+System.identityHashCode(str3)+"\n");
	}
}
class Client{
	public static void main(String [] pdp){
		Player obj1=new Player(18,"Virat","Virat");
		obj1.info();

		Player obj2=new Player(7,"MSD","MSD");
		obj2.info();

		Player obj3=new Player(45,"Rohit","Rohit");
		obj3.info();

	}
}
