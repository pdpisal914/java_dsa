class Parent{
	Parent(){
		System.out.println(this);
		System.out.println("In Parent Constructor");
	}

	void info(){
		System.out.println("In Client Constructor");
	}
}

class Child extends Parent{
	Child(){
		System.out.println(this);
		System.out.println("In Child Constructor");
	}
}
class Client{
	public static void main(String [] pdp){
		Child obj = new Child();
		System.out.println(obj);
	}
}
