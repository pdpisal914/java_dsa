class Parent{
	Parent(){
		System.out.println("In Parent Constructor");
	}
	void info(){
		System.out.println("In Parent Function");
	}
}

class Child extends Parent{
	Child(){
		this();
		this.info();
		System.out.println("In Child Constructor");
	}
}
class Client{
	public static void main(String [] pdp){
		Child obj = new Child();
	}
}

