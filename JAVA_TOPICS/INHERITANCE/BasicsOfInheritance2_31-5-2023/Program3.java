class Parent{
	int x=10;

	{
		System.out.println("In Instance block of Parent");
	}

	Parent(){
		System.out.println("Parent Constructor");
	}

	void fun(){
		System.out.println("In Parent Fun");
	}
}

class Child extends Parent{
	int y=20;
	{
		System.out.println("In Child Instance");
		System.out.println("In Child Instance = "+x+y);
	}

	Child(){
		System.out.println("In Child Constructor");

	}
	void gun(){
		fun();
		System.out.println("In gun");
	}
//fun();
}
class Client{
	public static void main(String [] pdp){
		Child obj = new Child();
		obj.gun();
	}
}


