interface Parent{
	void carrer();//Compiler by default adds the = Public Abstract
	void marry();.//Compiler by default adds the =Public Abstract
	void friends();//In Front Of friend method compiler by default add =Public Abstract
}
class Child implements Parent{
	public void carrer(){
		System.out.println("Doctor");
	}
	public void marry(){
		System.out.println("Rashmika Mandanna");
	}
	public void friends(){
		System.out.println("Rohit Lahamate");
	}
}
class Client{
	public static void main(String [] pdp){
		Child obj = new Child();
		obj.carrer();
		obj.marry();
		obj.friends();
	}
}

