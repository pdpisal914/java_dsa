interface Demo{
	int x=10;
}
interface Demo1{
	int y=20;
}
class Child extends Object implements Demo,Demo1{
	void fun(){
		System.out.println(x);
		System.out.println(y);
	}
}
class Client{
	public static void main(String [] pdp){
		Child obj = new Child();
		obj.fun();
		
		Object obj = new Child();
		//It checks the Method signature at compile time
		obj.fun();//The fun is not present in the object class hence they give error at compile time
	}
}


