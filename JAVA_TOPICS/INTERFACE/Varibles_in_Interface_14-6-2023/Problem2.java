interface Demo{
	int x=10;
	void fun();
}
class DemoChild implements Demo{
	public void fun(){
		//In Bytecode
		//bipush x=10;
		System.out.println(x);
		//bipush x=10;
		System.out.println(x);
		//bipush x=10;
		System.out.println(Demo.x);
	}
}
class Client{
	public static void main(String [] pdp){
		DemoChild obj = new DemoChild();
		obj.fun();
	}
}
