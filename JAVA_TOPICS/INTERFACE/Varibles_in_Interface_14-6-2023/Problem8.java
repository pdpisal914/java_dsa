
class Temp{
	void gun(){
		System.out.println("In Temp Class");
	}
}
interface Demo{
	Temp obj = new Temp();//Object created in INTERFACE
			      //Static Block Added In Bytecode
			      //static{};
}
class DemoChild implements Demo{
	void fun(){
		obj.gun();
	}
}
class Client{
	public static void main(String [] pdp){
		DemoChild obj1= new DemoChild();
		obj1.fun();
	}
}
