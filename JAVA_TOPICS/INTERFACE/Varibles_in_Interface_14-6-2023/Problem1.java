//Every variable in the interface is by Default Public Static Final .
//the compiler add the public static final in front of our variable declaration
//PUBLIC = Any classes having an access of that variable of interface hence it is public by default
//Static = The variable is by default Static because their is no constructor in interface for initializing the 
//		value of instance variable...and we are not able trro create theobject of tthe interface...
//		Bcoz of that their is only one way are remaining to Access the variable With the Help of Interface Name
//		For accessing the variable by using interface name it requird to be the static boc only the static things
//		can be accessed by using the Class Name / Interface Name
//Final = All variables in the interface is Final bcoz they dont want to change the value of that variable
//		they dont give the permission for anyone to change their value hence they declair that variable as Final
//
//INITIALIZATION Of Static VAriable in Interface = For initialiization of static variable/variable of 
//			interface their is no static block are come in picture 
//			in interface...the static variable of interface dont have locationn on the Method Area 
//			they present in the stack frame
//			where we want to access that variable above that access line the bipush line for the variable is present
//			i.e bipush    //x=10;;
//			SOP(x);
//
//			if we access that variable for multiple time then the variable is also initialize the multiple time
//			i.e bipush //x=10;
//			SOP(x);
//			i.e bipush   //x=10;
//			SOP(x);
//
interface Demo{
	int x=10;//public static final void int x;

	void fun();//public abstract void fun();
}

