class Demo{
		void fun(){
			System.out.println("In Demo Class");
		}
}

interface Inter{

	Demo objInter = new Demo();
	//Now the Static Block is Come in Interface for creating Object
	//static{};
/*	static{				// ERROR = Initializres Not Allow In Interface
		System.out.println("Static Block");
	}
*/
}
class ChildInter implements Inter{
	void gun(){
		objInter.fun();
	}
}
class Client{
	public static void main(String [] pdp){
		ChildInter obj = new ChildInter();
		obj.gun();
	}
}

