interface A{
	int x=10;
}
interface B{
	int x=20;
}
class child implements A,B{
	int x=30;
	void fun(){
		System.out.println(x);
		System.out.println(A.x);
		System.out.println(B.x);
	//	System.out.println(child.x);ERROR = Non static variable cannot be referenced from static context
	}
}
class Client{
	public static void main(String [] pdp){
		child obj = new child();
		obj.fun();
	}
}

