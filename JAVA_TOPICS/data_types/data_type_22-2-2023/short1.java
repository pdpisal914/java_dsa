class Demo{
	public static void main(String [] args){
		short x=10;
		short y=20;
		int z;
		System.out.println("x="+x);
		System.out.println("y="+y);

		//x=x*y;//error=incompatible types=possible lossy conversion from int to short
		z=x*y;
		System.out.println("z="+z);
		System.out.println("y="+y);
	}
}

