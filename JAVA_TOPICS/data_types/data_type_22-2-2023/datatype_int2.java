class Demo{
	public static void main(String [] pdp){
		int var1=12;
		int var2=23;
		//byte var3;
		int var3;//it is right way
		System.out.println(+var1);
		System.out.println(+var2);

		var3=var1+var2;//ERROR=incompatible type=possible lossy conversion of int to byte
		System.out.println(+var3);
	}
}
//it gives an error bcoz we declair var 1 and var2 are integer and we want to store their sum in var3 which have byte datatype
//size of int is 4 and size of byte is 1....we want to store 4byte data in 1 byte storage of byte datatype....during that some data are lossses hence it give an error
