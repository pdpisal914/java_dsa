//Correct way of declairing Float
class HowFloat{
	public static void main(String [] pdp){
		float f1=7.5f;
		float f2=7.5f;
		System.out.println(f1);
		System.out.println(f2);
	}
}
/*class HowFloat{
	public static void main(String [] pdp){
		float f1=7.5;//It is Wrong bcoz the jvm comsider any decimal value directly as a double ..and then jvm feels that we 
				store the doulbe data into the float data hence here we specifically 
				required to tell to the jvm the data is float by adding "f" with the data
		float f2=7.5;
		System.out.println(f1);
		System.out.println(f2);
	}
}*/
