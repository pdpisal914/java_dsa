class Demo{
	public static void main(String [] pdp){
		byte x=18;
		byte y=22;
		int z;
		System.out.println(x);
		System.out.println(y);
		
//		x=x+y;//ERROR=incompatible types=possible lossy conversion of integer to byte
	z=x+y;
		System.out.println(z);
		System.out.println(y);
	}
}

/*Error=
		x=x+y;//ERROR=incompatible types=possible lossy conversion of integer to byte
bcoz when jvm on ram do any mathematical operations at run time ...then jvm give INT datatype to the output value of this mathematical operations ...
Hence bcoz of that we are try to store the integer output into the byte variable...we try to store large spaced value in less spaced value...hence it give an error bcoz if we done storing that int into byte large size data will be lossed*/
