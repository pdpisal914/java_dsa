class StringDemo{
	public static void main(String [] pdp){

		String str1 = "Pranav Pisal";
		String str2 = "Pranav"+" "+"Pisal";

		System.out.println(str1 == str2);
		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str2));

		String str3 = "Pranav ";
		String str4 = str3 + "Pisal";

		System.out.println(str2 == str4);
		System.out.println(System.identityHashCode(str3));
		System.out.println(System.identityHashCode(str4));
	}
}
