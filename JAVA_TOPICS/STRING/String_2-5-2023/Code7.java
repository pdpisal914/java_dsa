class StringDemo{
	public static void main(String [] pdp){
		String str="ROHIT";//SCP
		String str1=str;//SCP
		String str2=new String(str1);//HEAP

		System.out.println(System.identityHashCode(str));//1000(SCP)
		System.out.println(System.identityHashCode(str1));//1000(SCP)
		System.out.println(System.identityHashCode(str2));//2000(heap)
	}
}
