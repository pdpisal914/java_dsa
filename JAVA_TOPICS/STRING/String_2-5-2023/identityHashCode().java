class StringDemo{
	public static void main(String [] pdp){
		String str=new String("PRANAV");
		String str1=str;
		String str2=new String(str);
		String str3=str1;
		String str4="PRANAV";
		String str5="PRANAV";

		System.out.println(System.identityHashCode(str));
		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str2));
		System.out.println(System.identityHashCode(str3));
		System.out.println(System.identityHashCode(str4));
		System.out.println(System.identityHashCode(str5));
	}
}
