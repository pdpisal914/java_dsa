class StringDemo{
	public static void main(String [] pdp){
		String str=new String("PRANAV");
		String str1=str;
		String str2=new String(str);
		String str3=str1;
		String str4="PRANAV";
		String str5="PRANAV";

		System.out.println(str.hashCode());
		System.out.println(str1.hashCode());
		System.out.println(str2.hashCode());
		System.out.println(str3.hashCode());
		System.out.println(str4.hashCode());
		System.out.println(str5.hashCode());
	}
}

/*NOTE=HashCode();this function is depend on the content present in the object
if the content in the object is same then this function returns the same hashcode for all that objects
in this code str,str1,str2,str3,str4,str5 all have the same content hence obj.hashCode(); method 
generate the same code for all strings*/
