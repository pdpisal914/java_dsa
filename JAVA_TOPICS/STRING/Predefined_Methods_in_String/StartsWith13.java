/*
startsWith

public boolean startsWith(String prefix,int toffset);

	-predicate which determines if the given String contains the given prefix
	beginning comparison at toffset
	-The result is false if the toffset is negative or greater than str,length();
	parameters.Prefix String to compare , toffset for this string where the compariosn starts

	ReturnType = Boolean

*/
import java.io.*;
class StartsWithDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the String");
		String str = br.readLine();

		System.out.println("Enter the String tofind Starting");
		String str1 = br.readLine();

		System.out.println("Enter the Index for start ");
		int index = Integer.parseInt(br.readLine());

		System.out.println(str.startsWith(str1,index));

	}
}

