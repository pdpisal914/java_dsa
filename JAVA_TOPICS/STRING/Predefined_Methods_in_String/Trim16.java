/*
trim();

	public String trim();

	-trims all the white spaces before and after of the String
	-it removes the white spaces before and After the String

	ex.str.trim();

	Parameters = NO Parameter
	Return Type = String;
*/

import java.io.*;
class TrimDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the String");
		String str = br.readLine();

		System.out.println(str.trim());

	}
}

