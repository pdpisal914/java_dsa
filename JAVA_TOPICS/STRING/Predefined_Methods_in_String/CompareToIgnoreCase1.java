/*
public int compareToIgnorecase(String str);
Description = It compares the str1 and str2(Case Insensitive);

parameter = String
Return Type = Integer
*/
import java.io.*;
class CompareIgnoreDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the First String");
		String str1=br.readLine();

		System.out.println("Enter the Second String");
		String str2=br.readLine();

	System.out.println(str1.compareToIgnoreCase(str2));
	}
}
