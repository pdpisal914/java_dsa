/*Public boolean equalsIgnoreCase(String anotherString);

Description = Compares a String to this String ignoring case

Parameter = String(str2);
Return Type = boolean
*/

class Code{
	public static void main(String [] pdp){
		String str1 = "Pranav";
		String str2 = "Pranav";
		System.out.println(str1.equalsIgnoreCase(str2));//True
		String str3 = "Pranav";
		String str4 = "pranav";
		System.out.println(str3.equalsIgnoreCase(str4));//True
		String str5 = "Pranav";
		String str6 = "Pisal";
		System.out.println(str5.equalsIgnoreCase(str6));//False
	}
}

