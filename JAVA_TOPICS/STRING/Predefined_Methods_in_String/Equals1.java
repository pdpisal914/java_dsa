/*
EQUALS

public boolean equals(Object anObject);

Description = Predicate which compares and object to this.
		This is true only for String with the same character sequence
		returns true if anObject is semantically equal to this.

Parameter = Object(anObject);
Return Type = boolean;
*/
import java.io.*;
class EqualsDemo{
	public static void main(String [] pdp) throws IOException{ 
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the First String");
		String str1 = br.readLine();
		System.out.println("Enter the Second String");
		String str2 = br.readLine();

		System.out.println(str1.equals(str2));//true
	}
}
