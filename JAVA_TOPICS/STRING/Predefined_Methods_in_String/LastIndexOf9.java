/*Prototype : public int lastIndexOf(char ch,int fromIndex);

Description : Finds the last Instance of the Character in the given String

Parameter : Character(ch to find),Integer(index of start the search(This search is travel from LEFT to RIGHT));
Return_Type : Integer
*/
import java.io.*;
class LastIndexDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the String");
		String str = br.readLine();

		System.out.println("Enter the character");
		char ch = (char)br.read();
		br.skip(1);

		System.out.println("Enter the starting index for lastIndex");
		int index = Integer.parseInt(br.readLine());

		System.out.println(str.lastIndexOf(ch,index));
	}
}
