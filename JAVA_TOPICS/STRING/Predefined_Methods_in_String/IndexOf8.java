/*Prototype : public int indeOf(char ch,int fromIndex);

Description : Finds the first instance of the Character in the given String
Parameter : Character(ch to find),Integer(index to start the search);
Return Type : Integer
*/
import java.util.*;
class IndexOfDemo{
	public static void main(String [] pdp) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the String");
		String str=sc.nextLine();

		System.out.println("Enter the character");
		char ch=sc.next().charAt(0);

		System.out.println("Enter the indexr");
		int index=sc.nextInt();

		System.out.println(str.indexOf(ch,index));//2
		
	}
}
