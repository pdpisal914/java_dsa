/*Prototype : String replace(char oldChar,char newChar);

Description : Replace every instance of a character oin the given String with new Character

Parameter : Character(old character),character(new character);
Return_Type : String
*/
import java.io.*;
class ReplaceCharPDP{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the String");
		String str = br.readLine();

		System.out.println("Enter the old character");
		char oldch=(char)br.read();
		br.skip(1);

		System.out.println("Enter the new Character");
		char newch=(char)br.read();
		br.skip(1);

		String ret = str.replace(oldch,newch);
		System.out.println(ret);//"Pranav Pisal"
	}
}

