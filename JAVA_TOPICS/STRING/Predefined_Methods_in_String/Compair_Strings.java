/*
 public int compareTo(String str2);

Description = It compare the str1 and str2(Case Sensitive) , if both the Strings are equal it return '0' otherwise 
		it return Comparison(Return the difference between the ascii values);

Parameter = String(second string);
Return Type = integer
*/

class Compare_String{
	public static void main(String [] pdp){

		String str1 = "Ashish";
		String str2="ashish";
		System.out.println(str1.compareTo(str2));//-32
		
		String str3 = "Ashish";
		String str4="ashish Khare";
		System.out.println(str3.compareTo(str4));//-32
		
		String str5 = "Ashish";
		String str6="Ashish Khare";
		System.out.println(str3.compareTo(str4));//0
	}
}

