/*
public String concat(String str);

info=Concatinate String to this String .i.e.Another string is concatinate with the First String
	Implements new Array of Character whose length is sum of str1.length and str2.length

Parameter = String
Return Type=String;
*/

class ConcatDemo{
	public static void main(String [] pdp){
		String str1="Core2";
		String str2="Web";

		String str3=str1.concat(str2);

		System.out.println(str3);
	}
}
