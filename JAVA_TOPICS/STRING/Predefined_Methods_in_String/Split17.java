/*
SPLIT9();

	publicString[] split(String delimeter);

	-Splits this String around matches of regular expressions

	Parameter = delimeter(Pattern to match);
	Return Type = String[] (array of split Strings);
*/

import java.io.*;
class SplitDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the String");
		String str = br.readLine();


		System.out.println("Enter the Delimeter");
		String deli = br.readLine();
		
		String str2 [] = str.split(deli);

		for(int i=0;i<str2.length;i++){
			System.out.println(str2[i]);
		}
	}
}
