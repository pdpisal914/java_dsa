/*
endsWith();

public boolean endsWith(String suffix);

	-predicate which determines if the string ends with given suffix
	-if the suffix is an empty String Then true is returned.
	-throws NullPointerException if suffix is null

	Parameters = prefix String to compare
	Return type = Boolean
*/
import java.io.*;
class EndsWithDemo{
	public static void main(String [] pdp)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the String");
		String str = br.readLine();

		System.out.println("Enter the string to find endWith");
		String str2 = br.readLine();

		System.out.println(str.endsWith(str2));

	}
}

