/*Prototype : public string subString(int index);

Description : Create a substring of the given String starting at a specified index and ending at the end of given String

Parameter : Integer(index of the substring);

Return_Type : String
*/
import java.io.*;
class SubStringRowdy{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the String");
		String str = br.readLine();
		
		System.out.println("Enter the Starting index Of Substring");
		int index = Integer.parseInt(br.readLine());

		String ret = str.substring(index);
		System.out.println(ret);
		
	}
}

