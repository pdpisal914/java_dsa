/*public char charAt(int index);

Description = It returns the Character located at Specified index
		within the given String

Parameter = integer(index);
return Type = character
*/

class CharAtDemo{
	public static void main(String [] pdp){

		String str = "Core2Web";

		System.out.println(str.charAt(4));//2
		System.out.println(str.charAt(-1));//C
		System.out.println(str.charAt(8));//StringIndexOutOfBoundException
	}
}
