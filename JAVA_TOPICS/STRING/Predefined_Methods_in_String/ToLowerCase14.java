/*toLowerCase();
	public String toLowerCase();

	-it lowercases to this String
	ex.str.toLowerCase();

	Parameters = NO Parameters;
	Return Type = String;

	*/

import java.io.*;
class ToLowerCaseDemo{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the String");
		String str = br.readLine();

		System.out.println(str.toLowerCase());
	}
}

