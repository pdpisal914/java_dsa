/*Length of String

public int length();

Description = It returns the number of characters contained in given String
		

Parameter = No parameter
Return Type = Integer
*/

class StringLengthDemo{
	public static void main(String [] pdp){

		String str1="Core2Web";

		System.out.println(str1.length());
	}
}

