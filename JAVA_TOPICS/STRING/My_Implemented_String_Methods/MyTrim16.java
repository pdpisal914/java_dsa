import java.io.*;
class TrimDemo{
	static String MyTrimFun(char [] arr){
		int start=0;
		for(int i=0;i<arr.length;i++){
			if(arr[i] != ' '){
				start=i;
				break;
			}
		}
		int end =0;
		for(int i=arr.length-1;i>=start;i--){
			if(arr[i] != ' '){
				end = i;
				break;
			}
		}

		char [] result=new char[end-start+1];
		
		for(int i=start,j=0;i<=end;i++,j++){
			result[j] = arr[i];
		}
		
		String str = new String(result);
		return str;
	}
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the String");
		String str = br.readLine();

		System.out.println(MyTrimFun(str.toCharArray()));
	}
}
