import java.io.*;
class LengthOfString{
	static int MyLength(String arr){
		int cnt=0;
		char [] x=arr.toCharArray();
		for(char ch : x){
			cnt++;
		}
		return cnt;
	}

	public static void main(String [] pdp) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));	
		System.out.println("Enter the string");
		String str = br.readLine();


		System.out.println("Length of String = "+ MyLength(str));
	}
}

