import java.io.*;
class Appendable{
	static int MyLastIndexOf(char [] arr,char ch,int index){
		if(index < 0){
			return -1;
		}

		if(index>=arr.length){
			index = arr.length-1;
		}

		for(int i=index;i>=0;i--){
			if(ch == arr[i]){
				return i;
			}
		}
		return -1;
	}

	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader( new InputStreamReader(System.in));
		System.out.println("Enter the String");
		String str = br.readLine();

	System.out.println("Enter the character");
		char ch = (char)br.read();
		br.skip(1);

		System.out.println("Enter the index for LastindexOf");
		int index = Integer.parseInt(br.readLine());

		int ret=MyLastIndexOf(str.toCharArray(),ch,index);
		if(ret != -1){
			System.out.println("index of "+ch+"is "+ret);
		}else{
			System.out.println("Wrong input");
		}
	}
}

