import java.io.*;
class ConcatStrings{

	static char[] MyConcat(char [] arr1,char [] arr2){
		int len = arr1.length + arr2.length;
		char arr[] = new char[(len+1)];
		int i=0;

		for(char x : arr1){
			arr[i]=x;
			i++;
		}
		
		arr[i]=' ';
		i++;

		for(char y : arr2){
			arr[i]=y;
			i++;
		}
		return arr;
			
	}
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the String 1");
		String str1=br.readLine();

		System.out.println("Enter the String 2");
		String str2 = br.readLine();

		System.out.println(MyConcat(str1.toCharArray(),str2.toCharArray()));
	}
}
