import java.io.*;
class SelfImplemented{
	static int MyIndexOf(char [] arr,char ch,int index){
		if(index >=arr.length){
			return -1;
		}
		if(index <0){
			index = 0;
		}
		for(int i=index;i<arr.length;i++){
			if(arr[i] == ch){
				return i;
			}
		}
		return -1;
	}

	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the String");
		String str = br.readLine();

		System.out.println("Enter the character");
		char ch = (char)br.read();
		br.skip(1);

		System.out.println("Enter the starting index");
		int index = Integer.parseInt(br.readLine());

		System.out.println(MyIndexOf(str.toCharArray(),ch,index));
	}
}
