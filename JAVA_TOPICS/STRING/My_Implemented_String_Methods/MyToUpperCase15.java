import java.io.*;
class UpperCase{
	static String myToUpperCase(char [] arr){

		for(int i=0;i<arr.length;i++){
			if(arr[i]>=97 && arr[i]<=122){
				arr[i]-=32;
			}
		}
		String str = new String(arr);
		return str;
	}

	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the String");

		String str = br.readLine();

		System.out.println(myToUpperCase(str.toCharArray()));
	}
}
