import java.io.*;
class EndsWithDemo{
	static boolean myEndsWith(char [] arr1,char arr2[]){
		for(int i=(arr2.length-1),j=(arr1.length-1);i>=0;i--,j--){
			if(arr2[i] != arr1[j]){

				return false;
			}
		}
		return true;
	}

		
	public static void main(String [] pdp) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the String");
		String str1 = br.readLine();

		System.out.println("Enter the String to Check EndsWith Condition");
		String str2 = br.readLine();

		System.out.println(myEndsWith(str1.toCharArray(),str2.toCharArray()));
	}
}

