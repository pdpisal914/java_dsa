import java.io.*;
class SubStringDemo{
	static String MySubStringFun(char [] arr,int index){
		char [] cpy = new char[arr.length - index];

		for(int i=index,j=0;i<arr.length;i++,j++){
			cpy[j]=arr[i];
			
		}
		String newstr = new String(cpy);
		return newstr;
	}

	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the String");
		String str = br.readLine();

		System.out.println("Enter the starting index of Substring");
		int index = Integer.parseInt(br.readLine());

		String ret = MySubStringFun(str.toCharArray(),index);
		System.out.println(ret);
	}
}

