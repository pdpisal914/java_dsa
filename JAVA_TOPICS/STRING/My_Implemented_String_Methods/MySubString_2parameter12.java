import java.io.*;
class Substring{
	static String MySubStringBetween(char [] arr,int index1,int index2){
		int small=0,large=0;
		if(index1<=index2){
			 small=index1;
			 large=index2;
		}else{
			 small=index2;
			 large=index1;
		}
		char []arr1=new char[(large - small)];
		for(int i=small,j=0;i<large;i++,j++){
			arr1[j]=arr[i];
		}
		String arr3 = new String(arr1);
		return arr3;
	}
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the String");
		String str = br.readLine();

		System.out.println("Enter the starting index");
		int index1=Integer.parseInt(br.readLine());

		System.out.println("Enter the ending index");
		int index2 = Integer.parseInt(br.readLine());

		String ret = MySubStringBetween(str.toCharArray(),index1,index2);
		System.out.println(ret);
	}
}

