import java.io.*;
class CompareStringDemo{
	static int MyCompare(char arr1[],char arr2[]){
		int len=0;
		if(arr1.length>=arr2.length){
			len=arr2.length;
		}else{
			len=arr1.length;
		}

		for(int i=0;i<len;i++){
			if(arr1[i] != arr2[i]){
				return (arr1[i]-arr2[i]);
			}
		}
		
		if(arr1.length == arr2.length){
			return 0;
		}else{
			return (arr1.length-arr2.length);
		}
	}
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the String first");
		String str1 = br.readLine();
		System.out.println("Enter the Second String");
		String str2 = br.readLine();

		int ret = MyCompare(str1.toCharArray(),str2.toCharArray());
		if(ret == 0){
			System.out.println("Strings are Same");
		}else{
			System.out.println(ret);
		}
	}

}
