import java.io.*;
class ReplaceChar{
	static String MyReplaceFun(char [] arr,char oldch,char newch){

		for(int i=0;i<arr.length;i++){
			if(arr[i] == oldch){
				arr[i] = newch;
			}
		}
		String arr1 = new String(arr);
		return arr1;
	}
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the String");
		String str = br.readLine();

		System.out.println("Enter the old character");
		char oldch=(char)br.read();
		br.skip(1);

		System.out.println("Enter the new character");
		char newch=(char)br.read();
		br.skip(1);

		String ret = MyReplaceFun(str.toCharArray(),oldch,newch);
		System.out.println(ret);
	}
}



