import java.io.*;
class MyEqual{
	static boolean MyEquals(char [] arr1,char [] arr2){
		if(arr1.length != arr2.length){
			return false;
		}
		for(int i=0;i<arr1.length;i++){
			if(arr1[i] != arr2[i]){
				return false;
			}
		}
		return true;
	}


	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the First String");
		String str1 = br.readLine();
	
		System.out.println("Enter the Second String");
		String str2 = br.readLine();
	
		System.out.println(MyEquals(str1.toCharArray(),str2.toCharArray()));
}
}

