import java.io.*;
class MyEqualIgnore{
	static boolean MyEqualIgnoreCase(char [] arr1,char [] arr2){
		if(arr1.length != arr2.length){
			return false;
		}
		for(int i=0;i<arr1.length;i++){
			if(arr1[i] != arr2[i] && arr1[i]!=(arr2[i]+32) && arr1[i] != (arr2[i]-32)){
				return false;
			}
		}
		return true;
	}

	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the First String");
		String str1 = br.readLine();

		System.out.println("Enter the second string");
		String str2 =br.readLine();

		System.out.println(MyEqualIgnoreCase(str1.toCharArray(),str2.toCharArray()));
	}
}
	
