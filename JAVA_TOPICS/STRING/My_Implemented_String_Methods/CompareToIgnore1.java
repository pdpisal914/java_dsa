import java.io.*;
class CompareToIgnoreCaseDemo{
	static int MyCompareToIgnoreCase(char []arr1,char [] arr2){
		int len=0;
		if(arr1.length >= arr2.length){
			len = arr2.length;
		}else{
			len = arr1.length;
		}
		for(int i=0;i<len;i++){
			if(arr1[i] != arr2[i] && arr1[i] != (arr2[i]+32) && arr1[i] != (arr2[i]-32) ){
				return (arr1[i] - arr2[i]);
			}
		}
		return arr1.length - arr2.length;
	}

	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
		System.out.println("Enter the first String");
		String str1 = br.readLine();
		System.out.println("Enter the second String");
		String str2 = br.readLine();

		int ans=MyCompareToIgnoreCase(str1.toCharArray(),str2.toCharArray());

		if(ans == 0){
			System.out.println("Strings are Same(With ignore cases)");
		}else{
			System.out.println(ans);
		}
	}
}
