import java.io.*;
class SplitDemo{
	static String[] MySplitFun(char []arr1,char [] arr2){
		int cnt=0;
		int []index=new int[arr1.length];
		int p=0;

		for(int i=0;i<arr1.length-arr2.length+1;i++){
			int j=0,x=0;

			if(arr1[i] == arr2[j]){
				int flag =0;
				for(x=i;j<arr2.length;x++){
					if(arr1[x] == arr2[j]){
						j++;
						flag++;
					}else{
						break;
					}
				}
				if(flag == arr2.length){
					cnt++;
					index[p]=i;
					i=x;
					p++;
				}
			}
		}
		index[p]=arr1.length;

		String starr[] = new String[cnt+1];

		if(cnt != 0){
				for(int i=0;i<=cnt;i++){
					int begin =0;
					int stop =0;
					char carr[];
	
					if(i==0){
						carr = new char[index[i]];
						stop =index[i];
					}else{
						carr = new char[index[i]-(index[i-1]+arr2.length)];
						begin = index[i-1] + arr2.length;
						stop = index[i];
					}
	
					for(int j=begin,x=0;j<stop;j++){
						carr[x]=arr1[j];
						x++;
					}
		
					String st = new String(carr);
					starr[i]=st;
				}
		  }else{
				String s1 = new String(arr1);
				starr[0]=s1;
		  }
		return starr;
	}

	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				System.out.println("Enter the String");
				String str = br.readLine();

				System.out.println("Enter the delimeter");
				String delimeter = br.readLine();

				String [] arraystr = MySplitFun(str.toCharArray(),delimeter.toCharArray());

				for(int i=0;i<arraystr.length;i++){
					System.out.println(arraystr[i]);
				}
	}

}
