import java.io.*;
class FileDemo{
	public static void main(String [] pfdp) throws IOException{
		File fobj = new File("FileHandling");
		fobj.mkdir();

		File fobj1 = new File(fobj,"Code1.txt");
		fobj1.createNewFile();

		File fobj2 = new File("FileHandling","Code2.txt");
		fobj2.createNewFile();
	}
}
