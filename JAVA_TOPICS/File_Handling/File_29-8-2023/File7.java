import java.io.*;
class FileDemo{
	public static void main(String [] pdp) throws IOException{
		File fobj = new File("/home/pdpisal914/java_dsa/JAVA_TOPICS/Classes_Objects");
		System.out.println(fobj.exists());

		String[] files = fobj.list();

		int countDir=0;
		int countFile=0;

		for(String str : files){

			File f = new File(str);

			if(f.isFile()){
				countFile++;
			}else{
				countDir++;
			}

			System.out.println(str);
		}
		
		System.out.println("Files Count = "+countFile);
		System.out.println("Folder Count = "+countDir);
	}
}
