import java.io.*;
class FileDemo{
	public static void main(String [] pdp) throws IOException{
		File f = new File("FileHandling");
		f.mkdir();
		File fobj =  new File("FileHandling","Incubator1.txt");
		fobj.createNewFile();

		System.out.println(fobj.getName());
		System.out.println(fobj.getParent());
		System.out.println(fobj.getPath());
		System.out.println(fobj.getAbsolutePath());
		System.out.println(fobj.canRead());
		System.out.println(fobj.canWrite());
		System.out.println(fobj.isDirectory());
		System.out.println(fobj.isFile());
	}
}
