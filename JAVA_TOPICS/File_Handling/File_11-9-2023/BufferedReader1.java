import java.io.*;
class FileDemo1{
	public static void main(String [] pdp) throws IOException{
		FileWriter fos = new FileWriter("Inc.txt");
		fos.write("Pranav\n");
		fos.write("Rohit\n");
		fos.write("Dnyaneshwar\n");
		fos.write("Adesh\n");
		fos.close();
		
		FileInputStream fis = new FileInputStream("Inc.txt");
		BufferedReader br = new BufferedReader(new InputStreamReader(fis));
			
		String str = br.readLine();

		while(str != null){
			System.out.println(str);
			str = br.readLine();
		}
		br.close();
		fis.close();
	}
}
