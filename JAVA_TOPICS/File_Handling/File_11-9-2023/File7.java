import java.io.*;
class Player implements Serializable{
	int jerNo = 0;
	String pName;

	Player(int jerNo,String pName){
		this.jerNo = jerNo;
		this.pName = pName;
	}
}
class SerializableDemo{
	public static void main(String [] pdp) throws IOException,ClassNotFoundException{
		Player obj1 = new Player(1,"KLRahul");
		Player obj2 = new Player(7,"MSDhoni");
		Player obj3 = new Player(11,"Virat Kohli");

		FileOutputStream fos = new FileOutputStream("PlayerData.txt");
		ObjectOutputStream oos = new ObjectOutputStream(fos);

		oos.writeObject(obj1);
		oos.writeObject(obj2);
		oos.writeObject(obj3);

		oos.close();
		fos.close();
		
		DiserializableDemo ro = new DiserializableDemo();
		ro.ReadObjects();
	}

}
class DiserializableDemo{
	void ReadObjects() throws IOException,ClassNotFoundException{

		FileInputStream fis = new FileInputStream("PlayerData.txt");
		ObjectInputStream ois = new ObjectInputStream(fis);

		Player obj1 = (Player)ois.readObject();
		Player obj2 = (Player)ois.readObject();

		ois.close();
		fis.close();

		System.out.println("JerNo : "+obj1.jerNo);
		System.out.println("Name : "+obj1.pName);
		
		System.out.println("JerNo : "+obj2.jerNo);
		System.out.println("Name : "+obj2.pName);
		


	}
}
