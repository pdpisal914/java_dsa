import java.io.*;
class FileDemo{
	public static void main(String [] pdp) throws IOException{
		FileWriter fw = new FileWriter("Incubator.txt",true);
		//FileWriter(FileName,Flag);
		//Flag is use for Appending the data at which position when we open file again for writing new data
		//True = Write/Append the new Data at the end of old data
		//False = Override the Old data bY New data 

		fw.write("Pranav ");
		fw.write("Dattatray ");
		fw.write("Pisal");

		fw.close();
	}
}
