import java.io.*;
class Player implements Serializable{
	int jerNo = 0;
	String pName;

	Player(int jerNo,String pName){
		this.jerNo = jerNo;
		this.pName = pName;
	}
}
class SerializableDemo{
	public static void main(String [] pdp) throws IOException{
		Player obj1 = new Player(1,"KLRahul");
		Player obj2 = new Player(7,"MSDhoni");
		Player obj3 = new Player(11,"Virat Kohli");

		FileOutputStream fos = new FileOutputStream("PlayerData.txt");
		ObjectOutputStream oos = new ObjectOutputStream(fos);

		oos.writeObject(obj1);
		oos.writeObject(obj2);
		oos.writeObject(obj3);

		oos.close();
		fos.close();
	}
}
