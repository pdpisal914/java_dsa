class Demo{
	void fun(String pdp){
		System.out.println(pdp);
	}

	public static void main(String [] pdp){
		Demo obj=new Demo();

		obj.fun("Pranav Pisal");
		obj.fun("16");
		obj.fun("100.76f");
		obj.fun("true");
		obj.fun("A");
		
		//obj.fun(16);//error=Double quotes missing for string
		//obj.fun(100.76f);//error=double quotes are required for considering String
		//obj.fun(true);//error=Missing 2bl quote fro considering it is string
		//obj.fun('A');//Error=give 2bl quote fro converting into String
	}
}


