class method{
	int x=10;
	static int y=20;

	void fun(){
		System.out.println("In Fun function");
	}
	static void gun(){
		System.out.println("In Gun function");
	}
	public static void main(String [] pdp){
		System.out.println("x="+x);//Error=non-static variable x cannot referrenced from static context
		System.out.println("y="+y);

		fun();//Error=Non-static method fun cannot referrenced from static context
		gun();
	}

}
