import java.io.*;
class Code{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the number of rows");
		int row=Integer.parseInt(br.readLine());

		System.out.println("Enter the number of columns");
		int col=Integer.parseInt(br.readLine());

		int arr[][]=new int[row][col];

		System.out.println("Enter the 2d array elements");
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){

				arr[i][j]=Integer.parseInt(br.readLine());
			}
		}

		System.out.println("Elements of 2D array");

		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				System.out.print(arr[i][j]+"\t");
			}
			System.out.println();
		}
	}
}

