import java.io.*;
class Code{
	public static void main(String [] pdp)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the total number of rows and Columns");

		int arr[][]=new int[Integer.parseInt(br.readLine())][Integer.parseInt(br.readLine())];

		System.out.println("Enter the elements of 2D array");
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				arr[i][j]=Integer.parseInt(br.readLine());
			}
		}

		System.out.println("Values of arr = "+arr);
		for(int i=0;i<arr.length;i++){
			System.out.println("arr["+i+"] ="+arr[i]);
		}


		System.out.println("HASHCODE of arr= "+System.identityHashCode(arr));
		for(int i=0;i<arr.length;i++){
			System.out.println("Address of "+arr[i]+" = "+System.identityHashCode(arr[i]));
		}
	}
}

		
