import java.io.*;
//import java.io.InputStreamReader;
class Code{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		int arr[][]=new int[3][3];

		System.out.println("Enter the array elements");

		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				arr[i][j]=Integer.parseInt(br.readLine());
			}
		}

		System.out.println("2D Array is");

		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				System.out.print(arr[i][j]+"\t");
			}
			System.out.println();
		}
	}
}

