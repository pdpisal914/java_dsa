class Code{
	public static void main(String [] pdp){
		int x=10;
		Integer y=10;
		Integer z=new Integer(10);

		System.out.println("x="+System.identityHashCode(x));
		System.out.println("same but different type of declaration y="+System.identityHashCode(y));
		System.out.println("forcly tell to make new object of 10 z="+System.identityHashCode(z));

	}
}
