class Jagged{
	public static void main(String [] pdp){
		int arr[][]=new int[4][];

		arr[0]=new int[]{10,20,30,40};
		arr[1]=new int[]{50,60,70};
		arr[2]=new int[]{80,90};
		arr[3]=new int[]{100};

		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				System.out.print(arr[i][j]+"\t");
			}
			System.out.println();
		}
	}
}
