import java.io.*;
class Jagged3D{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Total number of planes and number of rows in each plane");
		int arr[][][]=new int[Integer.parseInt(br.readLine())][Integer.parseInt(br.readLine())][];

		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				System.out.println("Enter the number of elements for "+(j+1)+" row");
				arr[i][j]=new int[Integer.parseInt(br.readLine())];
				System.out.println("Enter the elements ");
				for(int k=0;k<arr[i][j].length;k++){
					arr[i][j][k]=Integer.parseInt(br.readLine());
				}
			}
		}

		System.out.println("All Elements");
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				for(int k=0;k<arr[i][j].length;k++){
					System.out.print(arr[i][j][k]+"  ");
				}
				System.out.println();
			}
			System.out.println();
		}
					

	}
}

