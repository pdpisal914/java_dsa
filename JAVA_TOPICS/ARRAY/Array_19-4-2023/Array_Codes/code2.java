//Statement=Take i/p for array from user and print the count of even numbers

import java.io.*;
class Code{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the size of array");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter the elements");
		int cnt=0;
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
			if(arr[i]%2==0){
				cnt++;
			}
		}

		System.out.println("Even Count ="+cnt);
	}
}

