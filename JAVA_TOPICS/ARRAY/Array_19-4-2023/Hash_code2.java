class Code{
	public static void main(String [] pdp){
		int arr[]=new int[]{10,20,30};
		int arr1[]=new int[]{60,30,80};
		int arr2[]=new int[]{10,20,30};

		int x=10;
		int y=20;
		int z=30;

		System.out.println("arr="+arr);
		System.out.println("arr1="+arr1);
		System.out.println("arr2="+arr2);

		System.out.println();
		System.out.println("Hasch code of arr="+System.identityHashCode(arr));
		System.out.println("Hasch code of arr1="+System.identityHashCode(arr1));
		System.out.println("Hasch code of arr2="+System.identityHashCode(arr2));
		
		System.out.println();
		System.out.println("Hasch code of arr[0]="+System.identityHashCode(arr[0]));
		System.out.println("Hasch code of arr1[0]="+System.identityHashCode(arr1[0]));
		System.out.println("Hasch code of arr2[0]="+System.identityHashCode(arr2[0]));

		System.out.println();
		
		System.out.println("Hasch code of arr[1]="+System.identityHashCode(arr[1]));
		System.out.println("Hasch code of arr1[1]="+System.identityHashCode(arr1[1]));
		System.out.println("Hasch code of arr2[1]="+System.identityHashCode(arr2[1]));
		
		System.out.println();
		System.out.println("Hasch code of arr[2]="+System.identityHashCode(arr[2]));
		System.out.println("Hasch code of arr1[2]="+System.identityHashCode(arr1[2]));
		System.out.println("Hasch code of arr2[2]="+System.identityHashCode(arr2[2]));
		System.out.println("\n X,Y,Z");
		System.out.println("Hasch code of x="+System.identityHashCode(x));
		System.out.println("Hasch code of y="+System.identityHashCode(y));
		System.out.println("Hasch code of z="+System.identityHashCode(z));
	}
}

