class Code{
	static void fun(int arr[]){

		System.out.println("At the start of fun");
		for(int i=0;i<arr.length;i++){
			System.out.println("Address of "+arr[i]+"="+System.identityHashCode(arr[i]));
					arr[i]=arr[i]+50;
		}

		System.out.println("At the end of fun");
		for(int i=0;i<arr.length;i++){
			System.out.println("Address of "+arr[i]+"="+System.identityHashCode(arr[i]));
		}
		
	}
	public static void main(String [] pdp){
		int arr1[]={50,100,150};

		System.out.println("In start of main");
		for(int i=0;i<arr1.length;i++){
			System.out.println("Address of"+arr1[i]+"="+System.identityHashCode(arr1[i]));
		}

		fun(arr1);
		
		System.out.println("At end of main");
		for(int i=0;i<arr1.length;i++){
			System.out.println("Address of"+arr1[i]+"="+System.identityHashCode(arr1[i]));
		}
	}
}
