class Code{
	public static void main(String [] pdp){

		int arr[]={50,100,200,300};

		System.out.println("\nFirst For Loop\n");
		for(int i=0;i<arr.length;i++){
			System.out.println("Address of "+arr[i]+"is ="+System.identityHashCode(arr[i]));
		}
		
		
		System.out.println("\nSecond For Loop\n");
		for(int i=0;i<arr.length;i++){
			System.out.println("Address of "+arr[i]+"is ="+System.identityHashCode(arr[i]));
		}

		for(int i=0;i<arr.length;i++){
			arr[i]=arr[i]+50;
		}

		System.out.println("\n+50 For Loop\n");
		for(int i=0;i<arr.length;i++){
			System.out.println("Address of "+arr[i]+"is ="+System.identityHashCode(arr[i]));
		}

		for(int i=0;i<arr.length;i++){
			arr[i]=arr[i]-50;
		}

		System.out.println("\n-50 For Loop\n");
		for(int i=0;i<arr.length;i++){
			System.out.println("Address of "+arr[i]+"is ="+System.identityHashCode(arr[i]));
		}
		
		System.out.println("\nLast Time pass  For Loop\n");
		for(int i=0;i<arr.length;i++){
			System.out.println("Address of "+arr[i]+"is ="+System.identityHashCode(arr[i]));
		}
	}
}


