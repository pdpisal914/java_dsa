class Outer{
	int x=10;
	class Inner{
		int x=20;
		void fun(){
			System.out.println(x);
		}
	}
	void gun(){
		System.out.println(x);
	}
}
class Client{
	public static void main(String [] pdp){
		Outer obj = new Outer();
		obj.gun();
		Outer.Inner obj1 = obj.new Inner();
		obj1.fun();
	}
}

