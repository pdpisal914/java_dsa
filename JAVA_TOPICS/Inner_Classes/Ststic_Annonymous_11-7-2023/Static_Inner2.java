class Outer{
	int x=10;
	static int y=20;

	void m1(){
		int a=30;
		static int b=40;//ERROR=Illigal start of expression
		final static int c=50;//ERROR=Modified static is not allowed here
	}

	public static void main(String [] pdp){
		int p=60;
		static int q=70;//ERROR=We write static inside the static context
		final static int r=80;//Error=Modified static is not allowed ahere

	}
}


