class Parent{
	Object fun(){
		System.out.println("Object Return Type");
		return new Object();
	}
}
class Child extends Parent{
	String fun(){
		System.out.println("String return Type");
		return "Pranav";
	}
}
class Client{
	public static void main(String [] pdp){
		Child obj = new Child();
		obj.fun();
	}
}
//COVARRIENT RETURN TYPE = Return Types of methods are classes
//In parent class And in child class the methoid we want to Override then the Return Type of that Methods having Parent Child Relation
//Method og Parent class Having Parentclass Return type then Child class having Childclass return Type
