class Parent{
	String fun(){
		System.out.println("Object Return Type");
		return "Pranav";
	}
}
class Child extends Parent{
	StringBuffer fun(){
		System.out.println("String return Type");
		return new StringBuffer("Pranav");
	}
}
class Client{
	public static void main(String [] pdp){
		Child obj = new Child();
		obj.fun();
	}
}
//String return type in parent is incompatible with the StringBuffer return type in nChild
//Fun() from parent is cannot override with the fun() in Child
//StringBuffer is not compatible with String
//String ==StringBuffer==StringBuilder all are siblings they do not have parent child Connection between them
