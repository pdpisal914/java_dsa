class Parent{
	int fun(){
		System.out.println("In Parent fun");
		return 10;
	}
}
class Child extends Parent{
	char fun(){
		System.out.println("In Child fun");
		return 'A';
	}
}
class Client{
	public static void main(String [] pdp){
		Child obj = new Child();
		obj.fun();
	}
}
//ERROR = method fun is cannot be override bcoz in parent fun having int return type and in child fun having char return type
// Return type of Overriding methods having large impact on the Overriding
