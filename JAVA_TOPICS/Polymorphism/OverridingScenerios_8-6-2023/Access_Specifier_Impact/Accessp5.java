class Parent{
	protected void fun(){
		System.out.println("Parent Fun");
	}
}
class Child extends Parent{
	 void fun(){
		System.out.println("Child Fun");
	}
}
class Client{
	public static void main(String [] pdp){
		Child obj = new Child();
		obj.fun();
	}
}
//Error = Attempting to assign the weaker previliges
