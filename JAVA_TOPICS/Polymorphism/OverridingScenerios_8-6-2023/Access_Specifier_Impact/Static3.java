class Parent{
	static void fun(){
		System.out.println("Parent");
	}
}
class Child extends Parent{
	static void fun(){
		System.out.println("Child");
	}
}
class Client {
	public static void main(String [] pdp){
		Child obj1 = new Child();
		obj1.fun();//calling the Child Function
		Parent obj2 = new Parent();
		obj2.fun();//Calling the child function
		Parent obj3 = new Child();
		obj3.fun();//Calling the Parent function
	}
}
//when compiler see the line Parent obj = new Child();
//it means at compile stage the compiler take the reference of parent and they make the binding beteen route Barrier");







