class Parent{
	final void fun(){
		System.out.println("Parent");
	}
}
class Child extends Parent{
	void fun(){
		System.out.println("In Child");
	}
}
class Client{
	public static void main(String [] pdp){
		Child obj = new Child();
		obj.fun();
	}
}
//ERROR = Overriden method is Final
//Once the method declair as Final then this method is not Accessed / Overriding by its Child class
