class Parent{
	void fun(){
		System.out.println("Parent Fun");
	}
}
class Child extends Parent{
	public void fun(){
		System.out.println("Child Fun");
	}
}
class Client{
	public static void main(String [] pdp){
		Child obj = new Child();
		obj.fun();
	}
}

