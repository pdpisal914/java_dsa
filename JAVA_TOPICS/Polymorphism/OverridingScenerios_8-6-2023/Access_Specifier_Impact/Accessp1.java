class Parent{
	public void fun(){
		System.out.println("Parent Fun");
	}
}
class Child extends Parent{
	void fun(){
		System.out.println("Child Fun");
	}
}
class Client{
	public static void main(String [] pdp){
		Child obj = new Child();
		obj.fun();
	}
}
//ERROR = Attempting to assign the weaker previliges
//In Parent class method is public having large scope and in child we want to decrease 
//the scope of Fun method to the Default and by according to concept it is Wrong
