class Parent{
	Number fun(){
		System.out.println("Object Return Type");
		//return new Number(); Number Methoid is Abstract It Cannot be Initiated
		return null;
	}
}
class Child extends Parent{
	Integer fun(){
		System.out.println("String return Type");
		return 10;
	}
}
class Client{
	public static void main(String [] pdp){
		Child obj = new Child();
		obj.fun();
	}
}

