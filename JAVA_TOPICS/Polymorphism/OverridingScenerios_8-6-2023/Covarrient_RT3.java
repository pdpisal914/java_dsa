class Parent{
	Parent fun(){
		System.out.println("Object Return Type");
		return new Parent();
	}
}
class Child extends Parent{
	Child fun(){
		System.out.println("String return Type");
		return new Child();
	}
}
class Client{
	public static void main(String [] pdp){
		Child obj = new Child();
		obj.fun();
	}
}
//NO ERROR
