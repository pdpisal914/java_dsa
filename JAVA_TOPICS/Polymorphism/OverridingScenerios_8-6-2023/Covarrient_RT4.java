class Parent{
	Object fun(){
		System.out.println("Object Return Type");
		return new Child();
	}
}
class Child extends Parent{
	Parent fun(){
		System.out.println("String return Type");
		return new Parent();
	}
}
class Client{
	public static void main(String [] pdp){
		Child obj = new Child();
		obj.fun();
	}
}

