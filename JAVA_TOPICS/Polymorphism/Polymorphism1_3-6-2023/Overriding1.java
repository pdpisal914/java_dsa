class Parent{
	int x=10;
	static int y=20;
	void Sum(){
		System.out.println(" Parent sum = "+x+y);
	}
	void Sub(){
		System.out.println(" Parent Sub = "+(x-y));
	}
	void Div(){
		System.out.println("Parent Div = "+(x/y));
	}
	void Mult(){
		System.out.println("Parent Mult = "+(x*y));
	}
}

class Child extends Parent{
	int x=100;
	static int y=2;
	void Sum(){
		System.out.println(" Child sum = "+x+y);
	}
	void Sub(){
		System.out.println(" Child Sub = "+(x-y));
	}
	void Div(){
		System.out.println(" Child Div = "+(x/y));
	}
	void Mult(){
		System.out.println(" Child Mult = "+(x*y));
	}
}
class Client{
	public static void main(String [] pdp){
		Child obj = new Child();
		obj.Sum();
		obj.Sub();
		obj.Div();
		obj.Mult();
	}
}



