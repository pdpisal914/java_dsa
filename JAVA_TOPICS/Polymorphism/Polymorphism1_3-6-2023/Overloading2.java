//Polymorphism = Constructor OverLoading

class Add{
	int x=10;
	static int y=20;
	Add(){
		this(100,200);
		System.out.println("Add() = "+(x+y));
	}
	Add(int x,int y){
		this(100,200,300);
		System.out.println("Add(int,int) = "+(x+y));
	}
	Add(int x,int y,int z){
		this(10,20,5.5f);
		System.out.println("Add(int,int,int) = "+(x+y+z));
	}
	Add(int x,int y,float z){
		System.out.println("Add(int,int) = "+(x+y+z));
	}
}
class Client{
	public static void main(String [] pdp){
		Add obj = new Add();
	}
}


		
