//Polymorphism = Constructor OverLoading

class Mult{
	int x=10;
	static int y=20;
	void Mult(){
		
		System.out.println("Add() = "+(x+y));
	}
	void Mult(int x,int y){
		System.out.println("Add(int,int) = "+(x+y));
	}
	void Mult(int x,int y,int z){
		System.out.println("Add(int,int,int) = "+(x+y+z));
	}
	void Mult(int x,int y,float z){
		System.out.println("Add(int,int,float) = "+(x+y+z));
	}
}
class Client{
	public static void main(String [] pdp){
		Mult obj = new Mult();
		obj.Mult();
		obj.Mult(100,200);
		obj.Mult(100,200,300);
		obj.Mult(100,200,50.5f);
	}
}


		
