class Demo{
	void fun(Object obj){
		System.out.println("Object Para");
	}
	void fun(String obj){
		System.out.println("String Para");
	}
}
class Client{
	public static void main(String [] pdp){
		Demo obj = new Demo();
		obj.fun("Core2Web");//Find Exact match
		obj.fun(new StringBuffer("Core2Web"));//Parent Object are able to store address of StringBuffer
		obj.fun(null);//Child class(String class) having Higher prioroty than parent class(Object Class)
	}
}

