class Demo{
	void fun(String x){
		System.out.println("String");
	}
	void fun(StringBuffer str){
		System.out.println("StringBuffer");
	}
}
class Client{
	public static void main(String [] pdp){
		Demo obj = new Demo();
		String str = null;
		StringBuffer str1=null;

		obj.fun("Core2Web");//Exact ,match found
		obj.fun(new StringBuffer("Core2Web"));//Exact match is found
		obj.fun(null);//Exact match not found=Ambiguity Error
	}
}

