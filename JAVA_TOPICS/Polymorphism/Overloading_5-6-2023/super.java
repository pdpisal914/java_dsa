class Parent{
	int x=10;
	static int y=20;
	Parent(){
		System.out.println("Parent Constructor");
	}
}
class Child extends Parent{
	int x=10;
	static int y=20;
	Child(){
		System.out.println("Child Constructor");
	}
	void Access(){
		System.out.println(super.x);
		System.out.println(super.y);
		System.out.println(x);
		System.out.println(y);
	}
}
class Client{
	public static void main(String [] pdp){
		Child obj = new Child();
		obj.Access();
	}
}
