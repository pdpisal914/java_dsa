class Parent{
	Parent(){
		System.out.println("Parent Constructor");
	}
	void fun(){
		System.out.println("Parent fun");
	}
}
class Child extends Parent{
	Child(){
		System.out.println("Child Constructor");
	}
	void gun(){
		System.out.println("Child Gun");
	}
}
class Client{
	public static void main(String [] pdp){
		Child obj = new Child();
		obj.fun();
		obj.gun();

		Parent obj2=new Parent();
		obj2.fun();
	//	obj2.gun();
	}
}

