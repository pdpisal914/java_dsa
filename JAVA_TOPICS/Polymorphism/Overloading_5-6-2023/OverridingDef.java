class Parent{
	Parent(){
		System.out.println("In Parent Constructor");
	}

	void Property(){
		System.out.println("Car,Flat,Gold");
	}

	void Marry(){
		System.out.println("Deepika Padukon");
	}
}
class Child extends Parent{
	Child(){
		System.out.println("In Child Constructor");
	}
	void Marry(){
		System.out.println("Rashmika Mandanna");
	}
}
class Client{
	public static void main(String [] pdp){
		Child obj=new Child();
		obj.Property();
		obj.Marry();
	}
}

