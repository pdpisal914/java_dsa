/*PRINT
* * * *
* * *
* *
*

*/

import java.util.*;
class Code{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int row=sc.nextInt();

		for(int i=1;i<=row;i++){
			System.out.print("* ");

			if(i==row){
				System.out.println();
				i=0;
				row--;
			}

			if(row==0){
				break;
			}
		}
	}
}


