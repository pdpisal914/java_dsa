/*PRINT
* * * *
* * *
* *
*
*/

class Code{
	public static void main(String [] pdp){
		int row=4;

		for(int i=1;i<=row;i++){
			for(int j=1;j<=row-i+1;j++){
				System.out.print("* ");
			}
			System.out.println();
		}
	}
}
/*
OR ANOTHER LOGIC

for(int i=1;i<=row;i++){
	for(int j=i;j<=row;j++){
		System.out.print("* ");
	}
	System.out.println();
}
*/
