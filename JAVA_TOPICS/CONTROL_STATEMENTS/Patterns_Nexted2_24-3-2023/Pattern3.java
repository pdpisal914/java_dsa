/*PRINT
1
2 3
4 5 6
7 8 9 10
*/

import java.util.*;
class Code{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int row=sc.nextInt();
		int num=1;
		for(int i=1,x=1;i<=x;i++){
			System.out.print(num++ + " ");
			if(i==x){
				System.out.println();
				i=0;
				x++;
			}
			if(x==row+1){
				break;
			}
		}
	}
}

