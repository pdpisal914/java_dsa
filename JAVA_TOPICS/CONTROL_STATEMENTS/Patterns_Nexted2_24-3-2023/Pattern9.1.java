/*PRINT
1
2 3
16 5 36
49 8 81 10
*/

import java.util.*;
class Code{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int row=sc.nextInt();
		int x=1;

		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				if(j%2!=0){
					System.out.print(x*x + " ");
				}else{
					System.out.print(x + " ");
				}
				x++;

			}
			System.out.println();
		}
	}
}

