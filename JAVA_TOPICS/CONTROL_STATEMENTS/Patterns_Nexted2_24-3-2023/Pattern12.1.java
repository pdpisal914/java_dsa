/*PRINT
1
2 c
5 e 6
7 h 9 j
*/

import java.util.*;
class Code{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int row=sc.nextInt();
		int n=1;
		char ch=97;

		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				if(j%2!=0){
					System.out.print(n + " ");
				}else{
					System.out.print(ch + " ");
				}
				n++;
				ch++;
			}
			System.out.println();
		}
	}
}

