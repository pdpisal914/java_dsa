/*PRINT
A B C D
A B C
A B
A */

import java.util.*;
class Code{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		   System.out.println("Enter number of rows");
		   int row=sc.nextInt();

		   for(int i=1;i<=row;i++){
			   char ch=65;
			   for(int j=i;j<=row;j++){
				   System.out.print(ch++ + " ");
			   }
			   System.out.println();
		   }
	}
}
