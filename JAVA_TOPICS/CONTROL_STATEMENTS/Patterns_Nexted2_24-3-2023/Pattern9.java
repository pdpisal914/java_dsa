/*PRINT
1
4 3
16 5 36
49 8 81 10
*/

import java.util.*;
class Code{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int row=sc.nextInt();
		int num=1;
		for(int i=1,x=1;i<=x;i++){
			if(i%2!=0){
				System.out.print(num*num + " ");
			}else{
				System.out.print(num + " ");
			}
			num++;

			if(i==x){
				System.out.println();
				i=0;
				x++;
			}

			if(x==row+1){
				break;
			}
		}
	}
}

