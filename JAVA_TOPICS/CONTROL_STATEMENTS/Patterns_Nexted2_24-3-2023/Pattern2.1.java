/*PRINT
*
* *
* * *
* * * *
*/

import java.util.*;
class Code{
	public static void main(String [] dp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int row=sc.nextInt();

		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				System.out.print("* ");
			}
			System.out.println();
		}
	}
}
