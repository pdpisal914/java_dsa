/*PRINT
1
4 9
16 25 36
49  64 81 100*/

import java.util.*;
class Code{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int row=sc.nextInt();
		int x=1;

		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				System.out.print(x*x + " ");
				x++;
			}
			System.out.println();
		}
	}
}
