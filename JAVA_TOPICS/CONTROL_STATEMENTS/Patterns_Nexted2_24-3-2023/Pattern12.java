/*PRINT
1
2 c 
4 e 6
7 h 9 j
*/

import java.util.*;

class Code{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int row=sc.nextInt();
		char ch=97;
		int n=1;

		for(int i=1,x=1;i<=x;i++){
			if(i%2!=0){
				System.out.print(n + " ");
			}else{
				System.out.print(ch + " ");
			}
			n++;
			ch++;

			if(i==x){
				System.out.println();
				i=0;
				x++;
			}
			if(x==row){
				break;
			}
		}
	}
}
