/*PRINT
* _ _ *
* _ _ *
* _ _ *
* _ _ *
*/

import java.util.*;
class Code{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int row=sc.nextInt();
		int cnt=0;

		for(int i=1;i<=row;i++){

			if(i==1 || i==row){
				System.out.print("* ");
			}else{
				System.out.print("_ ");
			}

			if(i==row){
				System.out.println();
				i=0;
				cnt++;
			}
			if(cnt==row){
				break;
			}
		}
	}
}





