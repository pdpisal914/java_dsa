/*PRINT
10
9 8
7 6 5
4 3 2 1
*/

import java.util.*;
class Code{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int row=sc.nextInt();

		int num=(row*(row+1))/2;

		for(int i=1,x=1;i<=x;i++){
			System.out.print(num-- + " ");
			if(i==x){
				System.out.println();
				x++;
				i=0;
			}
			if(x==row+1){
				break;
			}
		}
	}
}

