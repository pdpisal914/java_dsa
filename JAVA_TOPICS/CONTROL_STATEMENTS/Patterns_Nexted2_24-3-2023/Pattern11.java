/*PRINT
A 1 B 2
C 3 D
E 4
F*/

import java.util.*;
class Code{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int row=sc.nextInt();
		char ch=65;
		

		for(int i=1,x=1;i<=row;i++){
			if(i%2!=0){
				System.out.print(ch++ + " ");
			}else{
				System.out.print(x++ + " ");
			}

			if(i==row){
				System.out.println();
				i=0;
				row--;
			}
			if(row==0){
				break;
			}
		}
	}
}

