/*PRINT
10
9 8
7 6 5
4 3 2 1
*/
import java.util.*;
class Code{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int row=sc.nextInt();
		int num=(row*(row+1))/2;

		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				System.out.print(num-- + " ");
			}
			System.out.println();
		}
	}
}



