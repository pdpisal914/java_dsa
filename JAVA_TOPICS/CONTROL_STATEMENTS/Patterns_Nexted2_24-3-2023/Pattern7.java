/*PRINT
A B C D
A B C
A B
A
*/
import java.util.*;
class Code{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int row=sc.nextInt();
		char ch=65;

		for(int i=1;i<=row;i++){
			System.out.print(ch++ + " ");

			if(i==row){
				System.out.println();
				i=0;
				row--;
				ch=65;
			}
			if(row==0){
				break;
			}
		}
	}
}

