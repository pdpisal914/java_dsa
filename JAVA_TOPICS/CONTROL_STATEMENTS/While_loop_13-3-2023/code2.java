//Statement=Given an integer N
//Print all digits og given N
//i/p=6531
//o/p=1  3   5   6


class Digits{
	public static void main(String [] pdp){
		int N=12345678;

		while(N!=0){
			System.out.println(N%10);
			N=N/10;
		}
	}
}


