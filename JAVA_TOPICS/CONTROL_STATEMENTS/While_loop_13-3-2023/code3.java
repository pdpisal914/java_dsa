//Statement=Given an integer n
//print the sum of the digits og number n
//i/p=6531
//0/p=15
class Sum{
	public static void main(String [] pdp){
		int n=6531;
		int sum=0;
		while(n!=0){
			sum=sum+n%10;
			n=n/10;
		}
		System.out.println(sum);
	}
}

