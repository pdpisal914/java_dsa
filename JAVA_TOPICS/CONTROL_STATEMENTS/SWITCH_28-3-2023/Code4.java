//basics
class SwitchDemo{
	public static void main(String [] pdp){

		char ch=65;

		switch(ch){
			case 'A':
				System.out.println("Char-A");
				break;
			case 65://Duplicate case error
				//Duplicate case error bcoz A internally goes as the ascii value and ascii value 65 is converted into //binary then the binary of A and the binary or direc6t 65 is same hence it gives an error
				System.out.println("num-65");
				break;
			case 'B':
				System.out.println("Char -'B'");
				break;
			case 66://Duplicate case error
				System.out.println("num-66");
				break;
			default:
				System.out.println("Invalid");
				break;
		}
		System.out.println("EXIT");
	}
}


