//Basics
class SwitchDemo{
	public static void main(String [] pdp){
		int x=3;
		int a=1;
		int b=2;

		switch(x){
			case a://constant expression required ERROR...variavles having changable valkues and hence it does not supprt to variables
				System.out.println("1");
				break;
			case b:
				System.out.println("2");
				break;
			case a+b:
				System.out.println("3");
				break;
			case a+a+b:
				System.out.println("4");
				break;
			case a+b+b:
				System.out.println("5");
				break;
			default:
				System.out.println("NOT MATCHED");
				break;
		}
		System.out.println("EXITED");
	}
}

