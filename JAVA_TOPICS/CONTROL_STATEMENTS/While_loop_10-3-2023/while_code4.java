//Statement=Print integer from N to 1 using while loop


import java.util.*;

class While{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the value of N");
		int N=sc.nextInt();

		while(N>=1){
			if(N%2!=0){
				System.out.println(N);
			}
			N--;
		}
	}
}

