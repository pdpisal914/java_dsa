//Statement=Take an integer N as an input 
//Print the odd integers from 1 to n using loop

import java.util.*;
class While{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the value of N");
		int N=sc.nextInt();
		int i=1;		
		
		
		while(i<=N){
			if(i%2==1){
				System.out.println(i);
			}
			i++;
		}

	}
}



