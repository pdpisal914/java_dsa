//Statement=Given integer value as an input
//		Print "fizz" if it divisible by 3
//		Print "buzz" if it divisible by 5
//		Print "fizz-buzz" if it is divisible by both
//		if not then print "Not divisible by both"

import java.util.*;

class Check{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the value for checking divisible by 3 or 5");

		int x=sc.nextInt();

		if(x%3==0 && x%5==0){
			System.out.println("FIZZ");
		}else if(x%3==0){
			System.out.println("BUZZ");
		}else if(x%5==0){
			System.out.println("FIZZ-BUZZ");
		}else{
			System.out.println("NOT DIVISIBLE BY BOTH");
		}
	}
}

