//Statement= ELECTRICITY BILL PROBLEM
//	given an integer a which is represent units of electricity consumed at your house
//	calculate & print bill amount 
//	units<=100	:	price per unit is 1
//	units>100	:	price per unit is 2


import java.util.*;

class Electricity_Bill{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of unites of electricity meter");

		int unit=sc.nextInt();

		if(unit<=100){
			System.out.println(unit);
		}else{
			System.out.println(2*unit-100);
		}
	}
}

