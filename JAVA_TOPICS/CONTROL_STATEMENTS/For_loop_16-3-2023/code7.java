//Statement=WAP to print the square of n terms and their sum

import java.util.*;
class Pdp{
	public static void main(String [] Pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the n");
		int n=sc.nextInt();
		
		int sum=0;
		for(int i=1;i<=n;i++){
			System.out.print(i*i +"  ");
			sum=sum+(i*i);
		}

		System.out.println("\nSUM = "+sum);
	}
}
