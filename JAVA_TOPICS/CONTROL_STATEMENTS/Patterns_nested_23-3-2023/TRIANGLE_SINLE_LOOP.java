/*PRINT
    *
  * * *
* * * * *

*/

import java.util.*;
class PAttern{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int row=sc.nextInt();

		//System.out.println("Enter the number of columns");
		//int col=sc.nextInt();
		
		int end=row;
		int itr=row;

		for(int i=1;i<=itr;i++){

			if(i<end){
				System.out.print("  ");
			}else{
				System.out.print("* ");
			}

			if(i==itr){
				System.out.println();
				itr++;
				i=0;
				end--;
			}
			if(end==0){
				break;
			}
		}
	}
}


