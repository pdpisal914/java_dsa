/*
PRINT
A B C
A B C
A B C
*/

import java.util.*;
class Code{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int row=sc.nextInt();

		System.out.println("Enter the number of columns");
		int col=sc.nextInt();
		char ch='A';
		int cnt=0;

		for(int i=1;i<=col;i++){
			System.out.print(ch++ +" ");
			if(i==col){
				System.out.print("\n");
				ch='A';
				i=0;
				cnt++;
			}
			if(cnt==row){
				break;
			}
		}
	}
}
