/*PRINT
A 1 A 1
A 1 A 1
A 1 A 1
A 1 A 1
*/

import java.util.*;
class Code{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int row=sc.nextInt();
		System.out.println("Enter the number of columns");
		int col=sc.nextInt();
		int cnt=0;

		for(int i=1;i<=col;i++){
			if(i%2!=0){
				System.out.print("A ");
			}else{
				System.out.print("1 ");
			}

			if(i==col){
				System.out.println();
				i=0;
				cnt++;
				if(cnt==row){
					break;
				}
			}
		}
	}
}


