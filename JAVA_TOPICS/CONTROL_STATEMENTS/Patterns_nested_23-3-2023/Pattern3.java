/*PRINT
A 1 B 2
A 1 B 2
A 1 B 2
*/

import java.util.*;
class Pattern{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int row=sc.nextInt();

		System.out.println("Enter the number of columns");
		int col=sc.nextInt();
		char ch=65;
		int num=1;
		for(int i=1;i<=row*col;i++){
			if(i%2==0){
				System.out.print(num++ +" ");
			}else{
				System.out.print(ch++ +" ");
			}

			if(i%col==0){
				System.out.println();
				ch=65;
				num=1;
			}
		}
	}
}
