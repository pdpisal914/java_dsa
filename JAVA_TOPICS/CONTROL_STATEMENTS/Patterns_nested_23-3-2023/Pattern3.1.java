/*PRINT
A 1 B 2
A 1 B 2
A 1 B 2
*/

import java.util.*;
class Code{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int row=sc.nextInt();

		System.out.println("Enter the number of columns");
		int col=sc.nextInt();
		char ch=65;
		int cnt=0;
		int x=1;

		for(int i=1;i<=col;i++){

			if(i%2!=0){
				System.out.print(ch++ +" ");
			}else{
				System.out.print(x++ +" ");
			}

			if(i==col){
				System.out.println();
				ch=65;
				cnt++;
				x=1;
				i=0;
				if(cnt==row){
					break;
				}
			}
		}
	}
}


