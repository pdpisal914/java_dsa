/*
PRINT
A B C
A B C
A B C
*/

import java.util.*;
class Code{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int row=sc.nextInt();

		System.out.println("Enter the number of columns");
		int col=sc.nextInt();
		char ch=65;

		for(int i=1;i<=row*col;i++){
			System.out.print(ch++ +" ");
			if(i%col==0){
				System.out.print("\n");
				ch=65;
			}
		}
	}
}
