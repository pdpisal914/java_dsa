/*Print
1 2 3
4 5 6
7 8 9*/

import java.util.*;
class Pattern{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int row=sc.nextInt();


		for(int i=1;i<=row*row;i++){
			System.out.print(i+" ");
			if(i%row==0){
				System.out.println();
			}
		}
	}
}

