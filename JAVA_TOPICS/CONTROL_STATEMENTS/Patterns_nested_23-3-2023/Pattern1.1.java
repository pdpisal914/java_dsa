//by using 2 for loops

import java.util.*;
class Code{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int row=sc.nextInt();

		System.out.println("Enter the number of columns");
		int col=sc.nextInt();
		int num=1;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=col;j++){
				System.out.print(num++ +" ");
			}
			System.out.println();
		}
	}
}

