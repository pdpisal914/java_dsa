/*PRINT
1 2 3 4
5 6 7 8 
9 10 11 12*/

import java.util.*;
class Pattern{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int row=sc.nextInt();

		System.out.println("Enter the number of columns");
		int col=sc.nextInt();
		int num=1;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=col;j++){
				System.out.print((num++)+" ");
			}
			System.out.println();
		}
	}
}


