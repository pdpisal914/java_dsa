/*
PRINT
1 1 1
2 2 2
3 3 3*/

import java.util.*;
class Pattern{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int row=sc.nextInt();

		for(int i=1;i<=row;i++){
			for(int j=1;j<=row;j++){
				System.out.print(i+" ");
			}
			System.out.println();
		}
	}
}
