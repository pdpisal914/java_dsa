/*PRINT
9 8 7
6 5 4
3 2 1
*/

import java.util.*;
class Pattern{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int row=sc.nextInt();

		System.out.println("Enter the number of columns");
		int col=sc.nextInt();
		
		int num=row*col;		
		for(int i=1;i<=row;i++){
			for(int j=1;j<=col;j++){
				System.out.print(num-- + " ");
			}
			System.out.print("\n");
		}
	}
}
