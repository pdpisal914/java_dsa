/*
PRINT
* * * 
* * *
* * *
*/
import java.util.*;
class Star{
	public static void main(String [] pdp){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of rows");

		int row=sc.nextInt();


		for(int i=1;i<=row;i++){
			for(int j=1;j<=row;j++){
				System.out.print("* ");
			}
			System.out.println();
		}
	}
}
