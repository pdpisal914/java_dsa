//Statement=WAP to print the given number if prime number or not by using break-continue keyword

class BRK{
	public static void main(String [] pdp){
		int n=25;
		int count=0;

		for(int i=1;i<=n;i++){

			if(n%i==0){
				count++;
			}
			if(count>2){
				break;
			}
		}

		if(count==2){
			System.out.println("The number is prime number");
		}else{
			System.out.println("The number is not prime number");
		}
	}
}

