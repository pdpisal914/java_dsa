class Demo{
	int x=10;
	
	Demo(){
		this(50);
		System.out.println("In No-Args");
		System.out.println(this.x);
	}
	Demo(int x){
		System.out.println("In Parameterized");
		System.out.println(this.x);
	}
	public static void main(String [] pdp){
		Demo obj = new Demo();
	}
}
