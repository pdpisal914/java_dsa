class Demo{
	int x=10;
	Demo(){
		this(50);
		System.out.println("In No-Args");
	}
	Demo(int x){
		this();
		System.out.println("In Parameterized");
	}

	public static void main(String [] pdp){
		Demo obj = new Demo();
	}
}

