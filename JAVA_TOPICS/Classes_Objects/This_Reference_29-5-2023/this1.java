class Demo{
	int x=10;

	Demo(){//Internally = Demo(Demo this);
		System.out.println("In No-Args");
		System.out.println(this.x);
	}

	Demo(int x){//Demo(Demo this,int x);
		System.out.println("In Parameterized");
		System.out.println(this.x);//Internally = obj2.x
	}

	public static void main(String [] pdp){
		Demo obj1 = new Demo();//Internally=Demo(obj1);
		Demo obj2 = new Demo(50);//Internally = Demo(obj2,50);
	}
}


