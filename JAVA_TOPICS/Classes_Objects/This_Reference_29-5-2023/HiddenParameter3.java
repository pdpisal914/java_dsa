class Demo{
	int x=10;
	Demo(){
		System.out.println("In No-Args");
	}
	Demo(int x){
		System.out.println("In Parameterized");
	}
	public static void main(String [] pdp){
		Demo obj1 = new Demo();
		Demo onj2 = new Demo(50);
	}
}

