class Demo{
	private int x=10;
	Demo(){
		System.out.println(this.x);
		System.out.println("In No-Args");
	}

	Demo(int x){
		System.out.println(this.x);
		System.out.println("In Parameterized");
	}
}
class Client{

	public static void main(String [] pdp){
		Demo obj = new Demo();
	}
}

