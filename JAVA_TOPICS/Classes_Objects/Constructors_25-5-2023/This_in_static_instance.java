class Demo{
	Demo(){//Internally = Demo(Demo obj);
		System.out.println("In Constructor");
		System.out.println(this);
	}
	void fun(){//Internally Declaration = void fun(Demo this);

		System.out.println("In Fun");
		System.out.println(this);
	}
	static void gun(){//Static goshtin mdhe this parameter add/pathvla jat nahi..bcoz static goshti method area vr astat tyana 
			  //access krnysathi object chi mdt lagat nahi
		System.out.println("In gun");
		//System.out.println(this); //Error = Non static variable this cannot be referenced from the static context
	}
	public static void main(String [] pdp){
		
		System.out.println("Main Start");
		Demo obj = new Demo();
		obj.fun();//Internally = obj.fun(obj);
		System.out.println("Obj= "+obj);
		System.out.println("Main End");
	}
}

