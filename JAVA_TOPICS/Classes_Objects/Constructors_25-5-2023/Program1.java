class Demo{
	int x=10;
	Demo(){
		System.out.println(this);
		System.out.println(this.x + " In Constructor");
	}
	void fun(){
		System.out.println(this.x + "In Fun");
	}

	public static void main(String [] pdp){
		System.out.println("In Main");
		Demo obj = new Demo();
		obj.fun();
		System.out.println("In End of Main");

		}
}


