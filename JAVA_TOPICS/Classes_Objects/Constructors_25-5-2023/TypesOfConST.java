//Types Of constructors 
//1)Default Constructor(Compiler adds this constructor in our Code)
//2)Parameterized Cxonstructor = 1) No Argument Constructor(User Defined Constructors)
//				2)Parameterized Constructor
class Demo{
	int x=10;
	Demo(){//Demo(Demo this)
		System.out.println("In Constructor");
	}
	void fun(){//void fun(Demo this)
		System.out.println(x);//System.out.println(this.x);
	}
	public static void main(String [] pdp){
		Demo obj = new Demo();//Demo obj = new Demo();
				      //Demo(obj);
	}
}

