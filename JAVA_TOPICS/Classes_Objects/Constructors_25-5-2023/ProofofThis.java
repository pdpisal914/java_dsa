class Demo{
	Demo(){//Internally = Demo(Demo obj);
		System.out.println("In Constructor");
		System.out.println(this);
	}
	void fun(){//Internally Declaration = void fun(Demo this);

		System.out.println("In Fun");
		System.out.println(this);
	}
	public static void main(String [] pdp){
		
		System.out.println("Main Start");
//		System.out.println("Class Name = "+Demo);//Cannot find symbol
		Demo obj = new Demo();
		obj.fun();//Internally = obj.fun(obj);
		System.out.println("Obj= "+obj);
//		System.out.println("this in main="+this);//Cannot findSymbol
		System.out.println("Main End");
	}
}

