class Demo{
	int x=10;
	static int y=20;

	Demo(){
		System.out.println("In constructor ="+x);
	}

	void fun(){
		System.out.println("In Fun = " +x);
	}
	static void gun(Demo this1){
		System.out.println("In Gun = "+this1.x);
	}
	public static void main(String [] pdp){
		Demo obj = new Demo();
		gun(obj);
		obj.fun();
	}
}

