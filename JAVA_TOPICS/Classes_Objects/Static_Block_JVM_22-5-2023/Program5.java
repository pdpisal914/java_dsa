class Demo1{
	static void fun(){
		System.out.println("In Static Void fun()");
	}
	void gun(){
		System.out.println("In Non-Static Gun()");
	}
	static{
		fun();
	//	gun();//Non-Static method Fun Cannot be referenced from static context
		System.out.println("Static Block");
	}

	public static void main(String [] pdp){
		System.out.println("Main Method");
	}
}

