class OperatingSystem{
	String OSName;		//Name of Operating System
	String CodeName;	//CodeName of Operating system
	float KernelVer;	//Kernel version
	int OSType;		//64-bit or 32-bit
	
	void MemoryManagement(){
		System.out.println(OSName);
	}
	void DeviceManagement(){
		System.out.println(CodeName);
		int TotalTypes=3;	//Types Of Devices In Device Management
		String type1="Boot Device";
		String type2="Character Device";
		String type3="Network Device";
	}
	void ProcessManagement(){
		System.out.println(KernelVer);
		int TotalProStat=7;  //Total Number of Process States
		int TotalOp=4;	//Total number of Process Operations
		String Op1="Creation";
		String Op2="Scheduling";
		String Op3="Execution";
		String Op4="Killing";
	}
	void SecurityManagement(){
		System.out.println(OSType);
		int TotTyp=2;	//Total Types of Security Management
		String type1 = "Internal Security";
		String type2 = "External Security";
	}
}
class MyOS{
	public static void main(String [] pdp){
		OperatingSystem os = new OperatingSystem();
		os.OSName="Ubuntu 22.04";
		os.CodeName="Jammy JellyFish";
		os.KernelVer=5.15f;
		os.OSType = 64;
		os.MemoryManagement();
		os.DeviceManagement();
		os.ProcessManagement();
		os.SecurityManagement();
	
		OperatingSystem obj = new OperatingSystem();
		obj.OSName="Ubuntu 19.04";
		obj.CodeName = "Disco Dingo";
		obj.KernelVer=5.0f;
		obj.OSType = 64;
		obj.MemoryManagement();
		obj.DeviceManagement();
		obj.ProcessManagement();
		obj.SecurityManagement();
	}
}

