class Demo{
	static{
		System.out.println("Static Block Demo");
	}

	public static void main(String [] pdp){
		System.out.println("In Demo Main");
	}
}
class Client{
	static{
		System.out.println("Static Block Client");
	}
	public static void main(String [] pdp){
		System.out.println("In Client Main");
	}
	static{
		System.out.println("Static Block Client2");
	}
}

