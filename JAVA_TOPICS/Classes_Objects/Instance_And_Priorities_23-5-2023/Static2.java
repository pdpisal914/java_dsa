class Demo{
	static{
		System.out.println("In Static Demo");
	}

	public static void main(String [] pdp){
		System.out.println("In Demo Main");
	}
}
class Client{
	static{
		System.out.println("In Client Static Block");
	}
	public static void main(String [] pdp){
		System.out.println("In Client Main");
		Demo dm=new Demo();
	}
	static{
		System.out.println("In Client static Block 2");
	}
}


