class Demo{
	static{
		static int x=10;//Error=Illegel start of Expression
	}
	void fun(){
		static int y=20;//Error=Illegel start of Expression
	}
	static void gun(){
		static int z=30;//Error=Illegel start of Expression
	}
}

//Bcoz the static variable is dont written in any Block,Because the In the priority Sequence the 
//Priority of static variable is greater Than all other members in program.
//When we write any static variable in any other method then the concepts is Opposite ,bcoz upto which the
//method does not get location upto we not able to initialize the static variable insite the method
