class Demo{
	static {
		fun();
		System.out.println("In Static Demo");
	}
	static void fun(){
		System.out.println("In Static Demo Fun");
	}
}
class Client{
	static {
		Demo obj = new Demo();//When first Call goes to class Demo only at that Time The static blocks 
				      //of class Demo is Executed
		System.out.println("After object making");
	}
	public static void main(String [] pdp){
		Demo obj2=new Demo();//When we making object for Demo again then at that time the
				     //static block is not repetedly executed...it executed once
				     //e only
	}
}


