class Demo{
	int x=10;

	Demo(){
		System.out.println("In Constructor");
	}

	{
		//Instance Block
		System.out.println("In Instance Block 1");
	
	}

	public static void main(String [] pdp){
		Demo obj=new Demo();
		System.out.println("In Main");
	}

	{
		//Instance Block
		System.out.println("Instance block 2");
	}
}

