//"default Access Specifier " = SCOPE ==Under The Same Folder
class C2W{
	int numCourses=8;//Java Consider the Access specifer of all variables are Default
			 //i.e.Bu Default Access Specifier of Variables is "default"
	String FavCourse="CPP";//?default" variables are only accessible Under Same Folder In same file Or
			       //In Different Files in the same Folder also

	void Display(){
		System.out.println(numCourses);
		System.out.println(FavCourse);
	}
}
class Student{
	public static void main(String [] pdp){
		C2W obj = new C2W();
		obj.Display();

		System.out.println(obj.numCourses);
		System.out.println(obj.FavCourse);
	}
}

