class C2W{
	int numCourses=8;
	private String FavCourse="CPP";//Scope of Private Variable is only for Under The class ,
				       //Not Accessible from OutSide the class

	void Display(){
		System.out.println(numCourses);
		System.out.println(FavCourse);
	}
}
class Student{
	public static void main(String [] pdp){
		C2W obj = new C2W();
		obj.Display();

		System.out.println(obj.numCourses);
		System.out.println(obj.FavCourse);//ERROR=We Try To Access Private Variable in Another Class
	}
}

