/*REVERSE
 public synchronized StringBuffer reverse();

 	-Reverse the characters in the StringBuffer
	-The same Sequence and characters exists but in the reverse index ordering

Parameter = No parameter
Return Type = StringBuffer obj
*/

class ReverseDemo{
	public static void main(String [] pdp){

		StringBuffer sb = new StringBuffer("Pranav Pisal");

		System.out.println(sb);
		
		sb.reverse();

		System.out.println(sb);
	}
}
