/* DELETE
public synchronized StringBuffer delet(int start,int end);

   It delet the specific part of given old string fron starting indexto ending index

   Parameter = int start( it is the starting index from which the string start to delet)
   		int end(it is the index upto which delet the string);
Return type = StringBuffer
*/

class DeletDemo{
	public static void main(String [] pdp){
		StringBuffer sb = new StringBuffer("Pranav Pisal From Sangamner");

		System.out.println(sb);
		
		sb.delete(12,17);

		System.out.println(sb);
	}
}

