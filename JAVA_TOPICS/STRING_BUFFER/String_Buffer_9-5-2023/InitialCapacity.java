class InitialCapacityDemo{
	public static void main(String [] pdp){

		StringBuffer sb = new StringBuffer(100);

		sb.append("Biencaps");
		sb.append("Core2Web");

		System.out.println(sb);//BiencapsCore2Web
		System.out.println(sb.capacity());//100

		sb.append("Incubator");//BiencapsCore2WebIncubator

		System.out.println(sb);//BiencapsCore2WebIncubator
		System.out.println(sb.capacity());//100
	}
}
