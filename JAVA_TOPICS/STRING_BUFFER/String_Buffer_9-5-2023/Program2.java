class SBDemo{
	public static void main(String [] pdp){
		String str1 = "Pranav";
		String str2 = new String("Pisal");

		StringBuffer str3 = new StringBuffer("Sits");

		String str4 = str3.append(str1);//ERROR=Incompatibble conversion from StringBuffer to String
						//StringBuffer cannot be converted into String

		System.out.println(str1);
		System.out.println(str2);
		System.out.println(str3);
		System.out.println(str4);
	}
}

