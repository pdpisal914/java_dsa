class SbDemo{
	public static void main(String [] pdp){

		String str1 = "Pranav";
		String str2 = new String("Pisal");

		StringBuffer sb = new StringBuffer("Sangamner");

		System.out.println(str1);//Pranav
		System.out.println(str2);//Pisal
		System.out.println(sb);//Sangamner
		
		str1.concat(str2);//Concat method only concate str1 and str2 and returns the address of new created String
				  //we required to store this address for accessing it
		sb.append(str1);//It append the str1 next to sb and give reference to sb and also returns the
				//reference of sb 

		System.out.println(str1);//Pranav
		System.out.println(str2);//Pisal
		System.out.println(sb);//SangamnerPranav
	}
}


