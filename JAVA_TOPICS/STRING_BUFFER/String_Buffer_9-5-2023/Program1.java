class SBDemo{
	public static void main(String [] pdp){
		String str1 = "Pranav";
		String str2 = new String("Pisal");

		StringBuffer str3 = new StringBuffer("Sits");

		String str4 = str1.append(str3);//ERROR

		System.out.println(str1);
		System.out.println(str2);
		System.out.println(str3);
		System.out.println(str4);
	}
}
//ERROR = Append(0 is the method of stringBuffer class 
//        we trying to append str3 in immutable string str1
//        String class Does not have Append(0 method
// Incompatinble Type=Bcoz the append method returns the object of stringBuffer and we try to store StringBuffer object in the String class
// It is Impossible
