//StringBuffer sb = sb1.append(str);
class SBDemo{
	public static void main(String [] pdp){
		String str1 = "Pranav";
		String str2 = new String("Pisal");

		StringBuffer str3 = new StringBuffer("Sits");

		StringBuffer str4 = str3.append(str1);//Append method is of the StringBuffer class and
						      //It returns the Object of StringBuffer class

		System.out.println(str1);//Pranav
		System.out.println(str2);//Pisal
		System.out.println(str3);//SitsPranav
		System.out.println(str4);//SitsPranav
	}
}

