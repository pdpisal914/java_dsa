class StringBufferDemo6{
	public static void main(String [] pdp){
		String str = "PRANAV ";
		System.out.println(System.identityHashCode(str));//100

		StringBuffer sb = new StringBuffer(str);
		System.out.println(sb);
		System.out.println(sb.capacity());//23
		System.out.println(System.identityHashCode(sb));//1000

		StringBuffer sb1 = new StringBuffer(sb + "PISAL");
		System.out.println(sb1);
		System.out.println(sb1.capacity());//28
		System.out.println(System.identityHashCode(sb1));//2000

		StringBuffer sb2 = new StringBuffer(sb1);

		System.out.println(sb2);
		System.out.println(sb2.capacity());//28
		System.out.println(System.identityHashCode(sb2));//3000

		sb = new StringBuffer(sb2);

		System.out.println(sb);
		System.out.println(sb.capacity());//28
		System.out.println(System.identityHashCode(sb));//4000
	}
}





