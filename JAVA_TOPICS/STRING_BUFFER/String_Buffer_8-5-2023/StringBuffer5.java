class StringBufferDemo5{
	public static void main(String [] pdp){
		StringBuffer sb = new StringBuffer();
		
		System.out.println(sb);//BLANK
		System.out.println(sb.capacity());//16
		System.out.println(System.identityHashCode(sb));//1000

		String str = "PRANAV PISAL ";

		sb.append(str);

		System.out.println(sb);//PRANAV PISAL
		System.out.println(sb.capacity());//16
		System.out.println(System.identityHashCode(sb));//1000

		char ch[] = {'S','I','N','H','G','A','D',' ','C','o','L','L','E','G','E'};

		sb.append(ch);

		System.out.println(sb);//PRANAV PISAL SINHGAD COLLEGEE
		System.out.println(sb.capacity());//34
		System.out.println(System.identityHashCode(sb));//1000

		String str2 = new String(" NARHE,AMBEGAON,PUNE");

		sb.append(str2);
		System.out.println(sb);//PRANAV PISAL SINHGAD COLLEGE NARHE,AMBEGAON,PUNE
		System.out.println(sb.capacity());//70
		System.out.println(System.identityHashCode(sb));//1000
	}
}




