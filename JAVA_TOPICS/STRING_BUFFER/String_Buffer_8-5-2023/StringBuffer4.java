import java.io.*;
class StringBufferDemo4{
	public static void main(String [] pdp) throws IOException{
		
		StringBuffer str = new StringBuffer();
		System.out.println(str.capacity());
		
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the string");
		str.append(br.readLine());

		System.out.println(str);
		System.out.println(str.capacity());
		System.out.println(System.identityHashCode(str));

		System.out.println("Enter the string to append");
		str.append(br.readLine());

		System.out.println(str);
		System.out.println(str.capacity());
		System.out.println(System.identityHashCode(str));

		System.out.println("Enter the appending String");
		str.append(br.readLine());


		System.out.println(str);
		System.out.println(str.capacity());
		System.out.println(System.identityHashCode(str));
	}
}


