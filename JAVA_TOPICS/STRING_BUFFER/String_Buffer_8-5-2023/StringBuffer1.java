// STRING BUFFER having 16 character BYDEFAULT Capacity
// If you enter pranav in the stringbuffers object then it occupies 6 characters from 16 but 10 are empty
// if CAPACITY of StringBuffer is Full then StringBuffer Copies All content of old Object into new Object 
//      make this new Object of Size By Using Special Formula
//      NEW CAPACITY = (OLD CAPACITY + 1)*2;
//      EX.New capacity = (16+1)*2;=17*2=34 Capacity of new Object of StringBuffer
//      (NEW OBJECT)


class StringBufferDemo{
	public static void main(String [] pdp){
		StringBuffer str = new StringBuffer();
		System.out.println(str);// BLANK
		System.out.println(str.capacity());//16

		str.append("Pranav");

		System.out.println(str);//Pranav
		System.out.println(str.capacity());//16

	}
}

