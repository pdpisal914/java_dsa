//STRING BUFFER DEFAULT CAPACITY =16 Characters
//StringBuffer str = new StringBuffer("Pranav");
/*STRING CLASS = Strings Are Immutable 
 		Strings mdhe jr string change keli tr same object mdhe Changes hota nhi
 	 String mdhe apn String mdhe Change nhi kru shkt....
	jr khi change kela tr mg String ch new Object taiyar kela jato ..
	old object mdhe kuthlahi change kela jat nhi...
	tyacha reference kadhun 2rya object la dila jato*/
/*STRINGBUFFER Class = Changes done in same String object
 	String Buffer asa class ahe jo mutable string store krto...
	jrr aplyala String mdhe changes kryche astil tr StringBuffer aplyala te changes krnyasathi Allow krto..
	old object mdhech changes krun to new updates / changed String same object mdhech thevli jate
	*/
class StringBuff{
	public static void main(String [] pdp){
		StringBuffer sb = new StringBuffer("Pranav");

		System.out.println(sb);
		System.out.println(System.identityHashCode(sb));

		sb.append("Pisal");
		
		System.out.println(sb);
		System.out.println(System.identityHashCode(sb));
	}
}




