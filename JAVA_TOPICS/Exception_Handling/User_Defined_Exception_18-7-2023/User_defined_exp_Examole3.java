import java.util.Scanner;
import java.io.*;
class DataOverFlowException extends IOException{

	DataOverFlowException(String msg){
		System.out.println(msg);
	}
}

class DataUnderFlowException extends IOException{

	DataUnderFlowException(String msg){
		System.out.println(msg);
	}
}

class Demo{
	public static void main(String [] pdp) throws DataOverFlowException,DataUnderFlowException{

		int arr[]=new int[5];

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the Integer Values");
		System.out.println("Note=0<data<100");


		for(int i=0;i<arr.length;i++){
			int data=0;
			try{
			data = sc.nextInt();

			if(data<0){
				throw new DataUnderFlowException("value 0 peksha Choti ahe re Dlindr");
			}
			if(data>100){
				throw new DataOverFlowException("Value 100 peksha mothi ahe re");
			}
			}catch(DataOverFlowException obj){
				System.out.println("Jast Stack Zalya");
			}catch(DataUnderFlowException obj){
				System.out.println("kami stack zalya");
			}

			arr[i]=data;
		}

		for(int i=0;i<arr.length;i++){
			System.out.println(arr[i]+" ");
		}
	}
}


