import java.io.*;
class NegativeIndexException extends Exception{
		String excp=null;
	NegativeIndexException(String excp){
			super(excp);
			this.excp=excp;
		}

}
class ArrayOutOfBound extends NegativeIndexException{
	String excp=null;
	ArrayOutOfBound(String excp){
		super(excp);
		this.excp=excp;
	}
}
class Demo{
	int arr[]=new int[]{10,20,30,40,50};
	int getVal(int index) throws NegativeIndexException,ArrayOutOfBound{
			if(index<0){
				throw new NegativeIndexException("Negative index nko talkus re...");
			}else if(index>=arr.length){
			throw new ArrayOutOfBound("Index Array chya length peksha motha takla ahe bhava");
			}
		return arr[index];
	}
}
class Client{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int index = Integer.parseInt(br.readLine());
		Demo obj= new Demo();
		try{

			System.out.println(obj.getVal(index));

		}catch(ArrayOutOfBound obj1){
			System.out.println("Exception in Thread Main :"+ obj1.excp);
			System.out.println("obj1.getMessage() = "+obj1.getMessage());
				obj1.printStackTrace();
			System.out.println("Obj1.toString() ="+obj1.toString());
		}catch(NegativeIndexException obj2){
			System.out.println("Exception in Thread Main :"+ obj2.excp); 
			System.out.println("obj2.getMessage = "+obj2.getMessage());
			obj2.printStackTrace();
			System.out.println("obj2.toString = "+obj2.toString());
		}

	}
}
