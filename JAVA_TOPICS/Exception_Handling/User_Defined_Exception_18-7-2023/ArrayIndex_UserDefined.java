import java.io.*;
class NegativeIndexException extends Exception{
		
	NegativeIndexException(String excp){
			super(excp);
		}

}
class ArrayOutOfBound extends NegativeIndexException{
	ArrayOutOfBound(String excp){
		super(excp);
	}
}
class Demo{
	int arr[]=new int[]{10,20,30,40,50};
	int getVal(int index) throws NegativeIndexException,ArrayOutOfBound{
			if(index<0){
				throw new NegativeIndexException("YedGandiChya....");
			}else if(index>=arr.length){
			throw new ArrayOutOfBound("Lavdya....");
			}
		return arr[index];
	}
}
class Client{
	public static void main(String [] pdp) throws IOException,NegativeIndexException,ArrayOutOfBound{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int index = Integer.parseInt(br.readLine());
		Demo obj= new Demo();
		System.out.println(obj.getVal(index));
	}
}
