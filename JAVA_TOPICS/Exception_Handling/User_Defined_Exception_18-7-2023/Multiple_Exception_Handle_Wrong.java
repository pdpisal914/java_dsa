class Demo{
	public static void main(String [] pdp){
		System.out.println("Start Main");

		try{
			System.out.println(10/0);
		}catch(NumberFormatException | NullPointerException | ArithmeticException | Exception obj){
			/*When we write multiple exceptions in single Catch block at that time write exactNames
			 of the Exceptions ,
			 write the exceptions of the same level in the exception classes hirarchy*/
			System.out.println("Exception is occured");
		}

		System.out.println("End Main");
	}
}
//Multiple Exception Handles by using single Catch Block
