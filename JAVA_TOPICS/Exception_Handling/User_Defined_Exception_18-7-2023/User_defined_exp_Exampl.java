import java.util.Scanner;
class DataOverFlowException extends RuntimeException{

	DataOverFlowException(String msg){
		System.out.println(msg);
	}
}

class DataUnderFlowException extends RuntimeException{

	DataUnderFlowException(String msg){
		System.out.println(msg);
	}
}

class Demo{
	public static void main(String [] pdp){

		int arr[]=new int[5];

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the Integer Values");
		System.out.println("Note=0<data<100");


		for(int i=0;i<arr.length;i++){
			int data = sc.nextInt();

			if(data<0){
				throw new DataUnderFlowException("value 0 peksha Choti ahe re Dlindr");
			}
			if(data>100){
				throw new DataOverFlowException("Value 100 peksha mothi ahe re");
			}

			arr[i]=data;
		}

		for(int i=0;i<arr.length;i++){
			System.out.println(arr[i]+" ");
		}
	}
}


