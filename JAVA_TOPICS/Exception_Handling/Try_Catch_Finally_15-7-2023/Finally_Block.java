class Demo{
	void m1(){
		System.out.println("In main");
	}
	void m2(){
		System.out.println("In m2");
	}
	public static void main(String [] pdp){

		Demo obj = new Demo();

		obj.m1();

		obj=null;

		try{
			obj.m2();
		}catch(ArithmeticException obj1){
			System.out.println("Here");
		}finally{
			System.out.println("Connection closed");
		}
		System.out.println("End Main");
	}
}
