import java.util.concurrent.*;
class MyThread implements Runnable{
	int num=0;
	MyThread(int num){
		this.num = num;
	}
	public void run(){
		System.out.println("Thread Start: "+ num + Thread.currentThread());
		Dilay();
		System.out.println("Thread End: "+ num + Thread.currentThread());
	}
	void Dilay(){
		try{
			Thread.sleep(10000);
		}catch(InterruptedException obj){
		}
	}
}
class ThreadPoolDemo{
	public static void main(String [] pdp){
		ExecutorService es1 = Executors.newFixedThreadPool(2);
		ExecutorService es2 = Executors.newFixedThreadPool(2);

		for(int i=1;i<=10;i++){
			MyThread mt = new MyThread(i);
			es1.execute(mt);
		}

		for(int i=1;i<10;i++){
			MyThread mt = new MyThread(i);
			es2.execute(mt);
		}
		es1.shutdown();
		es2.shutdown();
	}
}
