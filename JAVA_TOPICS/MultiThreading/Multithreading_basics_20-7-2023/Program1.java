class MyThread extends Thread{
	public void run(){
		System.out.println("In Run");
		System.out.println(Thread.currentThread().getName());
	}
	//Never Override the start method of Thread Class in his child class
	public void start(){

		System.out.println("In MyThread start");
		run();
	}
}

class ThreadDemo{
	public static void main(String [] pdp){
		MyThread obj = new MyThread();

		obj.start();//This call is not bound with the start method of Thread Class this start method is bound with the 
			    //overrided method start in the MyThread class
			    //Hence the newly created Thread is not being start and not go to the run method....except the
			    //main thread is goint in the MyThread class and its code is executed by main Thread

		System.out.println(Thread.currentThread().getName());
	}
}

