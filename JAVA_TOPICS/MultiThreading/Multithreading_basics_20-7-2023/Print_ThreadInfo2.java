class MyThread extends Thread{
	public void run(){
		Thread obj = Thread.currentThread();
		System.out.println("MyThread : "+obj);
		System.out.println("MyThread Name : "+getName());
	}
}
class ThreadDemo{
	public static void main(String [] pdp){
		Thread obj = Thread.currentThread();
		System.out.println("Start main : "+obj);
	
		MyThread t1 = new MyThread();
		t1.start();
		System.out.println("Start Main Thread Name : "+t1.getName());

		obj.setPriority(7);

		Thread obj1 = Thread.currentThread();
		System.out.println("End main : " + obj1);
		
		MyThread t2 = new MyThread();
		t2.start();
		System.out.println("End Main Thread Name : "+t2.getName());
	}
}


