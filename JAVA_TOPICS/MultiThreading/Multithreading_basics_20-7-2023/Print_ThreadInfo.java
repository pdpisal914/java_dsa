class MyThread extends Thread{
	public void run(){
		Thread obj = Thread.currentThread();
		System.out.println("MyThread : "+obj);
	}
}
class ThreadDemo{
	public static void main(String [] pdp){
		Thread obj = Thread.currentThread();
		System.out.println("Start main : "+obj);
	
		MyThread t1 = new MyThread();
		t1.start();

		obj.setPriority(7);

		Thread obj1 = Thread.currentThread();
		System.out.println("End main : " + obj1);
		
		MyThread t2 = new MyThread();
		t2.start();
	}
}


