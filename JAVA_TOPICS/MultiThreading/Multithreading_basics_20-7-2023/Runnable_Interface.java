class MyThread implements Runnable{

	public void run(){
		Thread t1 = Thread.currentThread();
		System.out.println("MtThread : "+t1.getState());

		MyThread0 obj = new MyThread0();
		Thread th = new Thread(obj);
		th.start();
	}
}

class MyThread0 implements Runnable{

	public void run(){
		Thread t2 = Thread.currentThread();
		System.out.println("MyThread 0 : "+t2.getState());

		try{
		Thread.sleep(2000);
		}catch(InterruptedException obj){
		}

		System.out.println("MyThread 0 : "+t2.getState());
	}
}
class ThreadDemo{
	public static void main(String [] pdp) throws InterruptedException{
		Thread t3 = Thread.currentThread();
		System.out.println("In Main : "+t3.getState());

		Thread.sleep(1000);

		System.out.println("In Main : "+t3.getState());
		MyThread obj =new MyThread();
		Thread th = new Thread(obj);
		th.start();
		System.out.println("In Main : "+t3.getState());
	}
}



