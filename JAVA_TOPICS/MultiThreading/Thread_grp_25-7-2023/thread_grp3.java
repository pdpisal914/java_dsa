class MyThread extends Thread{
	MyThread(){
	
	}
	MyThread(String str){
		super(str);
	}
	public void run(){
		System.out.println(getName());
		System.out.println(Thread.currentThread().getThreadGroup());
	}
}

class ThreadGroupDemo{
	public static void main(String [] pdp){

		MyThread obj1 = new MyThread(" Pranav ");
		MyThread obj2 = new MyThread("Rohit");
		MyThread obj3 = new MyThread();

		System.out.println("Obj1="+obj1.getName());
		System.out.println("Obj2 = "+obj2.getName());
		System.out.println("Obj3 ="+obj3.getName());
		obj1.start();
	}
}
