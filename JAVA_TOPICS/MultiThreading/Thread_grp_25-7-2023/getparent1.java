class MyThread extends Thread{
	MyThread(ThreadGroup obj,String str){
		super(obj,str);
	}

	public void run(){
		System.out.println(Thread.currentThread());
		System.out.println(getName());//Give the name of thread
		System.out.println(Thread.currentThread().getName());//Give the name of thread
		}
}
class ThreadGroupDemo{
	public static void main(String [] pdp){
		ThreadGroup par = new ThreadGroup("META");

		ThreadGroup t1 = new ThreadGroup(par,"WHATSAPP");
		ThreadGroup t2 = new ThreadGroup(par,"FACEBOOK");
		ThreadGroup t3 = new ThreadGroup(par,"MESSENGER");
		System.out.println("Active Groups = "+par.activeGroupCount());//7
		

		ThreadGroup mt1 = new ThreadGroup(t1,"COMMUNITY");
		
		MyThread cm1 = new MyThread(mt1,"GDSC_Community");
		MyThread cm2 = new MyThread(mt1,"ACM_Community");
		MyThread cm3 = new MyThread(mt1,"We_Build_Pune");
		cm1.start();
		cm2.start();
		cm3.start();
	
		ThreadGroup mt2 = new ThreadGroup(t1,"CHAT");

		MyThread ch1 = new MyThread(mt2,"Normal_Chat");
		MyThread ch2 = new MyThread(mt2,"Archived");
		MyThread ch3 = new MyThread(mt2,"Private");
		ch1.start();
		ch2.start();
		ch3.start();


		ThreadGroup mt3 = new ThreadGroup(t1,"STATUS");

		MyThread st1 = new MyThread(mt3,"My_Status");
		MyThread st2 = new MyThread(mt3,"Recent_Status");
		MyThread st3 = new MyThread(mt3,"Viewed_Status");

		st1.start();
		st2.start();
		st3.start();
		
		ThreadGroup mt4 = new ThreadGroup(t1,"CALLS");
		
		MyThread cl1 = new MyThread(mt4,"Audio_calls");
		MyThread cl2 = new MyThread(mt4,"Video_Calls");
		cl1.start();
		cl2.start();

		System.out.println("Par Active Group ="+par.activeGroupCount());//4
		System.out.println("Par Active count = "+par.activeCount());
		System.out.println(mt1.getParent());
		System.out.println(mt2.getParent());
		System.out.println(mt3.getParent());
		System.out.println(mt4.getParent());


		//		System.out.println(t1.activeGroupCount());
//		System.out.println(t2.activeGroupCount());
//		System.out.println(t3.activeGroupCount());


//		System.out.println(mt1.activeGroupCount());
//		System.out.println(mt2.activeGroupCount());
//		System.out.println(mt3.activeGroupCount());
//		System.out.println(mt4.activeGroupCount());
	}
}
	
		



