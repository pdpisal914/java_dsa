//Yield Method

class MyThread extends Thread{
	public void run(){
		System.out.println(Thread.currentThread().getName());
	}
}
class ThreadYieldClass{
	public static void main(String [] pdp){

		MyThread obj = new MyThread();

		obj.start();

		obj.yield();

		System.out.println(Thread.currentThread().getName());
	}
}
