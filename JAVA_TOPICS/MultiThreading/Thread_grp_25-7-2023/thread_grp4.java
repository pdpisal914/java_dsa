class MyThread extends Thread{
	MyThread(ThreadGroup obj,String str){
		super(obj,str);
	}
	public void run(){
		System.out.println(Thread.currentThread());
	}

}
class ThreadGroupDemo{
	public static void main(String [] pdp){
		ThreadGroup obj = new ThreadGroup("Core2Web");

		MyThread t1 = new MyThread(obj,"C_CPP_DSA");
		MyThread t2 = new MyThread(obj,"JAVA_DSA");
		MyThread t3 = new MyThread(obj,"PYTHON_ML");

		t1.start();
		t2.start();
		t3.start();
	}
}



