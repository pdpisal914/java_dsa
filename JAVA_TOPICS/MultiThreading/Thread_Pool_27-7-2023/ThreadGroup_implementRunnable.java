class MyThread implements Runnable{
	public void run(){
		System.out.println(Thread.currentThread());

		try{
			Thread.sleep(5000);
		}catch(InterruptedException obj){
			System.out.println(obj.toString());
		}
	}
}
class ThreadGroupDemo{
	public static void main(String [] pdp) throws InterruptedException{
		ThreadGroup tg1 = new ThreadGroup("Core2Web");
		MyThread mt1 = new MyThread();
		MyThread mt2 = new MyThread();
		MyThread mt3 = new MyThread();

		Thread t1 = new Thread(tg1,mt1,"C_Cpp_Dsa");
		Thread t2 = new Thread(tg1,mt2,"Java_Dsa");
		Thread t3 = new Thread(tg1,mt3,"Python_ML");

		t1.start();
		t2.start();
		t3.start();

		ThreadGroup tg2 = new ThreadGroup(tg1,"Incubator");
		MyThread mt4 = new MyThread();
		MyThread mt5 = new MyThread();
		MyThread mt6 = new MyThread();

		Thread t4 = new Thread(tg2,mt4,"Flutter");
		Thread t5 = new Thread(tg2,mt5,"Java_Springboot");
		Thread t6 = new Thread(tg2,mt6,"React_js");

		t4.start();
		t5.start();
		t6.start();
	
		//Thread.sleep(4000);
		tg2.interrupt();
		System.out.println(tg1.activeCount());
		System.out.println(tg1.activeGroupCount());
	}   
}
