class MyThread extends Thread{

	MyThread(ThreadGroup obj,String str){

		super(obj,str);
	}
	public void run(){
		System.out.println(Thread.currentThread());

		try{
			Thread.sleep(3000);
		}catch(InterruptedException obj){
			System.out.println(obj.toString());
		}
	}
}
class ThreadGroupDemo{
	public static void main(String [] pdp){
		ThreadGroup tg1 = new ThreadGroup("India");
		MyThread mt1 = new MyThread(tg1,"Maharashtra");
		MyThread mt2 = new MyThread(tg1,"Goa");
		mt1.start();
		mt2.start();

		ThreadGroup ctg1 = new ThreadGroup(tg1,"Pakistan");
		MyThread mt3 = new MyThread(ctg1,"Karachi");
		MyThread mt4 = new MyThread(ctg1,"Lahore");
		mt3.start();
		mt4.start();

		ThreadGroup ctg2 = new ThreadGroup(tg1,"Bangladesh");
		MyThread mt5 = new MyThread(ctg2,"Dhaka");
		MyThread mt6 = new MyThread(ctg2,"Mirpur");
		mt5.start();
		mt6.start();

		System.out.println(tg1.activeCount());
		System.out.println(tg1.activeGroupCount());
	}
}


