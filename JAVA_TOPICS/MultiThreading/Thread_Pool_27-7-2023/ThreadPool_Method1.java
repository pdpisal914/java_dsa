import java.util.concurrent.*;
class MyThread implements Runnable{
	int num;
	MyThread(int num){
		this.num=num;
	}

	public void run(){
		System.out.println(Thread.currentThread() + " start Thread "+num);
		dailyTask();
		System.out.println(Thread.currentThread() + " end Thread "+num);
	}

	void dailyTask(){
		try{
			Thread.sleep(8000);
		}catch(InterruptedException obj){
		}
	}
}
class ThreadPoolDemo{
	public static void fun(String [] pdp){
		ExecutorService es = Executors.newFixedThreadPool(5);

		for(int i=1;i<10;i++){
			MyThread obj = new MyThread(i);
			es.execute(obj);
		}
		es.shutdown();
	}
}
		
