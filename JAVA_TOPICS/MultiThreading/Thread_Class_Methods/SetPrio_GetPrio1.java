class MyThread extends Thread{
	public void run(){
		System.out.println(Thread.currentThread());
		System.out.println(Thread.currentThread().getPriority());
		System.out.println(getPriority());//this.getPriority();
	}
}

class ThreadDemo{
	public static void main(String [] pdp){
		Thread.currentThread().setPriority(9);//we set priority for main Thread
		MyThread obj1  = new MyThread();//Creation of new object
		obj1.setPriority(8);//we set the priority of newly created thread
		obj1.start();//we give instruction to newly created thread to run in the run method
		obj1.setName("Pdp");//we set the name of newly created thread
		System.out.println(obj1.getName());//we required the name of newly created thread
		System.out.println(Thread.currentThread().getName());//we find the name of main Thread
		System.out.println(Thread.currentThread().getPriority());//We find the priority of main Thread
		System.out.println(Thread.currentThread());//We get output =Thread[main,9,main]
	}
}



