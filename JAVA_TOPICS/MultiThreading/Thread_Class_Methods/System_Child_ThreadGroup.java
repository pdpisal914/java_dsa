class MyThread implements Runnable{
	int x =0;
	MyThread(int x){
		this.x = x;
	}
	public void run(){
		System.out.println( x +"  In Run : "+ Thread.currentThread());
		Info();
		System.out.println(x +"  End Run Parent of ChildGP_1: "+ Thread.currentThread().getThreadGroup().getParent());
	}
	void Info(){
		try{
			Thread.sleep(5000);
		}catch(InterruptedException obj ){
		}
	}
}
class ThreadDemo{
	public static void main(String [] pdp){
		ThreadGroup sys = Thread.currentThread().getThreadGroup().getParent();
		ThreadGroup gp =new ThreadGroup(sys,"ChildGP_1");

		MyThread mt1 = new MyThread(1);
		MyThread mt2 = new MyThread(2);
		MyThread mt3 = new MyThread(3);
		MyThread mt4 = new MyThread(4);
		MyThread mt5 = new MyThread(5);

		Thread t1 = new Thread(gp,mt1,"child1");
		Thread t2 = new Thread(gp,mt2,"child2");
		Thread t3 = new Thread(gp,mt3,"child3");
		Thread t4 = new Thread(gp,mt4,"child4");
		Thread t5 = new Thread(gp,mt5,"child5");
		t1.start();
		t2.start();
		t3.start();
		t4.start();
		t5.start();

		System.out.println("In main : "+ Thread.currentThread());
		System.out.println("In main : "+ Thread.currentThread().getThreadGroup());
		System.out.println("In main cha ParentGP : "+ Thread.currentThread().getThreadGroup().getParent());
		System.out.println("In main System childGP: "+ gp);
		System.out.println("In main childGP Parent: "+ gp.getParent());
	}
}


