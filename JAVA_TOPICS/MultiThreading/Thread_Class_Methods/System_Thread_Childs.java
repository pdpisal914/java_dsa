class MyThread extends Thread{
	MyThread(ThreadGroup x,String y){
		super(x,y);
	}

	public void run(){
		System.out.println("In Run : "+Thread.currentThread());
		try{
			sleep(5000);
		}catch(InterruptedException obj ){
		}
	}
}
class ThreadDemo{
	public static void main(String [] pdp){
		ThreadGroup tt= Thread.currentThread().getThreadGroup().getParent();
		MyThread mt1 = new MyThread(tt,"main-1");
		MyThread mt2 = new MyThread(tt,"main-2");
		mt1.start();
		mt2.start();

		System.out.println("In main = "+Thread.currentThread());
		System.out.println("In main = "+Thread.currentThread().getThreadGroup().getParent());
	}
}
