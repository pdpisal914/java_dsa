class MyThread extends Thread{
	public void run(){

		for(int i=0;i<20;i++){
			System.out.println("In Run");
			try{
				Thread.sleep(2000);/*Here sleep throws the Interruptedexception butrun method is the method of parent class Thread 
					   now we overried that method in theChild class MyThread then at that time is The parent class 
					   method doesnot throws any exception and in child class during overriding that method throws any
					   Exception then this is wrong....It will give an error...hence here we are not able to or the Run
					   method not able to throws that Interrupted exception towards main....Hence here only one way is 
					   remaining ....handle the InterruptedException thrown by Sleep(); method by using TRY_CATCH block
					   */
			}catch(InterruptedException obj){
			}
		}
	}
}
class Demo{
	public static void main(String [] pdp) throws InterruptedException{

		MyThread obj = new MyThread();
		obj.start();

		for(int i=0;i<20;i++){
			System.out.println("In Main");
			Thread.sleep(2000);//Now the sleep method throws the InterruptedException hence the main method also required 
					   //to Throws that InterruptedException or Handle that exception in main method by using try-catch
					   //block
		}
	}
}
