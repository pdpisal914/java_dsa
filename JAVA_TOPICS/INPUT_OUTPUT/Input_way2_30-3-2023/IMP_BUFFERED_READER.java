//main code

import java.io.*;
class Code{
	public static void main(String [] pdp) throws IOException{

		InputStreamReader isr= new InputStreamReader(System.in);

		BufferedReader br=new BufferedReader(isr);

		System.out.println("HOW TO GET STRING INPUT?");
		System.out.println("Enter the Name");
		String name=br.readLine();
		System.out.println("NAME="+name+"\nEnd string i/p");
		
		System.out.println("HOW TO GET INTEGER INPUT?");
		System.out.println("Enter the Age");
		int age=Integer.parseInt(br.readLine());
		System.out.println("AGE="+age+"\nEnd integer i/p");
	}
}
