import java.io.*;

class Bufferedreader{
	public static void main(String [] pdp) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the batsman name");
		String bts=br.readLine();

		System.out.println("Enter the bollers name");
		String boll=br.readLine();

		System.out.println("Enter the Jersey number");
		/*int jerNo=br.readLine(); -> it is wrong ...String is class And class is not converted into primitive dataty			pes ..for storing String into integer then it requires the warreper class and parsing of string into int...*/
		int jerNo=Integer.parseInt(br.readLine());

		System.out.println("Enter the average runrate");
		//float avg=br.readLine();  ->  readline() method having String datatype and classes are not converted into Primitive datatype directly
		float avg=Float.parseFloat(br.readLine());

		System.out.println("Batsmann="+bts);
		System.out.println("Boller="+boll);
		System.out.println("Jersey Number"+jerNo);
		System.out.println("Boller="+avg);
	}
}

