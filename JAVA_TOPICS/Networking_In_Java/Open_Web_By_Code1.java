import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class WebsiteOpener extends Frame {
    private Button openButton;

    public WebsiteOpener() {
        openButton = new Button("Open Website");
        openButton.setBounds(100, 100, 150, 30);
        openButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                openWebsite();
            }
        });

        add(openButton);

        setTitle("Website Opener");
        setSize(400, 200);
        setLayout(null);
        setVisible(true);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent we) {
                System.exit(0);
            }
        });
    }

    public void openWebsite() {
        try {
            URI uri = new URI("https://www.youtube.com"); // Replace with the website URL you want to open
            Desktop.getDesktop().browse(uri);
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new WebsiteOpener();
    }
}

